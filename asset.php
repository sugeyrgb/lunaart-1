<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	/*
	var_dump($_SERVER['REQUEST_METHOD']);
	echo "<br> GET dump: <br>";
	var_dump($_GET);
	echo "<br> POST dump: <br>";
	var_dump($_POST);
	echo "<br>";
	echo "Array POST: ".var_dump(empty($_POST));
	echo "<br>";
	echo "Array Get: ".var_dump(empty($_GET));
	echo "<br>";*/

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
			
				switch ($_GET["r"]) {
					case "collection":
						switch ($_GET["a"]) {
							case "da":
								$gam = "SELECT *, CONCAT(title,' (',materialsStr,')') as `text`, id as `value`, DATE_FORMAT(collection.dated, '%m - %d  - %y') as fdated FROM collection WHERE deleted = 0 ORDER BY category ASC, dated DESC";
								
								$api = $db->query($gam,true,true);

								foreach ($api["query"] as $key => $value) {
									if($api["query"][$key]["displayArray"] != null){
										$api["query"][$key]["displayArray"] = json_decode($api["query"][$key]["displayArray"]);
									}else{
										$api["query"][$key]["displayArray"] = array();
									}
									
								}

								$lb->toClient(true,$api["query"],false);
							break;

							case "dcone":
								$gam = "SELECT *, CONCAT(title,' (',materialsStr,')') as `text`, id as `value`, DATE_FORMAT(collection.dated, '%m - %d  - %y') as fdated FROM collection WHERE id = ".$_GET['id'];
								
								$api = $db->query($gam,true);


								$lb->toClient(true,$api["query"],false);
							break;

							case "dmater":
								$gam = "SELECT * FROM collection_material WHERE collection = ".$_GET['id'];
								
								$api = $db->query($gam,true);

								$materials = array();

								foreach($api["query"] as $key => $value) {
									$sql = "SELECT * FROM material WHERE id = ".$api["query"][$key]["material"]." LIMIT 1";
									$test = $db->query($sql, true,true);
									array_push($materials,$test["query"][0]);
								}


								$lb->toClient(true,$materials,false);
							break;
							
							default:
								$lb->defaultRequest();
							break;
						}
					break;

					case "dp":
						switch ($_GET["a"]) {
							case "da":
								$gam = "SELECT *, title as `text`, id as value FROM dp ORDER BY title ASC";

								$api = $db->query($gam,true);

								$lb->toClient(true,$api["query"],false);
							break;

							case "dpone":
								$gam = "SELECT *, title as `text`, id as value, sc as sf FROM dp WHERE id = ".$_GET['id'];

								$api = $db->query($gam,true);

								$lb->toClient(true,$api["query"],false);
							break;
							
							default:
								$lb->defaultRequest();
							break;
						}
					break;

					case "material":
						switch ($_GET["a"]) {
							case "da":
								$gam = "SELECT *, title as `text`, id as value FROM material WHERE deleted = 0 ORDER BY title ASC";

								$api = $db->query($gam,true);

								$lb->toClient(true,$api["query"],false);
							break;
							
							default:
								# code...
								break;
						}
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}

				

			break;

			case 'POST':
				//Post request handler

				switch($_POST["request"]){
					case "collection":
						switch($_POST["action"]) {
							case "new":
								
								unset($_POST["desc"]);
								unset($_POST["img"]);
													
								if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS) && !empty($_FILES)){
									
									extract($_POST);

									$displayAr = array();

									if($db->isValue("collection","title",$title,$debug=false)){
										$existingColQ = "SELECT * FROM collection WHERE title = '".$title."'";
										$existingCol = $db->query($existingColQ, true, true);
										
										if($existingCol['query'][0]['deleted'] == 1){
											$db->query("DELETE FROM collection WHERE title = '".$title."'", false);
										}else{
											$lb->toClient(false,$callback,"CollectionAlready");
											return false;
										}
									}

									$mi = array();

									$path = "media/collection/".$category."/".strtolower(trim($title));

									foreach ($_FILES as $key => $value) {
										array_push($mi,$lf->mf($_FILES[$key],$path,$key.".webp"));
									}

									foreach ($materials as $key => $value) {
										$value = json_decode($value);

										$materialSrt = "";

										foreach ($value as $iKey => $iValue) {
											$minisql = "SELECT title FROM material WHERE id = ".$value[$iKey];
											$tryit = $db->query($minisql,true);

											$materialSrt .= $tryit['query'][0]['title'].", ";

										}

										$materials[$key] = substr(trim($materialSrt), 0, -1);
									}
									
									$qm = "INSERT INTO collection (title, 
																   category, 
																   img, 
																   countImg, 
																   materialsStr, 
																   displayArray, 
																   price, 
																   dp) 
															VALUES ('$title', 
																	'$category',
																	'".$mi[0]['dir_name']."',
																	".(count($mi)-1).",
																	'".$materials[0]."',
																	'".json_encode($materials)."',
																	'$price',
																	'$dp')";

									$mc = $db->query($qm,false,true);

									foreach ($materials as $key => $value){
										$mtr = "INSERT INTO `collection_material` (`collection`, `material`, `dated`) 
												VALUES ('".$mc['last_id']."', '$value', CURRENT_TIMESTAMP)";

										$link = $db->query($mtr,false);
									}
									
									if($mi[0]["answer"]){
										//

										if($mc["status"]){
											$lb->toClient(true,$callback,false);
										}else{
											$lb->defaultQuery();
										}
									}else{
										$lb->toClient(false,$callback,"NotFileMoved");
									}
									
								}else{
									$lb->defaultEnough();
								}
								
							break;
							
							case "update":
								
								unset($_POST["countImg"]);
								unset($_POST["imgIndex"]);
								unset($_POST["hover"]);
								unset($_POST["description"]);
								unset($_POST["materialsStr"]);
								unset($_POST["active"]);
								unset($_POST["deleted"]);
								unset($_POST["text"]);
								unset($_POST["displayArray"]);


								if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS,true) && !empty($_FILES)){
									
									extract($_POST);

									if($db->isValue("material","title",$title,$debug=false)){
										$lb->toClient(false,$callback,"AssetAlready");
										return false;
									}

									
									$mi = array();

									$path = "media/collection/".$category."/".strtolower(trim($title));

									foreach ($_FILES as $key => $value) {
										array_push($mi,$lf->mf($_FILES[$key],$path,$key.".webp"));
									}

									foreach ($materials as $key => $value) {
										$value = json_decode($value);

										$materialSrt = "";

										foreach ($value as $iKey => $iValue) {
											$minisql = "SELECT title FROM material WHERE id = ".$value[$iKey];
											$tryit = $db->query($minisql,true);

											$materialSrt .= $tryit['query'][0]['title'].", ";

										}

										$materials[$key] = substr(trim($materialSrt), 0, -1);
									}

									$qm = "UPDATE collection 
										   SET title = '$title', 
												category = '$category',
												img = '".$mi[0]['dir_name']."',
												countImg = ".(count($mi)-1).",
												materialsStr = '$materialSrt',
												price = '$price',
												dp = '$dp',
												displayArray = '".json_encode($materials)."'
											WHERE id = ".$_POST['id'];

									$mc = $db->query($qm,false,true);


									foreach ($materials as $key => $value){
										$mtr = "INSERT INTO `collection_material` (`collection`, `material`, `dated`) 
												VALUES ('".$mc['last_id']."', '$value', CURRENT_TIMESTAMP)";

										$link = $db->query($mtr,false);
									}
									
									if($mi[0]["answer"]){
										//

										if($mc["status"]){
											$lb->toClient(true,$callback,false);
										}else{
											$lb->defaultQuery();
										}
									}else{
										$lb->toClient(false,$callback,"NotFileMoved");
									}

									
								}else{
									$lb->defaultEnough();
								}
							break;

							case "delete":
							
								$sql = "UPDATE collection SET deleted = 1 WHERE id = ".$_POST["id"];

								$try = $db->query($sql,false);

								if($try["status"]){
									$lb->toClient(true);
								}else{
									$lb->defaultQuery();
								}

							break;

							default:
								$lb->defaultRequest();
							break;
						}
					break;

					case "pattern":
						switch($_POST["action"]) {
							case "new":
							

								if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS) && !empty($_FILES)){
									
									extract($_POST);

									if($db->isValue("dp","title",$title,$debug=false)){
										$lb->toClient(false,$callback,"DPAlready");
										return false;
									}
									
									$path = "media/dp/".$category.'/'.strtolower(trim($title));

									$pti = $lf->mf($_FILES["ptImg"],$path,"ptimg.png");

									$ptd = $lf->mf($_FILES["ptDetail"],$path,"ptd.png");

									$ptc = $lf->mf($_FILES["chroma"],$path,"chroma.png");

									$dps = "INSERT INTO dp (title, img, imgDetail, imgChroma, category, chromas, description, width, height, sc,price, datetime) 
											VALUES ('".$title."',  
													'".$pti['dir_name']."', 
													'".$ptd['dir_name']."', 
													'".$ptc['dir_name']."',  
													'".$category."', 
													'".$cchromas."', 
													'".$desc."', 
													'".$w."', 
													'".$h."',
													'".$sc."',
													'".$price."',
													CURRENT_TIMESTAMP)";

									if($pti["answer"] && $ptd["answer"] && $ptc["answer"]){
										$ins = $db->query($dps,false);

										if($ins["status"]){
											$lb->toClient(true,$callback,false);
										}else{
											$lb->defaultQuery();
										}
									}else{
										$lb->toClient(false,$callback,"NotFileMoved");
									}

									
								}else{
									$lb->defaultEnough();
								}
							break;
							
							case "update":
								unset($_POST["deleted"]);

								if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){
									
									extract($_POST);

									$dps = "UPDATE dp SET 
														title = '".$title."',
														category = ".$category.",
														price = '".$price."',
														description = '".$description."'
													WHERE id = ".$id;

									$ins = $db->query($dps,false);

									if($ins["status"]){
										$lb->toClient(true,$callback,false);
									}else{
										$lb->defaultQuery();
									}

								}else{
									$lb->defaultEnough();
								}
							break;

							case "delete":
								$sql = "UPDATE dp SET deleted = 1 WHERE id = ".$_POST["id"];

								$try = $db->query($sql,false);

								if($try["status"]){
									$lb->toClient(true);
								}else{
									$lb->defaultQuery();
								}
							break;

							default:
								$lb->defaultRequest();
							break;
						}
					break;
					
					case "material":
						switch($_POST["action"]) {
							case "new":

								
								if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS) && !empty($_FILES)){
									
									extract($_POST);

									if($db->isValue("material","title",$title,$debug=false)){
										$existingColQ = "SELECT * FROM material WHERE title = '".$title."'";
										$existingCol = $db->query($existingColQ, true, true);
										
										if($existingCol['query'][0]['deleted'] == 1){
											$db->query("DELETE FROM material WHERE title = '".$title."'", false);
										}else{
											$lb->toClient(false,$callback,"AssetAlready");
											return false;
										}
									}

									$mi = $lf->mf($_FILES["img"],"media/material/".$category);

									

									$qm = "INSERT INTO material (title, category, img, deleted, dated) 
											VALUES ('".$title."', 
													'".$category."', 
													'".$mi['dir_name']."', 
													0,
													CURRENT_TIMESTAMP)";
									
									if($mi["answer"]){
										$ins = $db->query($qm,false,true);

										if($ins["status"]){
											$lb->toClient(true,$callback,false);
										}else{
											$lb->defaultQuery();
										}
									}else{
										$lb->toClient(false,$callback,"NotFileMoved");
									}

									
								}else{
									$lb->defaultEnough();
								}
							break;

							case "delete":

								$sql = "UPDATE material SET deleted = 1 WHERE id = ".$_POST["id"];

								$try = $db->query($sql,false);

								if($try["status"]){
									$lb->toClient(true);
								}else{
									$lb->defaultQuery();
								}

							break;
							
							default:
								$lb->defaultRequest();
							break;
						}
					break;

					default:
						$lb->defaultRequest();
					break;
				}

			break;

			case 'PUT':
				//Post handled like PUT

			break;

			case 'DELETE':
				//Post handled like Delete

			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

		//$db = new FriendofMySQL($execute);
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 
	

?>