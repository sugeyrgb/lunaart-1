<?php 
	include_once("controller/functions/elements.php");
	$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	//CORS Policy declatarion
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php 
    	include_once("reference.php");	
		luball_element("head.php","Urban City designs");

		session_start();

		if(!empty($_SESSION)){
			echo "<script> localStorage.setItem('sd','".json_encode($_SESSION)."');</script>\n";
		}else{
			echo "<script> localStorage.removeItem('sd');</script>";
		}

		if(!empty($_GET['o'])){
			echo "<script> const number = '".$_GET['o']."'</script>";
		}else{
			echo "<script> const number = null;</script>";
		}

		try{
			echo "<script> const user = ".json_encode($_SESSION['user'])."</script>";
		}catch(Exception $e){
			echo "<script> const user = null;</script>";
		}
	?>

	<meta property="og:type" content="website">

	<meta name="description" content="">
    
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/view.css">

	<!-- Components -->
		
	<style>
		
		p{
			width: 100%;
			display: inline-block;
			font-size: 14pt;
		}

		.order-prepend,.order-area{
			color: #5B5B5F;
			float: left;
			font-weight: 500;
		}

		.order-underline{
			border-bottom: 1px solid #B59568;
			display: block;
			text-align: center;
			font-size: 12pt;
			font-weight: 400;
		}

		h4{
			border-bottom: 1px solid #ADACB2;
			color: #B59568;
		}

	</style>

  </head>
  <body>
  	
	<div id="app">
		
		<b-navbar id="headerNav" class='luna-blue' fixed="top" toggleable="md" class="no-print" type="dark" :sticky=true variant="info">
			<b-navbar-nav class="ml-auto" right>
					<div class="padding">
						<b-button block @click="printPage" style="height: 100%;" class="btn-gold white-text">
								<i class="material-icons">
								print
								</i>
					</b-button>
					</div>
	   
	      	</b-navbar-nav>
		</b-navbar>
		
		<route-view></route-view>


	</div>

    <script type="text/javascript" src="js/view.js"></script>

  </body>
</html>