<?php 
	
	include_once("controller/classes/luball.php");

	$lb = new Luball();

	$lb->power_session_start();

	if(!$lb->isSession()){
		header("Location: /");
	}

	include_once("controller/functions/elements.php");
	$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php 
    	include_once("reference.php");	
		luball_element("head.php","Urban City designs");

		try{
			echo "<script> const user = ".json_encode($_SESSION['user'])."</script>";
		}catch(Exception $e){
			echo "<script> const user = null;</script>";
		}
	?>

	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:url" content="">
	<meta property="og:image" content=""> 
	<meta property="og:type" content="website">

	<meta name="description" content="">

	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/dashboard.css">

	<!-- Components -->

  </head>

  <body>
  	
	<div id="app">
		
		<b-navbar class="luna-blue" toggleable="md" :sticky="true" type="dark" variant="info">

			<b-navbar-brand href="./">
				<img src="media/img/logo_2.png" class="d-inline-block logo" alt="Urban City Designs" />
			</b-navbar-brand>

			<b-navbar-nav class="ml-auto">

				<my-toggle right></my-toggle>
			</b-navbar-nav>

		</b-navbar>
	
		<route-view class="displayed"></route-view>


		<sidebar>
			<!-- <Item class="item" v-for="item in items" v-bind:key="item" /> -->
		</sidebar>

		<b-modal ref="eulaModal" id="modal-eula" @hidden="closedEULA" hide-footer title="Accept EULA" v-if="session.role >= 2">
			<b-container fluid>
				<b-row>
					<b-col>
						<p>You must accept <a class="gold-link" href="/eula" target="_blank">End-User License Agreement</a> before to keep using the dashboard tools.</p>
					</b-col>
				</b-row>
				<b-row>
					<b-col offset-sm=3></b-col>
					<b-col sm=3><b-button @click="closeEULA" class="btn btn-secondary">REJECT</b-button></b-col>
					<b-col sm=3><b-button @click="acceptEULA" class="btn btn-blue">ACCEPT</b-button></b-col>
				</b-row>
			</b-container>
		</b-modal>
		
	</div>
	
	<script type="text/javascript" src="js/components/dashboard/material.js"></script>
	<script type="text/javascript" src="js/components/dashboard/collection.js"></script>
	<script type="text/javascript" src="js/components/dashboard/displayDP.js"></script>

	<?php if($lb->isAdmin()): ?>
		<script type="text/javascript" src="js/components/dashboard/lpage.js"></script>
		
		<script type="text/javascript" src="js/components/dashboard/user.js"></script>
		<script type="text/javascript" src="js/components/dashboard/company-request.js"></script>
		<script type="text/javascript" src="js/components/dashboard/search-user.js"></script>
		<script type="text/javascript" src="js/components/dashboard/newuser.js"></script>
		<script type="text/javascript" src="js/components/dashboard/ainvoice.js"></script>
		<script type="text/javascript" src="js/components/dashboard/AdisplayDP.js"></script>
		<script type="text/javascript" src="js/components/dashboard/asset.js"></script>
		<script type="text/javascript" src="js/components/dashboard/apacking-lists.js"></script>
		<script type="text/javascript" src="js/components/dashboard/aorders.js"></script>


	<?php endif; 

		$handle=opendir("js/components/dashboard/");

		$adminF = array(".","..","user.js","company-request.js", "search-user.js", "newuser.js", "ainvoice.js", "asset.js", "apacking-lists.js","aorders.js","material.js","collection.js","lpage.js", "displayDP.js", "AdisplayDP.js");

		while (($file = readdir($handle))!==false): 
			$flag = false;
			foreach ($adminF as $key => $value) {
				
				if($value == $file){
					$flag = false;
					break;
				}else{
					$flag = true;
					
				}
			}

			if($flag){
				echo "<script type='text/javascript' src='js/components/dashboard/$file'></script>";
			}
		endwhile;
			closedir($handle);
		?>
		
	<script src="https://unpkg.com/@johmun/vue-tags-input@2.0.1/dist/vue-tags-input.js"></script>
	<script type="text/javascript" src="js/components/route-view.js"></script>
    <script type="text/javascript" src="js/dashboard.js"></script>

  </body>
</html>