<?php 
	include_once("controller/functions/elements.php");
	$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	//CORS Policy declatarion
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php 
    	include_once("reference.php");	
		luball_element("head.php","Urban City designs");

		if(!empty($_GET['order'])){
			echo "<script> const id = '".$_GET['order']."'</script>";
		}else{
			echo "<script> const id = null;</script>";
		}
	?>

	<meta property="og:type" content="website">

	<meta name="description" content="">
    
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/view.css">

	<!-- Components -->
	<style>
		
		@page {
		  margin: 2mm;
		}

	</style>
  </head>
  <body>
  	
	<div id="app">
		
		<b-navbar id="headerNav" class='luna-blue' fixed="top" toggleable="md" class="no-print" type="dark" :sticky=true variant="info">
			<b-navbar-nav class="ml-auto" right>
					<div class="padding">
						<b-button block @click="printPage" style="height: 100%;" class="btn-gold white-text">
							<i class="material-icons">print</i>
						</b-button>
					</div>
	   
	      	</b-navbar-nav>
		</b-navbar>
		
		<route-view></route-view>


	</div>

    <script type="text/javascript" src="js/spec-page.js"></script>

  </body>
</html>