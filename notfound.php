<?php 
	$mainName = "404";
	include_once("reference.php");
	
?>

<!DOCTYPE html>
<html>
  <head>
    <?php 	
		luball_element(reference."/head.php",$mainName);
	?>
    
    <script type="text/javascript" src="<?= reference.'/' ?>js/index.js" defer></script>

    <link rel="stylesheet" href="<?= reference."/" ?>css/index.css">
  </head>
  <body>
   
    <div class="parallax-container">
		<div class="parallax"><img src="<?= reference.'/' ?>media/images/wall.jpg"></div>
	</div>
	<div class="section " style="padding-top: 0px;">
		<ul class="tabs tecBlack card">
			<li class="tab col s3"><a href="#test1" class="text-white">Info</a></li>
			<li class="tab col s3"><a href="#test2" class="text-white">Escanear QR</a></li>
		</ul>
	</div>
	
	<main>
		
		<div class="row">
			<!-- <button id="open">Open</button>  -->
			<div id="test1" class="col s12">
				<div class="col s12">
					<div class="col s12 card center padding tecBlack ">
						<p class="white-text">Busca los codigos QR ubicados por el campus para obtener información</p>
					</div>
				</div>
			</div>
			<div id="test2" class="col s12">
				<video id="preview" class="" style="width: 100%; height: auto;"></video>
				
			</div>

			<div id="getPage" class="modal modal-fixed-footer modal-fullscreen">
				<div class="modal-content">
					<h4 id="title"></h4>
					<p class="grey-text" id="section"></p>
					<small  class="grey-text" id="created"></small>
					
					<div id="mapAsInput" style="width: 100%;height: 400px;"></div>

					<div class="col s12">
						<div id="content" class="textEditor"></div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
				</div>
			</div>
		</div>
		
	</main>

	<footer class="page-footer tecBlack">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<p>App TEC 2018 </p>
				</div>
			</div>
		</div>
	</footer>

  </body>
</html>
<!--
-->
<!-- https://luballSoftware.com -->