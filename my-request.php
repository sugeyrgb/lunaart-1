<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	
	$method = "";
	$page = true;

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
				//Get request handler
				switch ($_GET["g"]) {

					case "all":
						if($lb->isAdmin()){
							$sql = "SELECT *,  DATE_FORMAT(request.dated, '%m - %d - %y') as fdated, user.id as uid, request.img as img, request.id as id FROM request,user WHERE request.owner = user.id ORDER BY request.dated DESC";
						}else{
							$sql = "SELECT *, DATE_FORMAT(dated, '%m - %d - %y') as fdated FROM request WHERE owner = ".$_SESSION["user"]["id"];
						}
						
						$requests = $db->query($sql,true);

						foreach($requests["query"] as $key => $value){
							$requests["query"][$key]["content"] = json_decode($requests["query"][$key]["content"],true);
						}

						if($requests['status']){
							
							$lb->toClient(true,$requests["query"]);
						}else{
							$lb->defaultQuery();
						}
						
					break;

					case "one":
						if($lb->isAdmin()){
							$sql = "SELECT *,  DATE_FORMAT(request.dated, '%m - %d - %y') as fdated, user.id as uid, request.img as img, request.id as id FROM request,user WHERE request.owner = user.id AND request.id = ".$_GET['id']." ORDER BY request.dated DESC";
						}else{
							$sql = "SELECT *, DATE_FORMAT(dated, '%m - %d - %y') as fdated FROM request WHERE owner = ".$_SESSION["user"]["id"]." AND id = ".$_GET['id'];
						}
						
						$requests = $db->query($sql,true);

						foreach($requests["query"] as $key => $value){
							$requests["query"][$key]["content"] = json_decode($requests["query"][$key]["content"],true);
						}

						if($requests['status']){
							
							$lb->toClient(true,$requests["query"]);
						}else{
							$lb->defaultQuery();
						}
					break;

					default:
						$lb->defaultRequest();
					break;
				}
				
			break;

			case 'POST':
				//Post request handler
				switch ($_POST['request']) {

					case "update":
						
						if($lb->isAdmin()){
							if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){
								extract($_POST);
	
								$sql = "UPDATE `request` SET `assignedTo` = '".$assignedTo."' WHERE `request`.`id` = ".$id;

								$request = $db->query($sql,false);
	
								if($request["status"]){
									$lb->defaultSuccess();
								}else{
									$lb->defaultQuery();
								}
							}else{
								$lb->defaultEnough();
							}
						}else{
							$lb->defaultPermission();
						}
						
					break;
					
					case "delete":

						if($lb->isAdmin()){
							extract($_POST);

							$sql = "DELETE FROM `request` WHERE `request`.`id` = ".$id;
							
							$request = $db->query($sql,false);
	
							if($request["status"]){
								$lb->defaultSuccess();
							}else{
								$lb->defaultQuery();
							}
						}

					break;

					case "add":
						
						$order = json_decode( $_POST["form"] , true);
						unset($_POST["form"]);
						
						unset($order["materials"]);
						unset($order["pattern"]["preview"]);
						unset($order["pattern"]["finish"]);
						
/*						print_r($order);
						print_r($_POST);
						print_r($_FILES);
*/
						extract($_POST);

						if(isset($_POST["custom"])){
							if(!isset($_FILES["preview"])){
								$lb->toClient(false,$callback,"NotPreview");
								return false;
							}

							if(empty($order['details']['materials'])){
								$lb->toClient(false,$callback,"NotEnoughMaterials");
								return false;
							}

							
							$times_hash = $lb->encrypt(time());
							$setPreview = $lf->mf($_FILES["preview"],"files/".$lb->encrypt($_SESSION['user']['id'])."/request/".$times_hash,"preview.jpg");
							$img = $setPreview['dir_name'];

							$setSql = "INSERT INTO request (content,
															custom,
															img,
															pdf,
															owner,
															comment)
														VALUES ('".json_encode($order)."',
																1,
																'".$img."',
																0,
																".$_SESSION['user']['id'].",
																'".$order['details']['comments']."')";
	
							$query = $db->query($setSql,false,true);
							
							if($query["status"]){
								$lb->defaultSuccess();
							}else{
								$lb->defaultQuery();
							}
						}else{
							if(!empty($order)){
								$zp = (empty($order["zp"]) ? $_SESSION["user"]["zp"] : $order["zp"] );
								
								if($imgType == "true"){
	
									/* You'll handle as you can */
									$lb->toClient(false,"Lalas");
									return false;
								}else{
									$img = $preview;
								}
	
								unset($order["pattern"]["displayArray"]);
	
								$setSql = "INSERT INTO request (content,
																img,
																pdf,
																owner,
																comment)
														VALUES ('".json_encode($order)."',
																'".$img."',
																1,
																'".$_SESSION['user']['id']."',
																'".$comment."')";
	
								$query = $db->query($setSql,false,true);
								
								if($query["status"]){
									$lb->defaultSuccess();
	
									$db->query("DELETE FROM mysaves WHERE id = ".$order["mySaveId"]." AND owner = ".$_SESSION['user']['id'],false);
								}else{
									$lb->defaultQuery();
								}
							}
						}

						
						
					break;

					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT
				$lb->defaultRequest();
			break;

			case 'DELETE':
				//Post handled like Delete
				$lb->defaultRequest();
			break;
			
			default:
				echo "{'error':'Bad server request'}";
			break;
		}
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	

	//Third FOM MySQL Server connection 

	if(!$page){
		$FriendofMySQL = new FriendofMySQL("root","login",$method); 
		if($FriendofMySQL->callback != "false"){ 
			echo json_encode($FriendofMySQL->callback); 
		}else{ 
			echo "{'error':false}"; 
		} 
	}
	

?>