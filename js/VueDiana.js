/*!
 * Diana Vuejs (https://luballsoftware.com)
 * Copyright 2017-2019 Materialize
 * MIT License
 */

class VueDiana{

	constructor(){
		console.log("VueDiana has been loaded");
	}

	isset(foo){
		if(foo == null){
			return false;
		}else{
			return true;
		}
	}

	emptyStr(foo){
		if(foo === ""){
			return true;
		}else{
			return false;
		}
	}

	dataURLtoFile(dataurl, filename) {
	    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
	        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
	    while(n--){
	        u8arr[n] = bstr.charCodeAt(n);
	    }
	    return new File([u8arr], filename, {type:mime});
	}


	b64toBlob(b64Data, contentType, sliceSize) {
	        contentType = contentType || '';
	        sliceSize = sliceSize || 512;

	        var byteCharacters = atob(b64Data);
	        var byteArrays = [];

	        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
	            var slice = byteCharacters.slice(offset, offset + sliceSize);

	            var byteNumbers = new Array(slice.length);
	            for (var i = 0; i < slice.length; i++) {
	                byteNumbers[i] = slice.charCodeAt(i);
	            }

	            var byteArray = new Uint8Array(byteNumbers);

	            byteArrays.push(byteArray);
	        }

	      var blob = new Blob(byteArrays, {type: contentType});
	      return blob;
	}

	b64toBlob2(dataURI) {

		var byteString = atob(dataURI.split(',')[1]);
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
	
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}
		return new Blob([ab], { type: 'image/svg+xml' });
	}

	isSnE(foo){	
		//
		if(this.isset(foo) && !this.emptyStr(foo)){
			return true;
		}else{
			return false;
		}
	}

	allStorage(){
		var archive = {}, 
			keys = Object.keys(localStorage),
			i = keys.length;
		while (i--) {
			archive[keys[i]] = localStorage.getItem(keys[i]);
			archive[""];
		}
		return archive;
	}

	toastError(title="Error",body="Something has happen, please contact with the Admin"){
		iziToast.show({
			timeout: 3000,
			title: title,
			message: body,
			color: "#F8D7DA",
			progressBar: false
		});
	}

	toastNotEnough(title="",body="Must fill all required fields"){
		iziToast.show({
			timeout: 3000,
			title: title,
			message: body,
			color: "#F8D7DA",
			progressBar: false
		});
	}

	toastSuccess(title="",body="Done"){
		iziToast.show({
			title: title,
			message: body,
			class: "success-white",
			backgroundColor: "#B99168",
			color: "white",
			timeout: 3000,
			progressBar: false
		});
	}

	toastConfirming(title="",body="",okText="Ok"){
		iziToast.show({
			icon: 'icon-person',
			title: title,
			message: body,
			class: "toastConfirming",
			close: false,
			timeout: 30000,
			position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
			buttons: [
				['<button class="btn btn-gold white-text">'+okText+'</button>', function (instance, toast) {
					instance.hide({
						transitionOut: 'fadeOutUp',
						onClosing: function(instance, toast, closedBy){
							console.info('closedBy: ' + closedBy); // The return will be: 'closedBy: buttonName'
						}
					}, toast, 'buttonName');
				}]
			],
			onOpening: function(instance, toast){
				console.info('callback abriu!');
			},
			onClosing: function(instance, toast, closedBy){
				console.info('closedBy: ' + closedBy); // tells if it was closed by 'drag' or 'button'
			}
		});
	}

}