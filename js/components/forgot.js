const ForgotComponent = Vue.component("forgot",{
	data:function(){
		return {}
	},
 	template: `<div class=padding>
				  <b-container v-if="step==1">
				  	<b-row>
					  	<b-col>
					  		<p>Please give us an email linked to the account that you want to recover</p>
					  	</b-col>
				  	</b-row>
					
					<b-row v-if="errors.length">
						<b-col sm=12 v-for="error in errors">
							<b-alert variant="danger" show>{{ error.message }}</b-alert>
						</b-col>
					</b-row>

				  	<b-form>
				  		<b-input type="email" autocomplete=off v-model="form.email" placeholder="EMAIL"/>
				  	</b-form>
				  </b-container>

				  <b-container v-if="step==2">
				  	<b-row>
					  	<b-col>
					  		<p>Write the code sent to your email</p>
					  	</b-col>
				  	</b-row>
					
					<b-row v-if="errors.length">
						<b-col sm=12 v-for="error in errors">
							<b-alert variant="danger" show>{{ error.message }}</b-alert>
						</b-col>
					</b-row>

				  	<b-form>
				  		<b-input type="number" autocomplete="off" v-model="form.code" placeholder="CODE"/>
				  	</b-form>
				  </b-container>

				  <b-container v-if="step==3">
				  	<b-row>
					  	<b-col>
					  		<p>Choose your new password</p>
					  	</b-col>
				  	</b-row>
					
					<b-row v-if="errors.length">
						<b-col sm=12 v-for="error in errors">
							<b-alert variant="danger" show>{{ error.message }}</b-alert>
						</b-col>
					</b-row>

				  	<b-form>
				  		<b-input type="password" autocomplete="off" v-model="form.pass1" placeholder="NEW PASSWORD"/>
				  		<b-input type="password" autocomplete="off" v-model="form.pass2" placeholder="CONFIRM PASSWORD"/>
				  	</b-form>
				  </b-container>

				  <b-container v-if="step==4">
				  	<b-row>
					  	<b-col class="text-center padding">
					  		<p class="luna-text-lightGrey">Your password has succesfully updated, please login with your new account</p>
					  	</b-col>
				  	</b-row>
				  </b-container>

				  <b-container v-if="step!=4">
					<b-row>

					  	<b-col offset=9 sm=3 class=padding>
					  		<b-button type="button" block class="btn blue-button" @click=send>Next</b-button>
					  	</b-col>
				  	</b-row>
				  	<b-row class="text-center padding" v-if=loading>
						<b-col>
							<l-spinner color="luna-text-gold"></l-spinner>
						</b-col>
					</b-row>
				  </b-container>
				</div>`,
	data() {
		return {
			step: 1,
			errors: [],
			loading: false,
			form: {
				email: null,
				code: null,
				pass1: null,
				pass2: null
			},
			request: "login"
		}
	},

	methods: {
		send(e){
			e.preventDefault();

			let main = this;
			main.loading = true;
			console.log("clic")

			var vd = new VueDiana();

			console.log(this.form);
			if(main.step == 1){
				var theForm = new FormData();

				theForm.append("request","restore");
				theForm.append("method","post");

				theForm.append("email",main.form.email);

				axios({
				    method: 'post',
				    url: 'login',
				    data: theForm,
				    config: { headers: {'Content-Type': 'multipart/form-data' }}
			    }).then(function (response) {
			        //handle success
			        console.log(response);
			        console.log(response.data);

			        let call = response.data.call;

			        main.errors = [];

			       	switch(call){

			       		case true:
			       			main.step = 2;
			     
			       		break;

			       		case false:
				       		switch(response.data.errors){
				       								
								case "NotEnoughForm":
				       				main.errors.push({message:"Must fill all fields in form"});
				       			break;

				       			case "NoEmail":
				       				main.errors.push({message:"This email isn't linked to any account"});
				       			break; 

				       			default:
				       				vd.toastError();
				       			break;
				       		}
			       		break;
			       		
			       		default:
			       			vd.toastError();
			       		break;
			       	}
			    }).catch(function (response) {
			        //handle error
			        console.log(response);
			    }).then(function () {
					main.loading = false;
				}); 	
			}else if(main.step == 2){

				var theForm = new FormData();

				theForm.append("request","code");
				theForm.append("method","post");

				theForm.append("email",main.form.email);
				theForm.append("code",main.form.code);

				axios({
				    method: 'post',
				    url: 'login',
				    data: theForm,
				    config: { headers: {'Content-Type': 'multipart/form-data' }}
			    }).then(function (response) {
			        //handle success
			        console.log(response);
			        console.log(response.data);

			        let call = response.data.call;

			        main.errors = [];

			       	switch(call){

			       		case true:
			       			main.step = 3;
			       		break;

			       		case false:
				       		switch(response.data.errors){
				       								
								case "NotEnoughForm":
				       				main.errors.push({message:"Must fill all fields in form"});
				       			break;

				       			case "NotCode":
				       				main.errors.push({message:"Code is incorrect"});
				       			break; 

				       			default:
				       				vd.toastError();
				       			break;
				       		}
			       		break;
			       		
			       		default:
			       			vd.toastError();
			       		break;
			       	}
			    }).catch(function (response) {
			        //handle error
			        console.log(response);
			    }).then(function () {
					main.loading = false;
				}); 
			}else if(main.step == 3){
				var theForm = new FormData();

				theForm.append("request","reset");
				theForm.append("method","post");

				theForm.append("email",main.form.email);
				theForm.append("pass1",main.form.pass1);
				theForm.append("pass2",main.form.pass2);

				axios({
				    method: 'post',
				    url: 'login',
				    data: theForm,
				    config: { headers: {'Content-Type': 'multipart/form-data' }}
			    }).then(function (response) {
			        //handle success
			        console.log(response);
			        console.log(response.data);

			        let call = response.data.call;

			        main.errors = [];

			       	switch(call){

			       		case true:
			       			main.step = 4;
			   
			       		break;

			       		case false:
				       		switch(response.data.errors){
				       								
								case "NotEnoughForm":
				       				main.errors.push({message:"Must fill all fields in form"});
				       			break;

				       			case "NotPassMatch":
				       				main.errors.push({message:"Passwords doesn't match"});
				       			break; 

				       			case "RestoreExpired":

				       			break;

				       			default:
				       				vd.toastError();
				       			break;
				       		}
			       		break;
			       		
			       		default:
			       			vd.toastError();
			       		break;
			       	}
			    }).catch(function (response) {
			        //handle error
			        console.log(response);
			    }).then(function () {
					main.loading = false;
				}); 
			}else if(main.step == 4){
				setTimeout(function() {
					this.$root.updateRoute("/");
				}, 5000);
				

			}
       		
		}
	}
});