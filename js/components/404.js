const NotFound = Vue.component("not-found",{
 	template: `<div class="nav-padder">
					<b-container>
						<b-row class="padding-"><b-col>Page not found</b-col></b-row>
					</b-container>
				</div>`
});

