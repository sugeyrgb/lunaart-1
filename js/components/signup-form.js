const signupComponent = Vue.component("signup-form",{
	props: {
		contact:{
			default: true
		},
		email: "no-reply@urbancitydesigns.com"
	},
	template: 
	`	<div>
			<div v-if="accountType == null && !contact">
				<b-row>
					<b-col sm=12 style="height: 60px; padding-bottom: 5px;">
						<b-button style="width: 100%; height: 100%;" class="golden-border" @click="accountType = 1">SIGN UP AS A GUEST</b-button>
					</b-col>

					<b-col sm=12 style="height: 60px;">
						<b-button style="width: 100%; height: 100%;" class="btn-primary" @click="accountType = 2">SIGN UP AS A COMPANY</b-button>
					</b-col>
				</b-row>
			</div>
			
			<!-- Form Guest -->
			<div v-if="accountType == 1">
				<b-button class="btn-flat" @click="accountType = null" v-if="!contact">
					<i class="material-icons">
						keyboard_backspace
					</i>
				</b-button>
				<b-form id="formId" @submit="preventSubmit">
					
					<b-form-group>
		
						<b-row v-if="errors.length">
							<b-col sm=12 v-for="error in errors">
								<b-alert variant="danger" show>{{ error.message }}</b-alert>
							</b-col>
						</b-row>
		
						<b-row>
							<b-col sm=12>
								<b-input class="mb-2 mr-sm-2 mb-sm-0" name="signupName" v-model="form.fname" placeholder="FIRST NAME" autocomplete="off" />
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
									<b-input v-model="form.sname" placeholder="LAST NAME" autocomplete="off" />
								</b-input-group>
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
									<b-input v-model="form.email" placeholder="EMAIL" autocomplete=email type="email" />
								</b-input-group>
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
									<b-input v-model="form.phone" placeholder="PHONE NUMBER" autocomplete="off"  />
								</b-input-group>
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
									<b-input v-model="form.address" placeholder="ADDRESS" autocomplete="off"  />
								</b-input-group>
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
									<b-input v-model="form.city" placeholder="CITY" autocomplete=off />
								</b-input-group>
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
									<b-form-select v-model="form.stateS">
								      <option v-for='state in states' :value='state.abbreviation'>
										{{ state.name }}
								      </option>
								    </b-form-select>
								</b-input-group>
							</b-col>
		
							<b-col sm=12>
								<b-input-group class="mb-2 mr-sm-2 mb-sm-0" autocomplete="off" >
									<b-input v-model="form.zp" placeholder="ZIPCODE" type=number />
								</b-input-group>
							</b-col>
							
							<b-col sm=12  v-if="contact == true">
								<b-input-group>
									<b-form-textarea
									v-model="form.fmessageC"
									placeholder="MESSAGE"
									rows="3"
									/>
								</b-input-group>
							</b-col>
		
							<div class="row" style="padding-left: 30px;" v-else>
								<b-col sm=12 style="padding-left: 0px; padding-right: 0px;">
									<b-input-group style="width:100%;">
										<b-input type="password" autocomplete="new-password" v-model="form.fpass" placeholder="PASSWORD" />
									</b-input-group>
								</b-col>
		
								<b-col sm=12 style="padding-left: 0px; padding-right: 0px;">
									<b-input-group style="width:100%;">
										<b-input type="password" autocomplete="new-password" v-model="form.spass" placeholder="COMFIRM PASSWORD" />
									</b-input-group>
								</b-col>
							</div>
											
						</b-row>
						
						<b-row>
							<b-col sm=12>
								<b-button type="submit" block class="golden-border" variant="none">{{ contact ? "SEND MESSAGE" : "SIGN UP"}}</b-button>
							</b-col>
						</b-row>
		
						<b-row class="text-center padding" v-if=loading>
							<b-col>
								<l-spinner color="luna-text-gold"></l-spinner>
							</b-col>
						</b-row>
					</b-form-group>
				</b-form>
				<!-- Form guest end -->
			</div>

			<div v-if="accountType == 2">
				<b-button class="btn-flat" @click="accountType = null; aplicating = false">
					<i class="material-icons">
						keyboard_backspace
					</i>
				</b-button>

				<b-col sm=12 v-if="aplicating == false" style="height: 70px; padding-top: 5px">
					<b-button class="btn-gold" style="width: 100%; height: 100%;" @click="initCompany">APPLY NOW</b-button>
				</b-col>

				<div v-else>
					<b-form class="companyRequest-form">
						<b-row>
							<h4>Company information</h4>
						</b-row>
						
						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Legal Company Name: </label>
							<div class="col">
								<b-input type="text" class="form-control" v-model="formCompany.companyName" :class="( formCompany.companyName === '' ? 'is-invalid' : 'is-valid')" />
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-2 col-form-label">D.B.A </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.dba" :class="( formCompany.dba === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-4 col-form-label">Business Address: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.businessAddress" :class="( formCompany.businessAddress === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-1 col-form-label">City: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.city" :class="( formCompany.city === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<b-row>
							<b-col>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">State: </label>
									<div class="col">
										<b-form-select v-model="formCompany.state">
											<option v-for='state in states' :value='state.abbreviation'>
												{{ state.name }}
											</option>
										</b-form-select>
									</div>
								</div>
							</b-col>
							<b-col>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Zip code: </label>
									<div class="col">
										<input type="number" min="0" class="form-control" v-model="formCompany.companyZp" :class="( formCompany.companyZp === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
						</b-row>

						<b-row>
							<b-col>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Phone: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.phone" :class="( formCompany.phone === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>

							<b-col>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Fax: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.fax" :class="( formCompany.fax === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
						</b-row>
						
						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Website: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.website" :class="( formCompany.website === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<b-row>&nbsp;</b-row>

						<b-row>
							<h4>Shipping Information</h4>
						</b-row>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Shipping Address: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.shippingAddress" :class="( formCompany.shippingAddress === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">City: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.shippingCity" :class="( formCompany.shippingCity === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<b-row>
							<b-col>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">State: </label>
									<div class="col">
										<b-form-select v-model="formCompany.shippingState">
											<option v-for='state in states' :value='state.abbreviation' :class="( formCompany.abbreviation === '' ? 'is-invalid' : 'is-valid')">
												{{ state.name }}
											</option>
										</b-form-select>
									</div>
								</div>
							</b-col>
							<b-col>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Zip code: </label>
									<div class="col">
										<input type="number" min="0" class="form-control" v-model="formCompany.shippingZp" :class="( formCompany.shippingZp === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
						</b-row>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Contact: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.shippingContact" :class="( formCompany.shippingContact === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Phone: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.shippingPhone" :class="( formCompany.shippingPhone === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<b-row>
							<h4>Contact Information</h4>
						</b-row>

						<b-row >
							<b-col sm=12>
								Main Contact/Owner:
							</b-col>
							
							<b-col sm=6>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Name: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.contactName" :class="( formCompany.contactName === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
							<b-col sm=6>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Lastname: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.contactLastname" :class="( formCompany.contactLastname === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
						</b-row>
						
						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Title: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.contactTitle" :class="( formCompany.contactTitle === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Phone: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.contactPhone" :class="( formCompany.contactPhone === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">E-mail: </label>
							<div class="col">
								<input type="email" class="form-control" v-model="formCompany.contactEmail" :class="( formCompany.contactEmail === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Accounting Contact: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.contactAccounting" :class="( formCompany.contactAccounting === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<b-row>
							<b-col sm=6>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Phone: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.accountingPhone" :class="( formCompany.accountingPhone === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
							<b-col sm=6>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">E-mail: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.accountingEmail" :class="( formCompany.accountingEmail === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
						</b-row>

						<b-row>&nbsp;</b-row>

						<div role="group" class="form-row form-group">
							<label class="col-5 col-form-label">Authorized sales staff: </label>
							<div class="col">
								<input type="text" class="form-control" v-model="formCompany.authorizedSalesStaff" :class="( formCompany.authorizedSalesStaff === '' ? 'is-invalid' : 'is-valid')">
							</div>
						</div>

						<b-row>
							<p>
								Sales Tax:
								<br><br>
								If you receive material from Urban City Development (UCD) in the states of NJ and CA, UCD is
								exceptions can be made.
								<br><br>
								By submitting this application, I, the undersigned, certify that all information on this application
								is accurate and true to the best of my knowledge.<br>
								Furthermore, I, the undersigned, agree to the <a href="/eula" target="_blank">Terms and Conditions</a> and understand our policies.
							</p>
						</b-row>

						<b-row>
							<b-col sm=7>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Printed Name: </label>
									<div class="col">
										<input type="text" class="form-control" v-model="formCompany.printedName" :class="( formCompany.printedName === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
							<b-col sm=5>
								<div role="group" class="form-row form-group">
									<label class="col-5 col-form-label">Title: </label>
									<div class="col">
										<input type="text" min="0" class="form-control" v-model="formCompany.title" :class="( formCompany.title === '' ? 'is-invalid' : 'is-valid')">
									</div>
								</div>
							</b-col>
						</b-row>

						<b-row>
							<b-col>
								<canvas id="signature-pad" class="signature-pad" width=400 height=200></canvas>
								<b-button class="btn-flat" @click="clearSign">
									<i class="material-icons">
										refresh
									</i>
								</b-button>
								<small>Please sign here by using the mouse cursor, stylus pen or finger</small>
							</b-col>
						</b-row>

						<b-row>
							<b-col>
								<b-button @click="submitCompany" class="btn-gold btn">SUBMIT</b-button>
							</b-col>
						</b-row>

						<b-row class="text-center padding" v-if=loading>
							<b-col>
								<l-spinner color="luna-text-gold"></l-spinner>
							</b-col>
						</b-row>

						<b-button @click="saveTest">Test</b-button>
					</b-form>
				</div>
			</div>
		</div>
	  `,
	data() {
		return {
			accountType: null,
			aplicating: false,
			formId: null,
			states: [{name: "STATE", abbreviation: null}],
			errors: [],
			loading: false,
			form: {
				fname: null,
				sname: null,
				email: null,
				phone: null,
				address: null,
				city: null,
				stateS: null,
				zp: null,
				fpass: null,
				spass: null,
				fmessageC: ""
			},

			formCompany: {
				companyName: undefined,
				dba: undefined,
				businessAddress: undefined,	
				city: undefined,
				state: undefined,
				companyZp: undefined,
				phone: undefined,
				fax: undefined,
				website: undefined,

				shippingAddress: undefined,
				shippingCity: undefined,
				shippingState: undefined,
				shippingZp: undefined,
				shippingContact: undefined,
				shippingPhone: undefined,

				contactName: undefined,
				contactLastname: undefined,
				contactTitle: undefined,
				contactPhone: undefined,
				contactEmail: undefined,
				contactAccounting: undefined,
				accountingPhone: undefined,
				accountingEmail: undefined,
				authorizedSalesStaff: undefined,
				
				printedName: undefined,
				title: undefined
			},

			initalizeVar: false,
			signaturePad: null,
			canvas: document.getElementById('signature-pad'),
			vd: new VueDiana()
		}
	},
	created(){

		let main = this;
		axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8303e1d831e89cb8af24a11f2fa77353c317e408/states_titlecase.json').then(function(response){
	      let lalas = response.data;
	      for(var key in response.data){
	      	main.states.push(response.data[key]);
	      }
	    }).catch(function(error){
	      console.log(error);
	    });
	}, 
	mounted(){
		
		if(this.contact){
			this.accountType = 1;
		}
		
	},
	methods: {
		initCompany(){
			var main = this;
			this.aplicating = true;

			setTimeout(function(){
				main.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
					backgroundColor: 'rgba(255, 255, 255, 0)',
					penColor: 'rgb(0, 0, 0)'
				});
	
				window.addEventListener("resize", main.resizeCanvas);
				window.addEventListener("scroll", main.initalizePad);
			},500);
			
			
		},
		validation(event, options = { required: true }){
			console.log(event);
			/*
			if(options.required){
				if(event.target.value == ""){
					if(event.target.className == "is-valid"){
						event.target.className = "is-invalid";
					}else{
						event.target.className = "is-valid";
					}
					return false;
				}	
			}

			return true;*/
		},
		saveTest(e){
			this.formCompany = {
				companyName: "LuballSandwich",
				dba: "dba",
				businessAddress: "An street",	
				city: "Tijuana",
				state: "BC",
				companyZp: 22222,
				phone: "664 780 9152",
				fax: "664 780 9152",
				website: "https://luballsoftware.com",

				shippingAddress: "Another street",
				shippingCity: "Culiacan",
				shippingState: "SL",
				shippingZp: 88888,
				shippingContact: "Susana Oria",
				shippingPhone: "664 780 9152",

				contactName: "Irma",
				contactLastname: "Legaspi",
				contactTitle: "CEO",
				contactPhone: "664 780 9152",
				contactEmail: "jaja.lorem@gmail.com",
				contactAccounting: "Elba Zurita",
				accountingPhone: "667 111 2547",
				accountingEmail: "accounting@gmail.com",
				authorizedSalesStaff: "Elsa Pato",

				printedName: "Adrian Legaspi",
				title: "CEO"
			}
		},
		submitCompany(e){
			
			e.preventDefault();
			var canvas = document.getElementById("signature-pad");
				canvas.classList.remove('is-invalid');
			let main = this;
			main.loading = true;

			var signupForm = new FormData();

			var vd = new VueDiana();
			var flag = false;
			for(var input in this.formCompany){
				if(this.formCompany[input] == undefined){
					this.formCompany[input] = "";
					flag = true;
				}
				signupForm.append(input,this.formCompany[input]);
			}

			if(!this.signaturePad.isEmpty()){
				signupForm.append("signature",main.vd.b64toBlob2(this.signaturePad.toDataURL("image/svg+xml")));
			}else{
				canvas.classList.add('is-invalid');
			}

			
			signupForm.append("request","signupCompany");
			signupForm.append("method","post");

			console.log(this.formCompany);


       		axios({
			    method: 'post',
			    url: 'login',
			    data: signupForm,
			    config: { headers: {'Content-Type': 'multipart/form-data' }}
		    }).then(function (response) {
		        //handle success
		        console.log(response);
		        console.log(response.data);

		        let call = response.data.call;

		        main.errors = [];

		       	switch(call){

		       		case true:
						main.vd.toastConfirming("","Thank you for applying to be part of Urban City Designs. Your information is being reviewed and will be sending you an e-mail with your username and password briefly", "UNDERSTOOD");

						main.formCompany = {
							companyName: undefined,
							dba: undefined,
							businessAddress: undefined,	
							city: undefined,
							state: undefined,
							companyZp: undefined,
							phone: undefined,
							fax: undefined,
							website: undefined,
			
							shippingAddress: undefined,
							shippingCity: undefined,
							shippingState: undefined,
							shippingZp: undefined,
							shippingContact: undefined,
							shippingPhone: undefined,
			
							contactName: undefined,
							contactLastname: undefined,
							contactTitle: undefined,
							contactPhone: undefined,
							contactEmail: undefined,
							contactAccounting: undefined,
							accountingPhone: undefined,
							accountingEmail: undefined,
							authorizedSalesStaff: undefined,
							
							printedName: undefined,
							title: undefined
						};

						main.clearSign();
						main.$root.$refs.loginModal.hide()
		       		break;

		       		case false:
			       		switch(response.data.errors){

			       			case "EmailExist":
								vd.toastError("","That contact email belongs to another account");
			       			break; 

			       			case "NotEnoughForm":
								vd.toastError("","You must fill al the field to complete the request");
			       			break;

			       			case "InvalidEmail":
			       				main.errors.push({message:"Invalid email"});
			       			break;

			       			default:
			       				vd.toastError();
			       			break;
			       		}
		       		break;
		       		
		       		default:
		       			vd.toastError();
		       		break;
		       	}
		    }).catch(function (response) {
		        //handle error
		        console.log(response);
		    }).then(function () {
				main.loading = false;
			});  
		
		},
		clearSign(){
			this.signaturePad.clear();
		},
		resizeCanvas() {
			var canvas = document.getElementById('signature-pad');
			var ratio =  Math.max(window.devicePixelRatio || 1, 1);
			canvas.width = canvas.offsetWidth * ratio;
			canvas.height = canvas.offsetHeight * ratio;
			canvas.getContext("2d").scale(ratio, ratio);
			this.signaturePad.clear(); // otherwise isEmpty() might return incorrect value
		},
		initalizePad(){
			var main = this;
			if(this.initalizeVar){
				main.resizeCanvas();
				this.initalizeVar = true;
			}
			
		},
		preventSubmit(e){
			e.preventDefault();

			let main = this;
			main.loading = true;
			console.log("clic")

<<<<<<< HEAD
			var signupForm = new FormData();

			var vd = new VueDiana();
			
=======
			var form = document.getElementById(this.formId);
			var signupForm = new FormData(form);

			var vd = new VueDiana();

			console.log(form)
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
       		console.log(signupForm.get("signupLastname"));

       		let request = (this.contact ? "contact" : "signup");

			signupForm.append("request",request);
			signupForm.append("method","post");

			console.log(this.form);

			for(var input in this.form){
				signupForm.append(input,this.form[input]);

			}

       		axios({
			    method: 'post',
			    url: 'login',
			    data: signupForm,
			    config: { headers: {'Content-Type': 'multipart/form-data' }}
		    }).then(function (response) {
		        //handle success
		        console.log(response);
		        console.log(response.data);

		        let call = response.data.call;

		        main.errors = [];

		       	switch(call){

		       		case true:
		       			if(main.contact){
		       				vd.toastSuccess("","Thanks for stay in touch with us!");
		       			}else{
		       				vd.toastSuccess("","Now you can login");
		       			}
		       			
		       		break;

		       		case false:
			       		switch(response.data.errors){

			       			case "NotPassMatch":
			       				vd.toastError("Error","Your passwords doesn't match");
			       				main.errors.push({message:"Passwords doesn't match"});
			       			break; 

			       			case "EmailExist":
			       				main.errors.push({message:"That email is already in use"});
			       			break; 

			       			case "NotEnoughForm":
			       				main.errors.push({message:"Must fill all fields in form"});
			       			break;

			       			case "InvalidEmail":
			       				main.errors.push({message:"Invalid email"});
			       			break;

			       			default:
			       				vd.toastError();
			       			break;
			       		}
		       		break;
		       		
		       		default:
		       			vd.toastError();
		       		break;
		       	}
		    }).catch(function (response) {
		        //handle error
		        console.log(response);
		    }).then(function () {
				main.loading = false;
			});  
		}
	}
});
