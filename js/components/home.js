const HomeComponent = Vue.component("home",{
 	template: `
 	<div>
 		<div>
			<l-carousel></l-carousel>
		</div>
	
		<b-container >
			
			<b-row style="padding-bottom: 20px;">
				<b-col sm=12>
					<p class='text-center luna-text-lightGrey home-greyp'>
						<br>

						We do not just connect you to stone and art, we bring your imagination to life. <br><br>


						We are committed to producing the highest quality of custom stone work and mosaics for
						commercial and residential projects, crafted by hand, from the finest materials around the world.<br><br>


						Whether fabricating a lavish backsplash or one of a kind medallion, our staff works around
						the clock in making clients dreams a reality. By assisting you throughout the design and fabrication
						process, we keep close contact every step of the way as you watch your design come alive.
						<br><br>
					</p>
				</b-col>

				<b-col sm=12 class="text-center">
					<b-button class='btn-gold' @click="openRoute('/about')">READ MORE <i class="fas fa-chevron-right"></i></b-button>
<<<<<<< HEAD
					<br><br>
=======
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
				</b-col>
			</b-row>

		</b-container>

		<b-container fluid style="padding-left: 25px; padding-right: 25px">
			<b-row>
				<b-col sm=12 md=6 class='no-padding'>
					<b-col class="home-card small-home-1" sm="12" style="padding-top: 30px;">
<<<<<<< HEAD
						<button class="gold-link home-gold-link" @click="$root.updateRoute('collections')">COLLECTIONS <i class="fas fa-chevron-right"></i></button>
					</b-col>
				
					<b-col class="home-card small-home-2" sm="12" style="padding-top: 30px;">
						<button class="gold-link home-gold-link" @click="$root.updateRoute('materials')">MATERIALS <i class="fas fa-chevron-right"></i></button>
=======
						<button class="gold-link" @click="$root.updateRoute('collections')">COLLECTIONS <i class="fas fa-chevron-right"></i></button>
					</b-col>
				
					<b-col class="home-card small-home-2" sm="12" style="padding-top: 30px;">
						<button class="gold-link" @click="$root.updateRoute('materials')">MATERIALS <i class="fas fa-chevron-right"></i></button>
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
					</b-col>
				</b-col>
								
				<b-col sm=12 md=6 class='noSidesPadding large-home my-auto text-center'>
					<b-button class="white gold-flat-text" @click="testLogin">SHOP NOW <i class="fas fa-chevron-right"></i></b-button> <br>
					<span style="color: #7D8181;">All Items in our cart will be priced by our
local dealer unless a specific dealer is selected</span>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid class="noSidesPadding">
			<br>
			<video src="media/video/video.mp4" controls="play" autoplay controlsList="nodownload noseek nofullscreen" class="home-video"></video>
		</b-container>

		<b-container fluid>
			<b-row>
				<b-col class='text-center luna-text-gold' style="padding: 45px;">
					<h3 class='craft-title elmessiri'>#CRAFTINGUNIQUESURFACES</h3>
				</b-col>
			</b-row>

			<b-row>
				<b-col class='text-center luna-text-gold'>

					<b-card 
					    overlay
					    title="Availability"
					    img-src="media/cards/3.webp"
					    img-alt="Card Image"
					    text-variant="white"
					    class='home-cardBot margin-middle availability-card full-width'>
					    <p class='text-center align-middle '>
					      <span class="luna-text-blue font-regular">Our products are available through hundreds of exclusive outlets across the country and
						  featured internationally in hotels, retails stores, shoping centers, office buildings, hospitals,
						  stadiums, museums and prominent residential homes.</span>
					    </p>
					  </b-card>
				</b-col>
			</b-row>
		</b-container>
 	</div>`,
 	data(){
		return {}
	},
	methods:{
		testLogin(){
			if(this.$root.$data.session.status){
				window.open("dashboard","_self");
			}else{
				this.$root.$refs.loginModal.show();
			}
		},
		openRoute(route){
			this.$root.updateRoute(route);
		}
	}
});