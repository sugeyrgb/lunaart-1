const ContactComponent = Vue.component("contact",{
	data:function(){
		return {}
	},
 	template: 
 	`<div class='nav-padder'>
		<b-container fluid>
			<b-row>
				<b-col sm=12><h4 class='font-boldy elmessiri gold-flat-text component-title'>Contact</h4></b-col>
			</b-row>
			<b-row>
				<b-col sm=12 offset-md=1 md=5 style="border-right: 1px solid #BF9B76; padding-right: 5%;">
					<signup-form :contact="true"></signup-form>
				</b-col>

				<b-col sm=12 md=5 style="padding-left: 5%;">
					<b-row class="contact-row">
						<b-col sm=12>
							<h4>HEADQUARTES</h4>
							<p>2907 Central Ave, <br>
							Union City, NJ 07087</p>
						</b-col>

						<b-col sm=12>
							<h4>WEST COAST LOCATION</h4>
							<p> 2364 Paseo de las Americas <br>
							#104-227 San Diego, CA 92154 <br>
							</p>
						</b-col>

						<b-col sm=12>
							<h4>INCOMING</h4>
							<p class='gold-flat-text'>Appointment Only</p>
							<p>2364 Paseo de las Americas <br>
							#104-227 San Diego, CA 92154 </p>
							<br>
							<p>Telephone: +1 (201) 840 1442 <br>
							FAX: +1 (201) 221 8098 <br>
							E-mail: info@urbancitydesigns.com</p>
						</b-col>

					</b-row>
				</b-col>
			</b-row>
		</b-container>		
 	</div>`
});