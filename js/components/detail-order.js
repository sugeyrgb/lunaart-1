const DetailOrderComponent = Vue.component("detail-order",{
 	template: 
 	`<div class='nav-padder'>
 		<b-container  v-if="!orderDone" class="padding">
 			<b-row>
	          <b-col>
	            <h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text">Detail order</h4>
	          </b-col>
	        </b-row>
	 		<b-row>
				<b-col sm=6 style="padding-right: 0px;">
					<b-form-group label="Sheets: " v-if="focusQ" >
						<b-input-group >
							<b-form-input type="number" @input="setS($event)" :value="quantity"  step="1"></b-form-input>
						</b-input-group>

					</b-form-group>
					
					<b-form-group label="Sheets: " v-else>
						<b-card no-body @click="focusQ = true; focusSF = false" class="text-center ">
							
							<div class="grey-luna" style="height: 38px; padding-top:5px;">
					
								{{ quantity }}
							</div>
						</b-card>
					</b-form-group>
				</b-col>
				
				<b-col sm=6 style="padding-right: 0px;">
					<b-form-group label="SF covered: " v-if="focusSF" >
						<b-input-group>
							<b-form-input type="number" @input="setSFC($event)" :value="sfCovered"  step="1"></b-form-input>
						</b-input-group>
<<<<<<< HEAD
=======

						
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
					</b-form-group>
					
					<b-form-group label="SF covered: "  v-else>
						<b-card no-body @click="focusSF = true; focusQ = false" class="text-center ">
							
							<div class="grey-luna " style="height: 38px; padding-top:5px;">
								
								<span>{{ sfCovered }}</span>
							</div>
						</b-card>
					</b-form-group>
				</b-col>
				
				<b-col sm=12>
					<b-form-group label="PO price: ">
						<b-card no-body class="text-center ">
							<div class="grey-luna" style="height: 38px; padding-top:5px;">
								$ {{ po }}
							</div>
						</b-card>
					</b-form-group>
				</b-col>
			</b-row>

			<b-row>
				<b-col class="text-right">
					<button class="btn-gold" @click="submitOrder">SUBMIT ORDER NOW!</button>
				</b-col>
			</b-row>
		</b-container>

		<transition v-else name="fade">
			<b-container>
				<b-row>
					<b-col class="text-center">
						<p>Your order has been set up. You can check your orders in the dashboard</p>
					</b-col>
					
				</b-row>

				<b-row>
					<b-col class="text-center">
						<button class="btn-gold" @click="openDashboard">GO TO THE DASHBOARD</button>
					</b-col>
				</b-row>
			</b-container>
		</transition>
	</div>`,

 	data(){
		return {
			focusQ: false,
      		focusSF: false,

      		orderDone: false,

      		quantity: 0,
			sfCovered: 0,
			po: "0.00",
			promoCode: "",
			sf: 0,

			orderItem:{
				details: {},
				pattern: {},
				build: {}
			},

			vd: new VueDiana()

		}
	},
	created(){
		this.$root.$data.displayButton = false;
		this.orderItem.pattern = this.$root.$data.cart.pattern;
		this.orderItem.build = this.$root.$data.cart.build;

		this.$root.$forceUpdate();

		var main = this;
		axios({
	        method: 'get',
	        url: 'asset?r=dp&a=dpone&id='+main.orderItem.pattern.dp,
	    }).then(function (response) {
	          //handle success
	          console.log(response);
	          console.log(response.data);

	          let call = response.data.call;

	          main.errors = [];

	           switch(call){

	             case true:
	               if(response.data.hasOwnProperty("info")){
	                  var aux = response.data.info[0];

	                  for(var key in aux) {
	                  	 main.orderItem.pattern[key] = aux[key];
	                  }

	                  main.sf = aux.sc;
	               }


	              
	             break;

	             case false:
	               switch(response.data.errors){

	                 default:
	                   vd.toastError();
	                 break;
	               }
	             break;
	             
	             default:
	               vd.toastError();
	             break;
	           }
	    }).catch(function (response) {
	          //handle error
	          console.log(response);
	    }).then(function(){
	    	main.$root.$forceUpdate();
	    });
	},
	methods: {
		openDashboard(){
			window.open("dashboard",'_self')
		},
		setS(q){
			console.log(q);

			this.sfCovered = (q*this.sf).toFixed(4);
			this.quantity = q;

			this.setPO();

			console.log(this.quantity);
			console.log(this.sfCovered);

			//this.po = q*(parseFloat(this.mainPattern.price)).toFixed(2);
			///asset?r=dp&a=dpone&id=

		},
		setSFC(s){
			
			console.log(s);

			var aux = Math.ceil(s/this.sf);

			this.quantity = aux;
			this.sfCovered = s;

			this.setPO();

			console.log(this.quantity);
			console.log(this.sfCovered);
		},
		setPO(){
			this.po = (this.quantity*this.$root.$data.cart.pattern.price).toFixed(2);


			this.orderItem["details"] = {
				quantity: this.quantity,
				sfCovered: this.sfCovered,
				po: this.po,
				sf: this.sf,
				promoCode: this.promoCode
			}
		},
		submitOrder(){
				
			var main = this;

			var submitForm = new FormData();

			if(!this.$root.$data.session.status){
				main.$root.$refs.loginModal.show();
				
			}else{

				NProgress.start();

				this.orderItem['client'] = this.$root.$data.session.id;

				this.orderItem['cutType'] = null;

			    submitForm.append("request","add");
			    submitForm.append("method","post");

			    //Here apennds stringified all content
			    submitForm.append("form",JSON.stringify(this.orderItem));

				axios.post('orders',submitForm,{
			        headers: {
			           'content-type': 'multipart/form-data'
			        }
			    }).then(function (response) {
			            //handle success
			            console.log(response);
			            console.log(response.data);

			            let call = response.data.call;

			            main.errors = [];

			             switch(call){

			               case true:
			                 main.vd.toastSuccess();
			               break;

			               case false:
			                 switch(response.data.errors){

			                   case "EmailExist":
			                     main.errors.push({message:"That email is already in use"});
			                   break; 
			                 }
			               break;
			               
			               default:
			                 main.vd.toastError();
			               break;
			             }
			    }).catch(function (response) {
			            //handle error
			            console.log(response);
			    }).then(function () {

				        NProgress.done();
				});
			}			

			
		}
	}
});