const CarouselComponent = Vue.component("l-carousel",{
 	template: 
 	`<div>
 		<b-carousel
	      v-model="slide"
<<<<<<< HEAD
	      :interval="5000"
=======
	      :interval="15000"
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
	      controls
	      indicators
	      background="#ababab"
	      img-width="1024"
	      img-height="480"
	      style="text-shadow: 1px 1px 2px #333;"
	      @sliding-start="onSlideStart"
	      @sliding-end="onSlideEnd"
	    >
  		  <div v-for="(item,index) in slides" role="listitem" class="carousel-item active" aria-current="true" aria-posinset="3" aria-setsize="5" :tabindex="index" aria-hidden="false">

			<img :src="setImage(item.img)" width="1024" height="480" class="img-fluid w-100 d-block">
			<div class="carousel-caption">
				<h3 v-if="item.title != ''">{{ item.title }}</h3>
				<p v-if="item.content != ''">{{ item.content }}</p>
			</div>
		</div>
  		  
		  
	    </b-carousel>
 	</div>`,

 	data(){
		return {
			slide: 0,
        	sliding: null,
        	slides: [],
		}
	},
	created(){
		this.loadSlides();
	},
	methods: {
		setImage(img){
			return img;
		},
		loadSlides(){
			var main = this;
			axios({
		        method: 'get',
		        url: 'lpage?g=carousel',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               main.slides = response.data.info;
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    }).then(function () {
		    	main.$forceUpdate();
		    }); 
		},
		onSlideStart(slide) {
			this.sliding = true
		},
		onSlideEnd(slide) {
			this.sliding = false
		}
	}
});