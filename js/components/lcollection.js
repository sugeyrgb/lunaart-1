 const lCollectionComponent = Vue.component("lcollection",{

	template: 
	`<div class='nav-padder'>
		
		<b-container fluid v-if=loading>
			<b-row v-if="collections.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
			
			<b-row>
	          <b-col>
	            <h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text component-title">Collections</h4>
	          </b-col>
	        </b-row>

			<b-container class="padding">
				<b-row>
					<b-button v-if="sorting != ''" class="btn btn-flat" @click="sorting = ''"> <i class="material-icons">keyboard_arrow_left</i> Back</b-button>
				</b-row>
				<b-row>
					<b-input-group class="mt-3">
				       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH PATTERN" autocomplete=off />
				    </b-input-group>
				</b-row>
			</b-container>

			<b-container v-if="sorting">
				
				<b-row>

					<b-col sm=3 lg=2 v-for="(m,index) in sortingM" @click="setItem(m)">
						<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive($event,index,m.category)" class="collection-card" img-top>
							<p class="text-center collection-title"><span class='gold-flat-text'>{{ m.title }}</span></p>
							<p class="text-center collection-p"> 
								
								{{ m.displayArray[m.imgIndex] }}
							</p>

							<div class="asset-container-button">
								<b-container>
									<b-row>
										<b-col class="text-left no-padding">
											<button class="asset-button asset-left" v-on:click.stop.prevent="(m.imgIndex == 0 ? m.imgIndex = 0 : m.imgIndex-- ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
												<
											</button>
										</b-col>

										<b-col class="text-right no-padding">
											<button class="asset-button asset-right " v-on:click.stop.prevent="(m.imgIndex == m.countImg ? m.imgIndex = m.countImg : m.imgIndex++ ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
												>
											</button>
										</b-col>
									</b-row>
								</b-container>
							</div>
					    </b-card>
					</b-col>

				</b-row>


			</b-container>

			<b-container fluid v-else>

				<b-tabs v-model="tabIndex"  class="nav-justified collection-tabs" content-class="mt-3" v-if="collections.length">
					<b-tab title="Coastline" active>
						
						<b-container>
							<b-row>
								<b-col sm=3 lg=2 v-for="(m,index) in coastline" @click="setItem(m)">
									<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive($event,index,m.category)" class="collection-card" img-top>
										<p class="text-center collection-title"><span class='gold-flat-text'>{{ m.title }}</span></p>
										<p class="text-center collection-p"> 
											
											{{ m.displayArray[m.imgIndex] }}
										</p>

										<div class="asset-container-button">
											<b-container>
												<b-row>
													<b-col class="text-left no-padding">
														<button class="asset-button asset-left" v-on:click.stop.prevent="(m.imgIndex == 0 ? m.imgIndex = 0 : m.imgIndex-- ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															<
														</button>
													</b-col>

													<b-col class="text-right no-padding">
														<button class="asset-button asset-right " v-on:click.stop.prevent="(m.imgIndex == m.countImg ? m.imgIndex = m.countImg : m.imgIndex++ ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															>
														</button>
													</b-col>
												</b-row>
											</b-container>
										</div>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Progressive">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3 lg=2 v-for="(m,index) in progresive" @click="setItem(m)">
									<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive($event,index,m.category)" class="collection-card" img-top>
										<p class="text-center collection-title"><span class='gold-flat-text'>{{ m.title }}</span></p>
										<p class="text-center collection-p"> 
											
											{{ m.displayArray[m.imgIndex] }}
										</p>

										<div class="asset-container-button">
											<b-container>
												<b-row>
													<b-col class="text-left no-padding">
														<button class="asset-button asset-left" v-on:click.stop.prevent="(m.imgIndex == 0 ? m.imgIndex = 0 : m.imgIndex-- ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															<
														</button>
													</b-col>

													<b-col class="text-right no-padding">
														<button class="asset-button asset-right " v-on:click.stop.prevent="(m.imgIndex == m.countImg ? m.imgIndex = m.countImg : m.imgIndex++ ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															>
														</button>
													</b-col>
												</b-row>
											</b-container>
										</div>
								    </b-card>
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Timeless">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3 lg=2 v-for="(m,index) in timeless" @click="setItem(m)">
									<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive($event,index,m.category)" class="collection-card" img-top>
										<p class="text-center collection-title"><span class='gold-flat-text'>{{ m.title }}</span></p>
										<p class="text-center collection-p"> 
											
											{{ m.displayArray[m.imgIndex] }}
										</p>

										<div class="asset-container-button">
											<b-container>
												<b-row>
													<b-col class="text-left no-padding">
														<button class="asset-button asset-left" v-on:click.stop.prevent="(m.imgIndex == 0 ? m.imgIndex = 0 : m.imgIndex-- ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															<
														</button>
													</b-col>

													<b-col class="text-right no-padding">
														<button class="asset-button asset-right " v-on:click.stop.prevent="(m.imgIndex == m.countImg ? m.imgIndex = m.countImg : m.imgIndex++ ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															>
														</button>
													</b-col>
												</b-row>
											</b-container>
										</div>
								    </b-card>
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Traditional">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3 lg=2 v-for="(m,index) in traditional" @click="setItem(m)">
									<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive($event,index,m.category)" class="collection-card" img-top>
										<p class="text-center collection-title"><span class='gold-flat-text'>{{ m.title }}</span></p>
										<p class="text-center collection-p"> 
											
											{{ m.displayArray[m.imgIndex] }}
										</p>

										<div class="asset-container-button">
											<b-container>
												<b-row>
													<b-col class="text-left no-padding">
														<button class="asset-button asset-left" v-on:click.stop.prevent="(m.imgIndex == 0 ? m.imgIndex = 0 : m.imgIndex-- ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															<
														</button>
													</b-col>

													<b-col class="text-right no-padding">
														<button class="asset-button asset-right " v-on:click.stop.prevent="(m.imgIndex == m.countImg ? m.imgIndex = m.countImg : m.imgIndex++ ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															>
														</button>
													</b-col>
												</b-row>
											</b-container>
										</div>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Geometric">

						<b-container>	
							<b-row>
							
								<b-col sm=3 lg=2 v-for="(m,index) in geometric" @click="setItem(m)">
									<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive($event,index,m.category)" class="collection-card" img-top>
										<p class="text-center collection-title"><span class='gold-flat-text'>{{ m.title }}</span></p>
										<p class="text-center collection-p"> 
											
											{{ m.displayArray[m.imgIndex] }}
										</p>

										<div class="asset-container-button">
											<b-container>
												<b-row>
													<b-col class="text-left no-padding">
														<button class="asset-button asset-left" v-on:click.stop.prevent="(m.imgIndex == 0 ? m.imgIndex = 0 : m.imgIndex-- ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															<
														</button>
													</b-col>

													<b-col class="text-right no-padding">
														<button class="asset-button asset-right " v-on:click.stop.prevent="(m.imgIndex == m.countImg ? m.imgIndex = m.countImg : m.imgIndex++ ); m.img = 'media/collection/'+m.category+'/'+m.title.toLowerCase()+'/file-'+m.imgIndex+'.webp'">
															>
														</button>
													</b-col>
												</b-row>
											</b-container>
										</div>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>
						
					</b-tab>
				</b-tabs>

				<b-row v-else>
					<b-col class="text-center">
						<p>There's no collections in your library</p>
					</b-col>
				</b-row>
			</b-container>
		 </b-container>

		 </b-container>
		 
		 <div v-viewer id="collection-g" class="gallery-invisible images clearfix gallery-card">
			<template>
				<b-img fluid :src="mainImage" class="image" :key="mainImage"/>
			</template>
		 </div>

		 <b-button v-if="addButton" @click="addToCart" class="addToCartButton btn-gold">ADD THIS COLLECTION TO YOUR <br> SAVED ITEMS</b-button>
	</div>
	  `,
	data() {
		return {
			collections: [],
			coastline: [],
			progresive: [],
			timeless: [],
			traditional: [],
			geometric: [],
			loading: false,
			sorting: "",
			sortingM: [],

			mainImage: "media/img/asset.png",
			mainCollection: null,
			addButton: false,

			tabIndex: 4,
		}
	},
	created(){
		var main = this;

		main.loadcollection();

		window.addEventListener("reloadcollections",function(){
			main.loadcollection();
			main.$forceUpdate();
		});
	},
	mounted(){
		
		var main = this;

		if(this.$root.$data.args != null){
			this.tabIndex = this.$root.$data.args;
		}else if(colTab != null){
			this.tabIndex = parseInt(colTab);
		}

		setInterval(function(){
			try{
				var viewer = main.$el.querySelector('#collection-g').$viewer
				if(!viewer.isShown){
					main.addButton = false;
				}
			}catch(e){}
			
		}, 2500);

		this.$forceUpdate();
	},
	methods: {
		toggleActive(e,i,collection){
			console.log(e)
			console.log(i)

			for(var key in this.coastline){
				this.coastline[key].active = false;
			}


			for(var key in this.progresive){
				this.progresive[key].active = false;
			}


			for(var key in this.timeless){
				this.timeless[key].active = false;
			}


			for(var key in this.geometric){
				this.geometric[key].active = false;
			}


			for(var key in this.traditional){
				this.traditional[key].active = false;
			}

			switch(collection){
				case 1:
					this.coastline[i].active = true;
				break;	

				case 2:
					this.progresive[i].active = true;
				break;	

				case 3:
					this.timeless[i].active = true;
				break;	

				case 4:
					this.geometric[i].active = true;
				break;	

				case 5:
					this.traditional[i].active = true;
				break;	
			}
		},
		addToCart(){
			console.log(this.$root.$data.cart.pattern);

			var newItem = this.mainCollection;
			var array = this.$root.$data.cart.pattern;
			
			var flag = false;

			for(var o in array){
				if(array[o].id == newItem.id){
					flag = false;
					throw false;
				}else{
					flag = true;
				}
			}


			this.$root.$data.cart.display = newItem;
			this.$root.$data.cart.pattern.push(newItem);
			this.$root.$forceUpdate();
		},
		setItem(collection){
			console.log(collection);
			
			var main = this;

			main.mainImage = collection.img;
			main.indexImage = collection.imgIndex;
			main.mainCollection = collection;
			
			setTimeout(function(){
				main.show();
			},800);
			
		},
		searchMat(){
			var main = this;
			
			this.sortingM = this.collections.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});

			console.warn(this.sortingM)
		},
		show() {
			const instViewer = this.$el.querySelector('#collection-g').$viewer
			instViewer.show();
			this.addButton = true;
			console.log(instViewer);
		},
		loadcollection(){
			//asset?r=collection&a=da
			 var main = this;

			 main.loading = true;

			 main.collections = [];

			 main.coastline = [];
			 main.progresive = [];
			 main.timeless = [];
			 main.traditional = [];
			 main.geometric = [];

			  axios({
		        method: 'get',
		        url: 'asset?r=collection&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.collections = response.data.info;

		                  	main.sortingM = main.collections;

							for(var m in main.collections){

						      	main.collections[m].category = parseInt(main.collections[m].category);
						      	
						      	switch(main.collections[m].category){
							  		case 1:
							  			main.coastline.push(main.collections[m]);
							  		break;

							  		case 2:
							  			main.progresive.push(main.collections[m]);
							  		break;

							  		case 3:
							  			main.timeless.push(main.collections[m]);
							  		break;

							  		case 4:
							  			main.traditional.push(main.collections[m]);
							  		break;

							  		case 5:
							  			main.geometric.push(main.collections[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.collections = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
	}
});
