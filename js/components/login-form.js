const loginComponent = Vue.component("login-form",{
	template: 
	`	<div>
			<h5 class='luna-gold-title'>REGISTERED CUSTOMERS</h5>
			<p>If you have an account with us, log in using your email address</p>
			<b-form @submit="login">
				<b-row v-if="errors.length">
					<b-col sm=12 v-for="error in errors">
						<b-alert variant="danger" show>{{ error.message }}</b-alert>
					</b-col>
				</b-row>

				<b-row>
					<b-col col sm=12 lg=6>
						<b-input v-model="form.email" placeholder="EMAIL" type=email />
					</b-col>
					<b-col col sm=12 lg=6>
						<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
							<b-input v-model="form.pass" type="password" autocomplete="off" placeholder="PASSWORD" />
						</b-input-group>
					</b-col>
				</b-row>
				<b-row>
					<b-col col sm=12 lg=3>
						<b-button type=submit block class="golden-border" variant="none">Login</b-button>
					</b-col>

					<b-col sm=12>
						<a href="#" id='closeThis' v-on:click="updateRoute('/forgot')">Forgot your password?</a>
					</b-col>
				</b-row>

				<b-row class="text-center padding" v-if=loading>
					<b-col>
						<l-spinner color="luna-text-gold"></l-spinner>
					</b-col>
				</b-row>
			</b-form>
		</div>
	  `,
	data() {
		return {
			loading: false,
			errors: [],
			form: {
				email: null,
				pass: null
			},
			request: "login"
		}
	},
	mounted(){
		
	},
	methods: {
		updateRoute(rt){
			console.log(this.$root)
			this.$root.$refs.loginModal.hide()
			this.$root.updateRoute(rt)

		},
		login(e){
			e.preventDefault();

			let main = this;
			main.loading = true;
			console.log("clic")

			var signupForm = new FormData();

			var vd = new VueDiana();

			signupForm.append("request",main.request);
			signupForm.append("method","post");

			signupForm.append("user",main.form.email);
			signupForm.append("pass",main.form.pass);

			console.log(this.form);

       		axios({
			    method: 'post',
			    url: 'login',
			    data: signupForm,
			    config: { headers: {'Content-Type': 'multipart/form-data' }}
		    }).then(function (response) {
		        //handle success
		        console.log(response);
		        console.log(response.data);

		        let call = response.data.call;

		        main.errors = [];

		       	switch(call){

		       		case true:

		       			main.$root.loadSession();
		       		break;

		       		case false:
			       		switch(response.data.errors){
			       								
							case "EnoughUser":
			       				vd.toastError("","Email field is empty");
			       				main.errors.push({message:"Email field is empty"});
			       			break; 

			       			case "EnoughPass":
			       				vd.toastError("","Is required a password");
			       				main.errors.push({message:"Password field is empty"});
			       			break; 

			       			case "NoUser":
			       				vd.toastError("","This email is not registered");
			       				main.errors.push({message:"Email is not registered"});
			       			break; 

			       			case "NoPass":
			       				vd.toastError("","The password is incorrect");
			       				main.errors.push({message:"Wrong password"});
			       			break; 

			       			case "NotEnoughForm":
			       				main.errors.push({message:"We need your email and password"});
			       			break;

			       			default:
			       				vd.toastError();
			       			break;
			       		}
		       		break;
		       		
		       		default:
		       			vd.toastError();
		       		break;
		       	}
		    }).catch(function (response) {
		        //handle error
		        console.log(response);
		    }).then(function () {
				main.loading = false;
			}); 
		}
	}
});
