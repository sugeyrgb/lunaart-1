const APackingComponent = Vue.component("apacking",{
	template: 
	`
	  <div class='cs-container'>

		<b-container v-if="!viewPack" fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-form-input v-model="search" @input="searchPacking(search)" placeholder="ORDER NUMBER" autocomplete="off" />
			
				</b-col>
			</b-row>
			
			<b-row v-if="invErrors.length" class='padding'>
              <b-col sm=12 v-for="error in invErrors">
                <b-alert variant="danger" show>{{ error.message }}</b-alert>
              </b-col>
            </b-row>

			<b-row>
              <b-col sm=12 class="padding">
                <b-collapse id="collapse-2">
                  <b-card>
                   	
                    <b-form-group>
		              <b-form-input v-model="packU.number" placeholder="ORDER" list='orders' autocomplete="off" @input="searchOrders(packU.number)" />

			            <datalist id="orders">
							<option v-for='o in orders' :value="o.number">{{ o.name+' '+o.lastname+' '+(o.business != ''? '('+o.business+')' : '')}}</option>
						</datalist>
		            </b-form-group>

		            <b-form-group>
		              <b-form-input v-model="packU.track" type="number" placeholder="TRACKING NUMBER" autocomplete="off" />
		            </b-form-group>

		            <b-form-group>
		              <b-form-input v-model="packU.carrier" type="text" placeholder="CARRIER" autocomplete="off" />
		            </b-form-group>
					
					<b-form-group>
		            	<b-input-group append='Lbs'>
							<b-form-input type="number" step=any v-model="packU.weight" placeholder="WEIGHT" autocomplete="off" />
						</b-input-group>
		            </b-form-group>

		            <b-form-group label="STATUS">
						<b-form-select v-model="packU.status" :options="statusOpt" :value="1" ></b-form-select>
					</b-form-group>

					<b-form-group>
		              <b-form-input type="number" v-model="packU.boxes" placeholder="BOXES" autocomplete="off" />
		            </b-form-group>
					
					<b-row>
		            	<b-col sm=6>
							<b-form-group label="ORIGIN">
								<b-form-select v-model="packU.origin" :options="states" ></b-form-select>
							</b-form-group>
						</b-col>

						<b-col sm=6>
							<b-form-group label="DESTINY">
								<b-form-select v-model="packU.destiny" :options="states" ></b-form-select>
							</b-form-group>
						</b-col>
		            </b-row>

		            <b-row>
		            	<b-col sm=6>
			            	<b-input-group append='"'>
								<b-form-input type="number" step=any v-model="packU.w" placeholder="WIDTH" autocomplete="off" />
							</b-input-group>
						</b-col>

						<b-col sm=6>
							<b-input-group append='"'>
								<b-form-input type="number" step=any v-model="packU.h" placeholder="HEIGHT" autocomplete="off" />
							</b-input-group>
						</b-col>
		            </b-row>

                    <b-col class="text-right padding">
                      <button @click="uploadPack" class="btn-blue">Upload</button>
                    </b-col>
                  </b-card>
                </b-collapse>
                
              </b-col>
            </b-row>
			
			<b-row class="text-center padding" v-if='loading'>
              <b-col>
                <l-spinner color="luna-text-gold"></l-spinner>
              </b-col>
            </b-row>
			
			<b-row v-else>
				<b-col sm=12 v-if="packings.length">
					<b-col class="padding">

		                <div class="table-responsive">
		                    <table class="table table-hover text-center" style="width: 100%!important">
		                      <thead>
		                        <tr>
		                          <th v-for="f in infields" scope="col">{{ f }}</th>
		                          <th><b-button v-b-toggle.collapse-2 class="m-1">Add Packing list</b-button></th>
		                        </tr>
		                      </thead>
		                      <tbody>
		                        <tr v-for="pac in packings">
		                          <th scope="row">#{{ pac.number }}</th>
		                       	  <td>{{ pac.track }}</td>
		                       	  <td>{{ pac.fdated }}</td>
		                       	  <td>{{ pac.boxes }}</td>
		                       	  <td>{{ pac.weight }}Lbs</td>
		                       	  <td>{{ pac.width }}" x {{ pac.height }}"</td>
		                       	  <td>{{ statusDisplay[pac.status] }}</td>
		                       	  <td>{{ pac.carrier }}</td>
		                       	  <td>{{ pac.name }} {{ pac.lastname }} {{ (pac.business != "" ? '('+pac.business+')' : "" ) }}</td>
		                       	  <td>{{ pac.origin }}</td>
		                       	  <td>{{ pac.destiny }}</td>
		                       	  <td class="text-center full-width" style='display: inline-block'><button type="button" @click="openPack(pac.id)" class="btn-flat"><i class="fas fa-eye"></i></button> <button type="button" @click="deletePack(pac.id)" class="btn-flat"><i class="fas fa-times"></i></button></td>
		                        </tr>
		                      </tbody>
		                    </table>
		                </div>

	                </b-col>
				</b-col>
			
				<b-col sm=12 class="text-center" v-else>
					<p class="text-center" >There's no packing lists, add one here <b-button v-b-toggle.collapse-2 variant="link">Link</b-button></p>
				</b-col>
			</b-row>


		</b-container>

		<b-container v-else fluid>
			<b-row>
	            <b-col>
	              <button class="btn-flat padding" style="padding: 2%;" @click="closePack"><i class="fas fa-chevron-left fa-2x"></i></button>
	            </b-col>
	        </b-row>

	        <b-row>
				<b-col sm=12 class="padding">
                  <b-card>
                   	
                    <b-form-group label="ORDER">
		              <b-form-input disabled v-model="updatePack.number" placeholder="ORDER" list='orders' autocomplete="off" @input="searchOrders(updatePack.number)" />

			            <datalist id="orders">
							<option v-for='o in orders' :value="o.number">{{ o.name+' '+o.lastname+' '+(o.business != ''? '('+o.business+')' : '')}}</option>
						</datalist>
		            </b-form-group>

		            <b-form-group label="TRACKING NUMBER">
		              <b-form-input v-model="updatePack.track" type="number" placeholder="TRACKING NUMBER" autocomplete="off" />
		            </b-form-group>

		            <b-form-group label="CARRIER">
		              <b-form-input v-model="updatePack.carrier" type="text" placeholder="CARRIER" autocomplete="off" />
		            </b-form-group>
					
					<b-form-group label="WEIGHT">
		            	<b-input-group append='Lbs'>
							<b-form-input type="number" step=any v-model="updatePack.weight" placeholder="WEIGHT" autocomplete="off" />
						</b-input-group>
		            </b-form-group>

		            <b-form-group label="STATUS">
						<b-form-select v-model="updatePack.status" :options="statusOpt" :value="1" ></b-form-select>
					</b-form-group>

					<b-form-group label="BOXES NUMBER">
		              <b-form-input type="number" v-model="updatePack.boxes" placeholder="BOXES" autocomplete="off" />
		            </b-form-group>
					
					<b-row>
		            	<b-col sm=6>
							<b-form-group label="ORIGIN">
								<b-form-select v-model="updatePack.origin" :options="states" ></b-form-select>
							</b-form-group>
						</b-col>

						<b-col sm=6>
							<b-form-group label="DESTINY">
								<b-form-select v-model="updatePack.destiny" :options="states" ></b-form-select>
							</b-form-group>
						</b-col>
		            </b-row>

		            <b-row>
		            	<b-col sm=6>
			            	<b-input-group label="WIDTH" append='"'>
								<b-form-input type="number" step=any v-model="updatePack.width" placeholder="WIDTH" autocomplete="off" />
							</b-input-group>
						</b-col>

						<b-col sm=6>
							<b-input-group label="HEIGHT" append='"'>
								<b-form-input type="number" step=any v-model="updatePack.height" placeholder="HEIGHT" autocomplete="off" />
							</b-input-group>
						</b-col>
		            </b-row>

                    <b-col class="text-right padding">
                      <button @click="updatePacking(updatePack.id)" class="btn-blue">Update</button>
                    </b-col>
                  </b-card>
                
              </b-col>
	        </b-row>
		</b-container>
	  </div>`,
	data() {
		return {
			viewPack: false,

			search: "",

			packU: {
				number: null,
				track: null,
				carrier: null,
				weight: null,
				status: 1,
				boxes: null,
				origin: null,
				destiny: null,
				w: null,
				h: null
			},

			updatePack: {
				id: null,
				number: null,
				track: null,
				carrier: null,
				weight: null,
				status: 1,
				boxes: null,
				origin: null,
				destiny: null,
				width: null,
				height: null
			},

			statusOpt: [
				{value: 1, text: "Preparing"},
				{value: 2, text: "Shipped"},
				{value: 3, text: "In transit"},
				{value: 4, text: "Delivered"},
				{value: 5, text: "Canceled"},
			],

			statusDisplay: ["Preparing", "Shipped", "In transit", "Delivered", "Canceled"],

			orders: [],

			loading: true,

			infields: ["ORDER", "TRACKING NUMBER", "DATE", "TOTAL BOXES", "TOTAL WEIGHT", "SIZE", "STATUS", "CARRIER", "CLIENT", "ORIGIN", "DESTINY"],

			packings: [],

			pack: [],

			states: [],

			invErrors: [],

			vd: new VueDiana()


		}
	},
	created(){
		this.$root.$data.componentTitle = "Admin packing lists";
		this.searchPacking();

		let main = this;
		axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8303e1d831e89cb8af24a11f2fa77353c317e408/states_titlecase.json').then(function(response){
	      let lalas = response.data;
	      console.log(lalas)
	      console.log(main.states);
	      for(var key in response.data){
	      	var temp = {
	      		text: response.data[key].name,
	      		value: response.data[key].abbreviation,
	      	}

	      	main.states.push(temp);
	      }
	      console.log(main.states);
	    }).catch(function(error){
	      console.log(error);
	    });
	},
	update(){

	},
	methods: {
		closePack(){
			this.viewPack = false;
			this.pack = [];
		},
		openPack(id){
			this.viewPack = true;
			var vd = new VueDiana();
			var main = this;
	
			main.updatePack = [];

			axios({
				method: 'get',
				url: 'packing-lists?g=o&args='+id,
			}).then(function (response) {
			  //handle success
			  console.log(response);
			  console.log(response.data);

			  let call = response.data.call;

			   switch(call){

			     case true:
			     	if(response.data.info.length){
			     		main.updatePack = response.data.info[0];
			     	}else{
			     		main.updatePack = []
			     	}
			     	
			     break;

			     case false:
			       switch(response.data.errors){

			         default:
			           vd.toastError();
			         break;
			       }
			     break;
			     
			     default:
			       vd.toastError();
			     break;
			   }
			}).catch(function (response) {
			  //handle error
			  console.log(response);
			}).then(function () {
				main.loading = false;
			});
		},
		updatePacking(){
			var main = this;
			NProgress.start();

			var uploadForm = new FormData();

			for(var input in main.updatePack){
				uploadForm.append(input,main.updatePack[input]);
			}

			uploadForm.append("request","update");
			uploadForm.append("method","post");

			 axios.post('packing-lists',uploadForm,{
	          headers: {
	             'content-type': 'multipart/form-data'
	          }
	        }).then(function (response) {
	              //handle success
	              console.log(response);
	              console.log(response.data);

	              let call = response.data.call;

	              main.errors = [];

	               switch(call){

	                 case true:
	                   main.vd.toastSuccess();
	                   main.searchPacking();
	                 break;

	                 case false:
	                   switch(response.data.errors){
	                   	 case "NumberAlready":
	                   	 	main.errors.push({message:"That order has already a packing lists"});
	                   	 break;

	                     default:
	                       main.vd.toastError();
	                     break;
	                   }
	                 break;
	                 
	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	          }).catch(function (response) {
	              //handle error
	              console.log(response);
	          }).then(function () {
		         NProgress.done();
		      });
		},
		uploadPack(){

			var main = this;
			main.loading = true;

			var uploadForm = new FormData();

			for(var input in main.packU){
				uploadForm.append(input,main.packU[input]);
			}

			uploadForm.append("request","add");
			uploadForm.append("method","post");

			 axios.post('packing-lists',uploadForm,{
	          headers: {
	             'content-type': 'multipart/form-data'
	          }
	        }).then(function (response) {
	              //handle success
	              console.log(response);
	              console.log(response.data);

	              let call = response.data.call;

	              main.invErrors = [];

	               switch(call){

	                 case true:
	                   main.vd.toastSuccess();
	                   main.searchPacking();
	                 break;

	                 case false:
	                   switch(response.data.errors){
	                   	 case "NumberAlready":
	                   	 	main.invErrors.push({message:"That order has already a packing lists"});
	                   	 break;

	                     default:
	                       main.vd.toastError();
	                     break;
	                   }
	                 break;
	                 
	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	          }).catch(function (response) {
	              //handle error
	              console.log(response);
	          }).then(function () {
	          main.loading = false;
	        });

		},
		searchPacking(g="a",args=""){
			var vd = new VueDiana();
			var main = this;
			main.loading = true;

			main.packings = [];

			axios({
			method: 'get',
			url: 'packing-lists?g='+g+'&args='+args,
			}).then(function (response) {
			  //handle success
			  console.log(response);
			  console.log(response.data);

			  let call = response.data.call;

			   switch(call){

			     case true:
			     	if(response.data.info.length){
			     		main.packings = response.data.info;
			     	}else{
			     		main.packings = []
			     	}
			     	
			     break;

			     case false:
			       switch(response.data.errors){

			         default:
			           vd.toastError();
			         break;
			       }
			     break;
			     
			     default:
			       vd.toastError();
			     break;
			   }
			}).catch(function (response) {
			  //handle error
			  console.log(response);
			}).then(function () {
				main.loading = false;
			});  
		},
		searchOrders(number){
	  		var main = this;
	  		
	  		axios({
		        method: 'get',
		        url: 'orders?g=as&number='+number,
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		             	main.orders = response.data.info;

		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   main.vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               main.vd.toastError();
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    }).then(function () {
		        main.loading = false;
		    }); 
	  	},
	  	deletePack(id){
	       main = this;
	       var updateForm =  new FormData();
	       var vd = new VueDiana();

	       if(confirm("Are you sure that you want delete packing list?")){
	         updateForm.append("id",id);
	         updateForm.append("request","delete");
	         updateForm.append("method","post");

	        axios.post('packing-lists',updateForm,{
	          headers: {
	             'content-type': 'multipart/form-data'
	          }
	        }).then(function (response) {
	              //handle success
	              console.log(response);
	              console.log(response.data);

	              let call = response.data.call;

	              main.invErrors = [];

	               switch(call){

	                 case true:
	                   main.vd.toastSuccess();
	                   main.searchPacking();
	                 break;

	                 case false:
	                   switch(response.data.errors){

	                     default:
	                       main.vd.toastError();
	                     break;
	                   }
	                 break;
	                 
	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	          }).catch(function (response) {
	              //handle error
	              console.log(response);
	          }).then(function () {
	          main.loading = false;
	        });
	       }
	    },
	}
});
