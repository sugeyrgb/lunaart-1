const collectionComponent = Vue.component("collection",{
	//Side default is null, 1 equals to landing page and 2 is admin dashboard, 3 is client dashboard.
	props:{
		side: null
	},
	template: 
	`<div>
		
		<b-container fluid v-if=loading>
			<b-row v-if="collections.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
			<div v-if="!updateMenu">	
				<b-container class="padding">
					
					<b-row>

						<b-input-group class="mt-3">
					       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH COLLECTION" autocomplete=off />
					    </b-input-group>

					</b-row>


				</b-container>

				<b-container v-if="sorting">
					
					<b-row>

						<b-col sm=3 lg=2  v-for="m in sortingM">
							<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive(m)" class="collection-card" img-top>
								<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
						    </b-card>
						</b-col>

					</b-row>


				</b-container>

				<b-container fluid v-else>

					<b-tabs class="nav-justified" content-class="mt-3" v-if="collections.length">
						<b-tab title="Coastline" active>
							
							<b-container>
								<b-row>
									<b-col sm=3 lg=2  v-for="m in coastline ">
										<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive(m)" class="collection-card" img-top>
											<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
									    </b-card>
									</b-col>
								</b-row>
							</b-container>

						</b-tab>

						<b-tab title="Progressive">
							
							<b-container>	
								<b-row>
								
									<b-col sm=3 lg=2  v-for="m in progresive" class="collection-card">
										<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive(m)" class="collection-card" img-top>
											<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
									    </b-card>
										
									</b-col>

								</b-row>

							</b-container>

						</b-tab>

						<b-tab title="Timeless">
							
							<b-container>	
								<b-row>
								
									<b-col sm=3 lg=2  v-for="m in timeless" class="collection-card">
										<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive(m)" class="collection-card" img-top>
											<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
									    </b-card>
										
									</b-col>

								</b-row>

							</b-container>

						</b-tab>

						<b-tab title="Traditional">
							
							<b-container>	
								<b-row>
								
									<b-col sm=3 lg=2  v-for="m in traditional" class="collection-card">
										<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive(m)" class="collection-card" img-top>
											<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
									    </b-card>
										
									</b-col>

								</b-row>
							</b-container>

						</b-tab>

						<b-tab title="Geometric">

							<b-container>	
								<b-row>
								
									<b-col sm=3 lg=2  v-for="m in geometric" class="collection-card">
										<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" :class="(m.active ? 'active' : '')" @click="toggleActive(m)" class="collection-card" img-top>
											<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
									    </b-card>
										
									</b-col>

								</b-row>
							</b-container>
							
						</b-tab>
					</b-tabs>

					<b-row v-else>
						<b-col class="text-center">
							<p>There's no collections in your library</p>
						</b-col>
					</b-row>
				</b-container>
			</div>

			<div v-else>
				<b-container fluid>
					<b-row>
						<b-col>
							<button type="button" @click="updateMenu = false;" class="flat"><i class="fas fa-chevron-left"></i></button>
						</b-col>

						<b-col>
							<button type="button" @click="deleteCollection" class="flat"><i class="fas fa-trash"></i></button>
						</b-col>
					</b-row>
					
					<b-container fluid>
						<b-row v-if="errorsColl.length">
	                      <b-col sm=12 v-for="error in errorsColl">
	                        <b-alert variant="danger" show>{{ error.message }}</b-alert>
	                      </b-col>
	                    </b-row>

						<b-row>
							<b-col>
								<h5 class="gold-flat-text">Update collection information</h5>
							</b-col>
						</b-row>

						<b-row>
							<b-col sm=12>
							
								<b-form-group
							        label="Title"
							      >
							        <b-form-input
							          v-model="updateCollection.title"
							          type="text"
							          placeholder="Set title collection..."
							        ></b-form-input>
							    </b-form-group>
						    </b-col>
							
							<b-col sm=12>
								<b-form-group description="Images that will be displayed in the landing page">
					    	  	  <b-col class="padding">
							  		<button @click="nm.push( { ms: [] } )" ><i class="fas fa-plus"></i></button>
							  		<button @click="nm.splice(-1,1)"><i class="fas fa-minus"></i></button>
							  	  </b-col>
                                  <div v-for="(item,index) in nm" >

		                            <b-col sm=12>
		                            	<b-form-file
			                                v-model="item[index]"
			                                placeholder="Chose pattern display image..."
			                                drop-placeholder="Drop file here..."
			                                accept="image/*"
			                            />
								    	
										<b-form-group description="Add materials">
										  	<b-col class="padding">
										  		<button @click="nm[index]['ms'].push( { 'val': null } )" ><i class="fas fa-plus"></i></button>
										  		<button @click="nm[index]['ms'].splice(-1,1)"><i class="fas fa-minus"></i></button>
										  	</b-col>
			                                <b-form-select v-for="(m,uindex) in nm[index]['ms']" @input="addMtoC($event,index,uindex)" :options="materials" />
			                            </b-form-group>
								    </b-col>
							  	  </div>
	                            </b-form-group>

							</b-col>

						    <b-col sm=12>
								<b-form-group description="Select main pattern">
                                	<b-form-select v-model="updateCollection.dp" :options="cdps" />
                                </b-form-group>
						    </b-col>


						    <b-col sm=12>
								<b-form-group>
								  	<b-input-group prepend="$" >
											<b-form-input type="number" step="any" v-model="updateCollection.price" placeholder="PRICE" autocomplete="off" />
									</b-input-group>
								</b-form-group>
						    </b-col>


						    <b-col sm=12>
								<b-form-group>
	                                <b-form-select v-model="updateCollection.category" :options="cCat" />
	                            </b-form-group>
						    </b-col>

						    <b-col class="text-right padding">
                            	 <button @click="uploadCollection" class="btn-blue">UPDATE</button>
                            </b-col>       
						</b-row>
					</b-container>

				</b-container>
			</div>
		 </b-container>
	</div>
	  `,
	data() {
		return {
			collections: [],
			coastline: [],
			progresive: [],
			timeless: [],
			traditional: [],
			geometric: [],
			loading: false,
			sorting: "",
			sortingM: [],

			errorsColl: [],

			materials: [],

			dps: [],
			cdps: [],

			cCat: [
				{text: 'Collection', value: null },
				{text: "Coastline",value: 1},
				{text: "Progresive",value: 2},
				{text: "Timeless",value: 3},
				{text: "Traditional",value: 4},
				{text: "Geometric",value: 5}
			],

			updateMenu: false,
			updateCollection: {},
			ms: [],
			nm: []
		}
	},
	mounted(){
		console.log(this.collections);
		console.log(this.sortingM);


	},
	created(){
		var main = this;
		main.loadcollection();
		main.loadMaterial();
		main.loadPattern();

		console.log("Hi")

		window.addEventListener("reloadcollections",function(){
			main.loadcollection();
			main.$forceUpdate();
		});
	},
	updated(){
		console.log(this.collections);
		console.log(this.sortingM);
		console.log(this.nm);
	},
	methods: {
		searchMat(){
			var main = this;
			
			this.sortingM = this.collections.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});

			console.warn(this.sortingM)
		},
		toggleActive(m){
			var main = this;
			main.updateMenu  = true;
			main.updateCollection = m;
			main.ms = [];
			main.nm = [];
		},
		addMtoC(e,m,i){
			console.log(e);
			console.log(m);

			this.nm[m]["ms"][i] = e;
		},
		uploadCollection(e){
			e.preventDefault();

		      let main = this;
		      NProgress.start();

		      var updateForm = new FormData();
		      var vd = new VueDiana();

		      for(var input in this.updateCollection){
		        updateForm.append(input,this.updateCollection[input]);
		      }

		      for(var input in this.nm){
		      	if(input != "val"){
		      		updateForm.append("file-"+input,this.nm[input][input]);
		      		updateForm.append("materials[]",JSON.stringify(this.nm[input].ms));
		      	}

		      	console.log(this.nm[input][input]);
		      }

		      updateForm.append("request","collection");
		      updateForm.append("action","update");
		      updateForm.append("method","post");

		      axios.post('asset',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errorsColl = [];

		             switch(call){

		               case true:
		                 vd.toastSuccess();
		                 main.loadMaterial();
		                 window.dispatchEvent(new Event("reloadCollections"));
		                 main.updateMenu = false;
		                 main.$forceUpdate();

		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NotEnoughForm":
		                     main.errorsColl.push({message:"Must fill all fields in form"});
		                   break;

		                   case "NotFileMoved":
		                   	 main.errorsColl.push({message:"Couldn't move file to server"});
		                   break;

		                   case "CollectionAlready":
		                   	 main.errorsColl.push({message:"That collection already exist"});
		                   break;

		                   default:
		                     vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
		        	NProgress.done();
		      });

		  
		},
		loadcollection(){
			//asset?r=collection&a=da
			 var main = this;

			 main.loading = true;

			 main.collections = [];

			 main.coastline = [];
			 main.progresive = [];
			 main.timeless = [];
			 main.traditional = [];
			 main.geometric = [];

			  axios({
		        method: 'get',
		        url: 'asset?r=collection&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.collections = response.data.info;

		                  	main.sortingM = main.collections;

							for(var m in main.collections){
						      	console.log(m);
						      	console.log(main.collections[m].category)

						      	console.log(main.collections[m]);

						      	main.collections[m].category = parseInt(main.collections[m].category);
						      	
						      	switch(main.collections[m].category){
							  		case 1:
							  			main.coastline.push(main.collections[m]);
							  		break;

							  		case 2:
							  			main.progresive.push(main.collections[m]);
							  		break;

							  		case 3:
							  			main.timeless.push(main.collections[m]);
							  		break;

							  		case 4:
							  			main.traditional.push(main.collections[m]);
							  		break;

							  		case 5:
							  			main.geometric.push(main.collections[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.collections = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
		deleteCollection(){
			   var main = this;
		       var updateForm =  new FormData();
		       var vd = new VueDiana();

		       if(confirm("Are you sure that you want delete this collection?")){
		       	 NProgress.start();
		         updateForm.append("id",main.updateCollection.id);
		         updateForm.append("request","collection");
		         updateForm.append("action","delete");
		         updateForm.append("method","post");

		        axios.post('asset',updateForm,{
		          headers: {
		             'content-type': 'multipart/form-data'
		          }
		        }).then(function (response) {
		              //handle success
		              console.log(response);
		              console.log(response.data);

		              let call = response.data.call;

		              main.errors = [];

		               switch(call){

		                 case true:
		                   vd.toastSuccess();
		                   main.loadcollection();
		                   main.updateMenu = false;
						   main.$forceUpdate();
		                 break;

		                 case false:
		                   switch(response.data.errors){

		                     default:
		                       vd.toastError();
		                     break;
		                   }
		                 break;
		                 
		                 default:
		                   vd.toastError();
		                 break;
		               }
		          }).catch(function (response) {
		              //handle error
		              console.log(response);
		          }).then(function () {
		         	NProgress.done();
		        });
		       }
		},
		loadMaterial(){
			 var main = this;
			 main.materials =[];
			 axios({
		        method: 'get',
		        url: 'asset?r=material&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.materials = response.data.info;

		                  for (var m in main.materials) {
		                  	main.materialsAutocomplete[m] = {
		                  		text: main.materials[m].title,
		                  		id: main.materials[m].id
		                  	}
		                  }
		               }else{
		                  main.materials = [];
		               }
		              
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		      	vm.$forceUpdate();
		      });
		},
		loadPattern(){
			var main = this;
			 main.cdps =[];
			 axios({
		        method: 'get',
		        url: 'asset?r=dp&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.cdps = response.data.info;
		               }else{
		                  main.cdps = [];
		               }
		              
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		      	vm.$forceUpdate();
		      });
		},
	}
});
