const MaterialSelectComponent = Vue.component("material-select-component",{	
	props:{
		mainStyle: "",
		buttonValue: "",
	},
	template: 
		`<div>
			<button :class="mainStyle" @click="onScroll" v-b-modal.modal-select v-html="buttonValue"></button>

			<keep-alive>
				<b-modal centered ref="material-select" id="modal-select" class="selectedMaterial" title="Select material" size="xl" hide-footer>

					<b-container fluid>
						<h5 class="gold-flat-text">SAVED ITEMS</h5>
						<b-row class="material-tabs">
							<b-col cols=4 sm=4 md=2 v-for="m in mysaves" @click="setItem(m)">
								<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
									<span class='material-title'>{{ m.title }}</span>
								</b-card>
							</b-col>
						</b-row>
					</b-container>

					<b-container fluid v-if=loading>
						<b-row v-if="materials.length">
							<b-col class="text-center padding">
								<l-spinner color="luna-text-gold"></l-spinner>
							</b-col>
						</b-row>
					</b-container>

					<b-container fluid v-else>
						<b-container>
							<b-row>
								<b-input-group class="mt-3">
									<b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH MATERIAL" autocomplete=off />
								</b-input-group>
							</b-row>
						</b-container>

						<b-container class="container-scroll" v-if="sorting">
							<b-row class="material-tabs">
								<b-col cols=4 sm=4 md=2 v-for="m in sortingM" @click="setItem(m)">
									<b-card no-body :img-src="m.img" img-alt="Image" class="material-card" img-top>
										<span class='material-title'>{{ m.title }}</span>
									</b-card>
								</b-col>
							</b-row>
						</b-container>

						<b-container v-else>

							<b-tabs v-model="tabIndex" class="nav-justified material-tabs" content-class="mt-3" v-if="materials.length">
								<b-tab title="Stone" @click="loadMore(0)">
									
									<b-container id="first-container" class="container-scroll">
										<b-row>
											<b-col cols=4 sm=4 md=2 v-for="m in displayStone" @click="setItem(m)">
												<b-card no-body :img-src="m.img" img-alt="Image" class="material-card" img-top>
													<span class='material-title'>{{ m.title }}</span>
												</b-card>
											</b-col>
										</b-row>
									</b-container>

								</b-tab>

								<b-tab title="Mirror" @click="loadMore(1)">
									
									<b-container class="container-scroll">	
										<b-row>
										
											<b-col cols=4 sm=4 md=2 v-for="m in displayMirror" @click="setItem(m)" class="material-card">
												<b-card no-body :img-src="m.img" img-alt="Image" class="material-card" img-top>
													<span class='material-title'>{{ m.title }}</span>
												</b-card>
												
											</b-col>

										</b-row>

									</b-container>

								</b-tab>

								<b-tab title="Metal" @click="loadMore(2)">
									
									<b-container class="container-scroll">	
										<b-row>
										
											<b-col cols=4 sm=4 md=2 v-for="m in displayMetal" @click="setItem(m)" class="material-card">
												<b-card no-body :img-src="m.img" img-alt="Image" class="material-card" img-top>
													<span class='material-title'>{{ m.title }}</span>
												</b-card>
												
											</b-col>

										</b-row>

									</b-container>

								</b-tab>

								<b-tab title="Glass" @click="loadMore(3)">
									
									<b-container class="container-scroll">	
										<b-row>
										
											<b-col cols=4 sm=4 md=2 v-for="m in displayGlass" @click="setItem(m)" class="material-card">
												<b-card no-body :img-src="m.img" img-alt="Image" class="material-card" img-top>
													<span class='material-title'>{{ m.title }}</span>
												</b-card>
												
											</b-col>

										</b-row>
									</b-container>

								</b-tab>

								<b-tab title="Gemstones" @click="loadMore(4)">

									<b-container class="container-scroll">	
										<b-row>
											<b-col cols=4 sm=4 md=2 v-for="m in displayGemstones" @click="setItem(m)" class="material-card">
												<b-card no-body :img-src="m.img" img-alt="Image" class="material-card" img-top>
													<span class='material-title'>{{ m.title }}</span>
												</b-card>
												
											</b-col>

										</b-row>
									</b-container>
									
								</b-tab>
							</b-tabs>
							
							<b-row v-else>
								<b-col class="text-center">
									<p>There's no materials in your library</p>
								</b-col>
							</b-row>
						</b-container>
					</b-container>

					<b-container fluid>
						<b-row>
							<b-col cols=8 md=5>
								<b-row v-if="materialSelected">
									<b-col cols=4>
										<span class="grey-text">Selected</span>
										<b-img :src="materialSelected.img" fluid></b-img>
									</b-col>
									
									<b-col cols=8>
										<br>
										<h5 class="gold-flat-text"> {{ materialSelected.title }} </h5>
									</b-col>
								</b-row>
							</b-col>

							<b-col cols=4 md=7 class="text-right">
								<b-button class="mt-3" @click="returnMaterial">Select material</b-button>
							</b-col>
						</b-row>
					</b-container>
				</b-modal>
			</keep-alive>
		</div>`,
	data() {
		return {
			material: null,
			materials: [],
			display: [],
			loading: true,
			limit: 0,

			materialSelected: {},

			mysaves: [],

			materials: [],
			stone: [],
			mirror: [],
			metal: [],
			glass: [],
			gemstones: [],

			displayStone: [],
			displayMirror: [],
			displayMetal: [],
			displayGlass: [],
			displayGemstones: [],

			limitStone: 0,
			limitMirror: 0,
			limitMetal: 0,
			limitGlass: 0,
			limitGemstones: 0,

			loading: false,
			sorting: "",
			sortingM: [],

			mainImage: "media/img/asset.png",

			mainMaterial: null,

			tabIndex: 0
		}
	},
	created(){
		this.loadMaterial();
		
	},
	mounted(){
	},	
	update(){
		
	},
	methods: {
		setButtonValue(value){
			return value;
		},
		onScroll() {
			var main = this;
				this.loadSaves();
			setTimeout(function(){
				var elements = document.querySelectorAll(".container-scroll");
				main.loadMore();
			
				for(var elem in elements){
					elements[elem].addEventListener("scroll", function(event){
						var o = event.target;

						if(o.offsetHeight + o.scrollTop == o.scrollHeight){
							main.loadMore();
						}
					});
				}
			},100);
			
		},
		pushToArray(limit,array,parentArray){
			//Limit is the last limit that we had to start the count
			for(var i = limit; i < 40; i++){
				if(i in parentArray){
					array.push(parentArray[i]);
				}
			}
			//I want the iterator returned because it will be saved to set the new count
			return i;	
		},
		loadMore(index = null){
			var main = this;
			let loadIndex = 0;

			if(index != null){
				loadIndex = index;
			}else{
				loadIndex = main.tabIndex;
			}

			switch(loadIndex){
				case 0:
					main.limitStone = main.pushToArray(main.limitStone,main.displayStone,main.stone);
				break;

				case 1:
					main.limitMirror = main.pushToArray(main.limitMirror,main.displayMirror,main.mirror);
				break;

				case 2:
					main.limitMetal = main.pushToArray(main.limitMetal,main.displayMetal,main.metal);
				break;
				
				case 3:
					main.limitGlass = main.pushToArray(main.limitGlass,main.displayGlass,main.glass);
				break;

				case 4:
					main.limitGemstones = main.pushToArray(main.limitGemstones,main.displayGemstones,main.gemstones);
				break;

				default: 
				break;
			}
		},
		searchMat(){
			var main = this;
			
			this.sortingM = this.materials.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
			});
		},
		setItem(material){
			this.materialSelected = material;
		},
		returnMaterial(){
			if(this.materialSelected != {}){
				this.$emit("material-selected", this.materialSelected);
				this.$refs['material-select'].hide();
			}
			
		},
		drawMoreMaterials(){
			var main = this;

			for(var i = this.limit; i < 25; i++ ){
				if(i in main.materials){
					main.display.push(main.materials[i]);
				}
			}

			this.limit = i;
		},
		loadSaves(){
			//my-saves?g=selfMaterials
			var main = this;

			axios({
				method: 'get',
				url: 'my-saves?g=selfMaterials',
			 }).then(function (response) {
				  //handle success
				  console.log(response);
				  console.log(response.data);
 
				  let call = response.data.call;
 
				  main.errors = [];
 
				   switch(call){
 
					 case true:
					   if(response.data.hasOwnProperty("info")){
					      main.mysaves  = response.data.info;
					   }else{
						  main.mysaves = [];
					   }
					  
					 break;
 
					 case false:
					   switch(response.data.errors){
 
						 default:
						   vd.toastError();
						 break;
					   }
					 break;
					 
					 default:
					   vd.toastError();
					 break;
				   }
 
				   main.loading = false;
			 }).catch(function (response) {
				  //handle error
				  console.log(response);
			 }).then(function () {
				main.loading = false;
			 });
 
			main.$forceUpdate();
		},
		loadMaterial(){
			//my-saves?g=selfMaterials
			//asset?r=material&a=da
			var main = this;

			main.loading = true;

			main.materials = [];

			main.stone = [];
			main.mirror = [];
			main.metal = [];
			main.glass = [];
			main.gemstones = [];

			axios({
			   method: 'get',
			   url: 'asset?r=material&a=da',
			}).then(function (response) {
				 //handle success
				 console.log(response);
				 console.log(response.data);

				 let call = response.data.call;

				 main.errors = [];

				  switch(call){

					case true:
					  if(response.data.hasOwnProperty("info")){
							 main.materials = response.data.info;

							 main.sortingM = main.materials;

						   for(var m in main.materials){

								 main.materials[m].category = parseInt(main.materials[m].category);
								 
								 switch(main.materials[m].category){
									 case 1:
										 main.stone.push(main.materials[m]);
									 break;

									 case 2:
										 main.mirror.push(main.materials[m]);
									 break;

									 case 3:
										 main.metal.push(main.materials[m]);
									 break;

									 case 4:
										 main.glass.push(main.materials[m]);
									 break;

									 case 5:
										 main.gemstones.push(main.materials[m]);
									 break;

									 default:
										 console.log("Nu")
									 break;
								 }
						   }
					  }else{
						 main.materials = [];
					  }
					 
					break;

					case false:
					  switch(response.data.errors){

						default:
						  vd.toastError();
						break;
					  }
					break;
					
					default:
					  vd.toastError();
					break;
				  }

				  main.loading = false;
			}).catch(function (response) {
				 //handle error
				 console.log(response);
			}).then(function () {
			   main.loading = false;
			});

		   main.$forceUpdate();
		}
	}
});