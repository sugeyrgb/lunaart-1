const OrderComponent = Vue.component("orders",{
	template: 
	`<div class="cs-container"> 
		
		<b-container fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-form-input v-model="search" @input="sortOrders()" placeholder="ORDER NUMBER" autocomplete="off" />
				</b-col>
			</b-row>

			<div v-if="search == ''">
				<b-tabs class="nav-justified" content-class="mt-3" v-if="!loading">
					<b-tab title="Submited orders" active>
		    
						<div class="table-responsive">
				            <table class="table table-hover text-center with-grid" style="width: 100%!important">
				              <thead>
				                <tr>
				                  <th v-for="f in infieldsSubmited" scope="col">{{ f }}</th>
				                </tr>
				              </thead>
				              <tbody>
				                <tr v-for="ord in orders">
				                  <th scope="row">#{{ ord.number }}</th>
				               	  <td>{{ '$'+""+ord.content.details.po }}</td>
				               	  <td v-if="ord.invoice != null" class="text-center"><b-button block v-on:click.stop="viewInvoice(ord.invoice.pdf)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
				               	  <td v-else class="text-center"><b-button block disabled >Not set yet</b-button> </td>

				               	  <td>{{ ord.content.details.quantity }}</td>

				               	  <td>{{ (ord.comment != "" ? ord.comment : 'Not description') }}</td>
				               	  <td>{{ (ord.rdated != null ? ord.rdated : 'Not received yet') }}</td>
				               	  <td>{{ (ord.adated != null ? ord.adated : 'Not approved yet') }}</td>
				                </tr>
				              </tbody>
				            </table>
				        </div>
					</b-tab>

					<b-tab title="APPROVAL INFO">
						<div class="table-responsive">
				            <table class="table text-center with-grid" style="width: 100%!important">
				              <thead>
				                <tr>
				                  <th v-for="f in infieldsApproval" scope="col">{{ f }}</th>
				                </tr>
				              </thead>
				              <tbody>
				                <tr v-for="ord in orders" >
				                  <th scope="row">#{{ ord.number }}</th>
				               	  <td>{{ ord.fdated }}</td>

				               	  <td>{{ priority[ord.priority] }}</td>

				               	  <td>{{ '$'+""+ord.content.details.po }}</td>
				               	 
								  <td v-if="ord.approval == 1"><b-form-select :options="approval" :value="ord.approval" disabled></b-form-select></td>
							  	  <td v-else><b-form-select v-on:click.stop @input="updatePriority($event,ord.number,ord.content.details.po)" :options="approval" :value="ord.approval"></b-form-select></td>

								  <td class="text-center"><b-button block @click="viewOrder(ord.number)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
								  <td>{{ (ord.comment != "" ? ord.comment : 'Not description') }}</td>
								 
				                </tr>
				              </tbody>
				            </table>
				        </div>
					</b-tab>
					<b-tab title="PRODUCTION">
						<div class="table-responsive">
				            <table class="table text-center with-grid" style="width: 100%!important">
				              <thead>
				                <tr>
				                  <th v-for="f in infieldsProduction" scope="col">{{ f }}</th>
				                </tr>
				              </thead>
				              <tbody>
				                <tr v-for="ord in orders" >
				                  <th scope="row">#{{ ord.number }}</th>
				               	  <td>{{ ord.fdated }}</td>

				               	  <td>{{ priority[ord.priority] }}</td>
				               	 
								  <td class="text-center"><b-button block @click="viewOrder(ord.number)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
								  <td>{{ ord.process }}%</td>
								  <td v-if="ord.previewGallery == '1'">
									<img class="img-fluid image" style="border: 7px solid #EDEEF0; height: 60px; width: auto;" :src="ord.previewImages[0]" @click="ord.galleryIndex = 0">
								  	<vue-gallery-slideshow :images="ord.previewImages" :index="ord.galleryIndex" @close="ord.galleryIndex = null"></vue-gallery-slideshow>
								  </td>
								  <td v-else>Not process gallery</td>
				                </tr>
				              </tbody>
				            </table>
				        </div>
					</b-tab>
				</b-tabs>

				<b-row v-else>
					<b-col class="text-center padding">
						<l-spinner color="luna-text-gold"></l-spinner>
					</b-col>
				</b-row>
			</div>
			
			<b-row v-else>
				<b-col>
					<div class="table-responsive">
			            <table class="table table-hover text-center with-grid" style="width: 100%!important">
			              <thead>
			                <tr>
			                  <th v-for="f in sortingInfield" scope="col">{{ f }}</th>
			                </tr>
			              </thead>
			              <tbody>
			                <tr v-for="ord in sortingTable">
			                  <th scope="row">#{{ ord.number }}</th>
			                  <td>{{ ord.fdated }}</td>
	
							  <td v-if="ord.invoice != null" class="text-center"><b-button block v-on:click.stop="viewInvoice(ord.invoice.pdf)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
			               	  <td v-else class="text-center"><b-button block disabled >Not set yet</b-button> </td>							
			               	 
			               	  <td>{{ ord.process }}%</td>

			               	  <td v-if="ord.approval == 1"><b-form-select :options="approval" :value="ord.approval" disabled></b-form-select></td>
							  <td v-else><b-form-select v-on:click.stop @input="updatePriority($event,ord.number,ord.content.details.po)" :options="approval" :value="ord.approval"></b-form-select></td>

			               	  <td>{{ (ord.rdated != null ? ord.rdated : 'Not received yet') }}</td>
			               	  <td>{{ (ord.adated != null ? ord.adated : 'Not approved yet') }}</td>
			                </tr>
			              </tbody>
			            </table>
			        </div>	
				</b-col>
			</b-row>
		</b-container>

		<b-modal ref="modal-payout" centered ok-only ok-variant="secondary" ok-title="Cancel">
			<template slot="modal-title">
		    	{{ paymentTitle }}
		    </template>
			<div id="paypal-button-container"></div>
		</b-modal>
		
		
	 </div>
	  `,
	data() {
		return {
			loading: true,

			index: null,

			orders: [],

			infieldsSubmited: ["NUMBER","P.O","INVOICE","QTY","DESCRIPTION","RCVD DATE", "APRVL DATE"],

			infieldsApproval: ["NUMBER","DATE","PRIORITY","P.O", "STATUS", "PROCESS", "DESCRIPTION"],

			infieldsProduction: ["NUMBER", "DATE", "PRIORITY", "PDF", "PROCESS", "PROCESS GALLERY"],

			sortingTable: [],
			search: "",
			sortingInfield: ["NUMBER", "DATE", "INVOICE","PROCESS", "STATUS","RCVD DATE", "APRVL DATE"],

			keys: {
				user: ""
			},

			priority: [
				'Working as usual',
				'High',
				'Normal'
			],

			approval: [
				{value: 0, text: "Pending"},
				{value: 1, text: "Approved"},
				{value: 2, text: "Disapproved"},
			],

			paymentCheckoutAmount: "00.01",
			paymentTitle: "",
			toPlaceNumber: null,
		}
	},
	created(){
		this.$root.$data.componentTitle = "Orders";
		this.getAllOrders();
		let main = this;
		var timeQuery = setInterval(function(){
			main.getAllOrders();
		},300000);
		
	},
	update(){

	},
	methods: {
		sortOrders(e){
			let main = this;
			this.sortingTable = this.orders.filter(function(index) {
				
				if(index.number.toLowerCase().match(main.search.toLowerCase()) || index.fdated.toLowerCase().match(main.search.toLowerCase())){
					return index;
				}

				console.log(index.number.toLowerCase())
				console.log(main.search.toLowerCase())
				
			});
		},
		updatePriority(e,number, po){
			NProgress.start();
			var main = this;
			if(e == 1){
				document.getElementById("paypal-button-container").innerHTML = "";
				
				main.paymentCheckoutAmount = po;

				main.$refs['modal-payout'].show();

				console.log(po);

				if(po != ""){
					paypal.Buttons({
					    createOrder: function(data, actions) {
					      return actions.order.create({
					        purchase_units: [{
					          amount: {
					            value: po
					          }
					        }]
					      });
					    },
					    onApprove: function(data, actions) {
					      return actions.order.capture().then(function(details) {
					        
					        // Call your server to save the transaction
					        console.log(details)
					        var updateForm = new FormData();

								updateForm.append("number",number);
								updateForm.append("option",1);

								updateForm.append("request","approval");
								updateForm.append("method","post");

								axios.post('orders',updateForm,{
							        headers: {
							           'content-type': 'multipart/form-data'
							        }
							    }).then(function (response) {
							            //handle success
							            console.log(response);
							            console.log(response.data);

							            let call = response.data.call;

							          
							    }).catch(function (response) {
							            //handle error
							            console.log(response);
							    }).then(function () {
							    	main.$root.updateRoute("/thanks");
							    });
						      });
					    },
					    onError: function (err) {
						    alert("Something went wrong processing payment");
						    console.log(err);
						}
					}).render('#paypal-button-container');
				}
				

				main.paymentTitle = "Pay to approve order #"+number;
			}else{
				var updateForm = new FormData();

				updateForm.append("number",number);
				updateForm.append("option",e);

				updateForm.append("request","approval");
				updateForm.append("method","post");

				axios.post('orders',updateForm,{
			        headers: {
			           'content-type': 'multipart/form-data'
			        }
			    }).then(function (response) {
			            //handle success
			            console.log(response);
			            console.log(response.data);

			            let call = response.data.call;

			          
			    }).catch(function (response) {
			            //handle error
			            console.log(response);
			    }).then(function () {
			    	
			    });
			}
			

			console.log(arguments);
			/*
			*/

		    NProgress.done();
		},
		viewInvoice(url){
			window.open(url);
		},
		viewOrder(number){
			window.open('view?o='+number);
		},
		getAllOrders(){
		      var vd = new VueDiana();
		      var main = this;
		      main.loading = true;

		      main.orders = [];

		      axios({
		        method: 'get',
		        url: 'orders?g=m',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		             	main.orders = response.data.info;

		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		        main.$forceUpdate();
		      });  
		},
		sortO(){
		      // 
		}
	}
});
