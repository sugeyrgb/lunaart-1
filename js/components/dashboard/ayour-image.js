const AYourComponent = Vue.component("your-image",{
	template: 
	` <div class='cs-container'>

		<b-container fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-form-input v-model="search" @input="sortYourList()" placeholder="ORDER NUMBER" autocomplete="off" />
			
				</b-col>
			</b-row>
			
			<b-row class="text-center padding" v-if='loading'>
              <b-col>
                <l-spinner color="luna-text-gold"></l-spinner>
              </b-col>
            </b-row>
			
			<div v-if="!sortYours.length">

				<b-row>
					<b-col sm=12 v-if="yours.length">
						<b-col class="padding">

			                <div class="table-responsive">
			                    <table class="table table-hover text-center" style="width: 100%!important">
			                      <thead>
			                        <tr>
			                          <th v-for="f in infields" scope="col">{{ f }}</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                        <tr v-for="pac in yours">
			                        	<th scope="row">#{{ pac.number }}</th>
										<td><a :href="pac.clientImg" download>Download</a></td>
										<td><a :href="pac.editImg" download>Downlad</a></td>
										<td>{{ pac.email }}</td>
										<td>{{ pac.fdated }}</td>
			                        </tr>
			                      </tbody>
			                    </table>
			                </div>

		                </b-col>
					</b-col>
				
					<b-col sm=12 class="text-center" v-else>
						<p class="text-center" >There's no images request lists</p>
					</b-col>
				</b-row>
			
			</div>

			<div v-else>
				<b-row>
					<b-col sm=12 v-if="sortYours.length">
						<b-col class="padding">

			                <div class="table-responsive">
			                    <table class="table table-hover text-center" style="width: 100%!important">
			                      <thead>
			                        <tr>
			                          <th v-for="f in infields" scope="col">{{ f }}</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                        <tr v-for="pac in sortYours">
			                        	<th scope="row">#{{ pac.number }}</th>
										<td><a :href="pac.clientImg" download>Download</a></td>
										<td><a :href="pac.editImg" download>Downlad</a>/td>
										<td>{{ pac.email }}</td>
										<td>{{ pac.fdated }}Lbs</td>
			                        </tr>
			                      </tbody>
			                    </table>
			                </div>

		                </b-col>
					</b-col>
				
					<b-col sm=12 class="text-center" v-else>
						<p class="text-center" >There's no match packing lists</p>
					</b-col>
				</b-row>
			</div>
			
		</b-container>
	  </div>
	  `,
	data() {
		return {
			search: "",
			infields: ["ORDER", "ORIGINAL IMAGE", "EDITED IMAGE", "CLIENT EMAIL", "DATE"],

			orders: [],

			loading: true,

			yours: [],

			sortYours: [],

			vd: new VueDiana()
		}
	},
	created(){
		this.$root.$data.componentTitle = "Your Images requests";
		this.searchYours();
	},
	methods: {
		sortYourList(){
			let main = this;
			this.sortYours = this.yours.filter(function(index) {
				
				if(index.number.toLowerCase().match(main.search.toLowerCase()) || index.fdated.toLowerCase().match(main.search.toLowerCase())){
					return index;
				}

				console.log(index.number.toLowerCase())
				console.log(main.search.toLowerCase())
				
			});
		},
		searchYours(){
			var vd = new VueDiana();
			var main = this;
			main.loading = true;

			main.yours = [];

			axios({
				method: 'get',
				url: 'orders?g=yours',
			}).then(function (response) {
			  //handle success
			  console.log(response);
			  console.log(response.data);

			  let call = response.data.call;

			   switch(call){

			     case true:
			     	if(response.data.info.length){
			     		main.yours = response.data.info;
			     	}else{
			     		main.yours = []
			     	}
			     	
			     break;

			     case false:
			       switch(response.data.errors){

			         default:
			           vd.toastError();
			         break;
			       }
			     break;
			     
			     default:
			       vd.toastError();
			     break;
			   }
			}).catch(function (response) {
			  //handle error
			  console.log(response);
			}).then(function () {
				main.loading = false;
			});  
		}
	}
});
