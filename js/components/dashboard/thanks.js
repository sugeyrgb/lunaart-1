const ThanksComponent = Vue.component("empty",{
		template: 
	`<div style="padding-top: 10%;">
		<b-container fluid>
			<b-row>
				<b-col sm=12 class="text-center">
					<p>Thanks for your payment. We've already received the payment for your order</p>
				</b-col>

				<b-col sm=12 class="text-center">
					<span class="animated fadeIn gold-flat-text"><i class="fas fa-check fa-3x"></i></span>
				</b-col>
			</b-row>
		</b-container>
	</div>
	  `,
	data() {
		return {
			
		}
	},
	created(){
	},
	update(){

	},
	methods: {

	}
});