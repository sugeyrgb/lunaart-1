const DashComponent = { 
  template: 
  `<div class="cs-container">
		<b-container v-if="role != 2">
			<b-row>
				<b-col class="text-center">
					<h4 class="elmessiri animated fadeInUp">
						Welcome to panel area
					</h4>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
			
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-input-group class="mt-3">
						
						<b-form-input v-model="search" @input="generalSearch" placeholder="SEARCH" autocomplete="off" />
						<b-input-group-append>
							<b-button>SEARCH</b-button>
						</b-input-group-append>

					</b-input-group>
					
				</b-col>
			</b-row>

			<div v-if="!search">
				<b-row style="padding-top: 2">
					<b-col sm=12 md=5>
						<h2 class='luna-text-blue' style="font-size: 3em;">{{ $root.$data.session.reward }}%</h2>
						<span class='luna-text-lightGrey'>Your reward percentage</span>
					</b-col>

					<b-col sm=12 md=7 class='text-right' style="padding-top: 60px;">
						<div class='progress-container'><b-progress :value="parseInt($root.$data.session.reward)" :max="100" class="mb-3"></b-progress></div>
						<span class='luna-text-lightGrey'>Until next reward Lv.</span>
					</b-col>
				</b-row>
				
				<b-row>
					<b-col sm=12><h4 class='luna-text-blue sub-title padding' >Pending orders</h4></b-col>
					<b-col sm=12>
						<p class='gold-flat-text font-boldy' style='margin-bottom: 0px; font-size: 1.5em;' v-for="(o,i) in pendings">PO: {{ o.number }}</p>
					</b-col>
				</b-row>
			</div>
		
			<b-row v-else>
				<b-container fluid v-if="generalResults.length">
					<b-row v-if="generalResults.orders.length">
						<b-col>
							<h4 class="gold-flat-text font-boldy">Orders</h4>
							<div class="table-responsive">
					            <table class="table table-hover text-center with-grid" style="width: 100%!important">
					              <thead>
					                <tr>
					                  <th v-for="f in infieldsOrders" scope="col">{{ f }}</th>
					                </tr>
					              </thead>
					              <tbody>
					                <tr v-for="ord in generalResults.orders">
					                  <th scope="row">#{{ ord.number }}</th>
					               	  <td>{{ '$'+""+ord.content.details.po }}</td>
					               	  <td v-if="ord.invoice != null" class="text-center"><b-button block v-on:click.stop="viewInvoice(ord.invoice.pdf)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
					               	  <td v-else class="text-center"><b-button block disabled >Not set yet</b-button> </td>

					               	  <td>{{ ord.content.details.quantity }}</td>

					               	  <td>{{ (ord.comment != "" ? ord.comment : 'Not description') }}</td>
					               	  <td>{{ (ord.rdated != null ? ord.rdated : 'Not received yet') }}</td>
					               	  <td>{{ (ord.adated != null ? ord.adated : 'Not approved yet') }}</td>
					                </tr>
					              </tbody>
					            </table>
					        </div>
						</b-col>
					</b-row>

					<b-row v-if="generalResults.trackings.length">
						<b-col>
							<h4 class="gold-flat-text font-boldy">Tracking lists</h4>
							<div class="table-responsive">
			                    <table class="table table-hover text-center" style="width: 100%!important">
			                      <thead>
			                        <tr>
			                          <th v-for="f in infieldsTrack" scope="col">{{ f }}</th>
			                        </tr>
			                      </thead>
			                      <tbody>
			                        <tr v-for="pac in generalResults.trackings">
			                          <th scope="row">#{{ pac.number }}</th>
			                       	  <td>{{ pac.track }}</td>
			                       	  <td>{{ pac.fdated }}</td>
			                       	  <td>{{ pac.boxes }}</td>
			                       	  <td>{{ pac.weight }}Lbs</td>
			                       	  <td>{{ pac.width }}" x {{ pac.height }}"</td>
			                       	  <td>{{ statusDisplay[pac.status] }}</td>
			                       	  <td>{{ pac.carrier }}</td>
			                       	  <td>{{ pac.origin }}</td>
			                       	  <td>{{ pac.destiny }}</td>
			                        </tr>
			                      </tbody>
			                    </table>
			                </div>
						</b-col>
					</b-row>

					<b-row v-if="generalResults.collections.length">
						<b-col sm=12>
							<h4 class="gold-flat-text font-boldy">Collections</h4>
						</b-col>
						<b-col sm=3 v-for="(m,index) in generalResults.collections">
							<b-card style="border: none;" no-body :img-src="m.img" img-alt="Image" class="collection-card" img-top>
								<p class="text-center"> <span class='gold-flat-text'>{{ m.title }}</span><br>{{ m.materialsStr }}</p>
						    </b-card>
						</b-col>
					</b-row>
				</b-container>

				

				<b-container v-else>
					<b-row>
						<b-col class="text-center">
							<p>There's no matching results</p>
						</b-col>
					</b-row>
				</b-container>
			</b-row>

		</b-container>
	</div>`,
  data(){
  	return {
  		role: false,
  		search: null,
  		pendings: [],
  		generalResults: [],

		infieldsOrders: ["NUMBER","P.O","INVOICE","QTY","DESCRIPTION","RCVD DATE", "APRVL DATE"],
		infieldsTrack: ["ORDER", "TRACKING NUMBER", "DATE", "TOTAL BOXES", "TOTAL WEIGHT", "SIZE", "STATUS", "CARRIER", "ORIGIN", "DESTINY"],
		statusDisplay: ["Preparing", "Shipped", "In transit", "Delivered", "Canceled"],

  		vd: new VueDiana()
  	}
  },
  created(){
  	this.loadPending();
  	this.$root.$data.componentTitle = "Welcome";
  	console.log(this.$root.$data["session"]);
  	this.role = (parseInt(user.role) <= 1 ? true : false);
  },
  methods: {
  	viewInvoice(url){
  		window.open(url);
  	},
  	loadPending(){
  		  var main = this;
  		  NProgress.start();

	    axios({
	        method: 'get',
	        url: 'orders?g=p',
	    }).then(function (response) {
	          //handle success
	          console.log(response);
	          console.log(response.data);

	          let call = response.data.call;

	           switch(call){

	             case true:
	               if(response.data.hasOwnProperty("info")){
	                  main.pendings = response.data.info;
	               }else{
	                 main.pendings = [];
	               }
	              
	             break;

	             case false:
	               switch(response.data.errors){

	                 default:
	                   vd.toastError();
	                 break;
	               }
	             break;
	             
	             default:
	               vd.toastError();
	             break;
	           }
	    }).catch(function (response) {
	          //handle error
	          console.log(response);
	    }).then(function () {
	        NProgress.done();
	    }); 
  	},
  	generalSearch(){
  		var main = this;
  		NProgress.start();

		axios({
			method: 'get',
			url: 'lpage?g=d&s='+main.search,
		}).then(function (response) {
		  //handle success
		  console.log(response);
		  console.log(response.data);

		  let call = response.data.call;

		   switch(call){

		     case true:
		       if(response.data.hasOwnProperty("info")){
		          main.generalResults = response.data.info;
		          main.generalResults.length = true;
		       }else{
		         main.generalResults = [];
		         main.generalResults.length = false;
		       }
		      
		     break;

		     case false:
		       switch(response.data.errors){

		         default:
		       		main.vd.toastError();
		         break;
		       }
		     break;
		     
		     default:
		    	main.vd.toastError();
		     break;
		   }
		}).catch(function (response) {
		  //handle error
		  console.log(response);
		}).then(function () {
			NProgress.done();
		}); 
  	}
  }
};