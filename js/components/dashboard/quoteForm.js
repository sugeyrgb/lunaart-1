const QuoteFormComponent = Vue.component("quote-form",{
	template: 
		`<div>
			<b-container fluid>
				<b-row>
					<b-col><h5 class="gold-flat-text">Fill the following information</h5></b-col>
				</b-row>

				<b-row>
					<b-col md=3>
						<b-col cols=12 style="margin-bottom: 3px;">
							<b-form-checkbox v-model="form.floor">Floor</b-form-checkbox> <br>
						</b-col>
						<b-col cols=12 style="margin-bottom: 3px;">
							<b-form-checkbox v-model="form.medallion">Medallion</b-form-checkbox>
						</b-col>
					</b-col>
					<b-col md=9>
						<b-col>
							<div role="group" class="form-row form-group" style="margin-bottom: 3px;">
								<label for="input-default" class="col-lg-5 col-7 col-form-label">Square feet</label>
								<div class="col">
									<input type="text" class="form-control" />
								</div>
							</div>
						</b-col>
						<b-col>
							<div role="group" class="form-row form-group" style="margin-bottom: 3px;">
								<label for="input-default" class="col-lg-5 col-7 col-form-label">Medallion measurements</label>
								<div class="col">
									<input type="text" class="form-control" />
								</div>
							</div>
						</b-col>
					</b-col>
				</b-row>	

				<b-row>
					<b-col cols=12>
						<h5>CUT TYPE</h5>
					</b-col>
					<b-col>
						<b-form-radio v-for="(cut,index) in cutTypeOptions" name="cut-type" v-model="form.cutType" :value="index">{{ cut }}</b-form-radio>
					</b-col>
				</b-row>

				<b-row>
					<b-col cols=12>
						<b-form-textarea
							v-model="form.comments"
							rows="3"
							max-rows="6"
						></b-form-textarea>
					</b-col>
					
					<b-col cols=12 class="text-right">
						<material-select-component @material-selected="addMaterial" mainStyle="more-materials-quote" buttonValue="Add materials columns <i class='material-icons'>add_box</i>"></material-select-component>
					</b-col>
				</b-row>

				<b-row>
					<b-col cols=12 md=4 v-for="(m,index) in materials">
						<b-row>
							<b-col cols=4>
								<b-img :src="m.img" fluid ></b-img>
							</b-col>
							<b-col cols=8>
								<b-col>
									 <b-button class="btn btn-flat" @click="removeMaterial(index)">
										<i class="material-icons">
											delete_forever
										</i>
									 </b-button>
									 
									 <span class="gold-flat-text">Section {{ (index+1) }}</span>
								</b-col>
		
								<b-col cols=12>
									<p>Name: {{ m.title }}</p>
								</b-col>
							</b-col>
						</b-row>

						<b-col cols=12>
							<!-- TDF -->
							<b-row v-for="(item,c) in tdf">
								<b-col cols=4><b-form-checkbox v-model="m.tdf[c].active" :value="c">{{ item }}</b-form-checkbox></b-col> 
								<b-col cols=8><b-input v-model="m.tdf[c].value" /></b-col>
							</b-row>
						</b-col>

						<b-col cols=12>
							<span>FINISH</span>
							<div v-for="(finish, f) in finishs"><b-form-radio :name="'cut-type-'+index" v-model="form.finish" :value="f">{{ finish }}</b-form-radio></div>
						</b-col>

						<b-col cols=12>
							<span>SELECTION</span>
							<div v-for="(select, s) in selection"><b-form-radio :name="'select-'+index" v-model="form.selection" :value="s">{{ select }}</b-form-radio></div>
						</b-col>
					</b-col>
				</b-row>

			</b-container>
		</div>`,
	data() {
		return {

			form: {
				floor: null,
				medallion: null,
				cutType: null,
				comments: null,
				po: null,
			},

			cutTypeOptions: [
				"Not cut type selected",
				"Water Jet",
				"Cut by hand",
				"Mix cut",
				"Nominal dimension",
				"Real dimension",
			],

			tdf: [
				"Trim",
				"Dot",
				"Field"
			],

			finishs: [
				"Polished",
				"Honed",
				"Tumbled"
			],

			selection: [
				"Clean select",
				"Clean",
				"Regular",
				"Few veins"
			],

			materials: [],

			vd: new VueDiana()
		}
	},
	created(){
	},
	update(){

	},
	methods: {
		
		addMaterial(material){
			material["tdf"] = [
				{
					active: false,
					value: ""
				},
				{
					active: false,
					value: ""
				},
				{
					active: false,
					value: ""
				}
			]

			//0: Polished, 1: Honed, 2: Field
			material["finish"] = null;
			material["selection"] = null;

			if(this.materials.length <= 8){
				this.materials.push(material);
			}else{
				this.vd.toastError("Material limit", "You can only add 8 materials");
			}
			
		},
		removeMaterial(index){
			this.materials.splice(index,1);
		},
		setQuoteForm(){
			this.form["materials"] = this.materials;

			this.$emit("quote-form", this.form);
		}
	}
});