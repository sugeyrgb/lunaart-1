const MyRequestComponent = Vue.component("my-request",{
		template: 
			`<div>
				<b-container fluid>
					<b-row>
						<b-col sm=12 md=6>
							<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
						</b-col>
					</b-row>

					<b-row>
						<b-col>
							<div class="table-responsive">
								<table class="table text-center with-grid" style="width: 100%!important">
								<thead>
									<tr>
										<th v-for="f in infieldsRequests" scope="col">{{ f }}</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="req in requests" >
										
									<td>{{ req.fdated }}</td>
										
										<td>
											<div v-if="req.custom == 1">
												<b-img  fluid src="media/img/asset.png" style="max-width: 64px;"></b-img> <br/>
												<span>CUSTOM</span>
											</div>
											<div v-else>
												<b-img  fluid :src="req.img" style="max-width: 64px;"></b-img> <br/>
												<span>{{ req.content.pattern.title }}</span>
											</div>
										</td>
	
										<td v-if="req.pdf == 1"><b-button block @click="viewOrder(req.id)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
										<td v-else><span class="grey-text">Pending</span></td>
	
										<td v-if="req.assignedTo != null">{{ req.assignedTo }}</td>
										<td v-else><span class="grey-text">Pending</span></td>
									</tr>
								</tbody>
								</table>
							</div>
						</b-col>
					</b-row>
				</b-container>
			</div>`,
	data() {
		return {
			infieldsRequests: ["DATE", "ORDER DETAILS", "PDF", "ASSIGNED TO"],

			requests: [],
			
			vd: new VueDiana()
		}
	},
	created(){
		this.$root.$data.componentTitle = "My requests";
		this.getRequests();
		console.log("My request")
	},
	update(){

	},
	methods: {
		viewOrder(){
			this.vd.toastSuccess("","What do you want here?");
		},
		getRequests(){
			var main = this;
			NProgress.start();

		    main.requests = [];

		    axios({
		        method: 'get',
		        url: 'my-request?g=all',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
						var aux =  response.data.info;
						/*
					 	for(var key in aux){
							aux[key]["content"] = JSON.parse(aux[key]["content"]);
						}*/

		             	main.requests = response.data.info;

		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   main.vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               main.vd.toastError();
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    }).then(function () {
		        NProgress.done();
		        main.$forceUpdate();
		    });  
		}
	}
});