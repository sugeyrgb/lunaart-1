const DisplayGalleryComponent = Vue.component("displayGallery",{
	//Side default is null, 1 equals to landing page and 2 is admin dashboard, 3 is client dashboard.
	props:{
		side: null
	},
	template: 
	`<div>
		
		<b-container fluid v-if=loading>
			<b-row v-if="galleries.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
				
			<b-container class="padding">
				
				<b-row>

					<b-input-group class="mt-3">
				       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH PATTERN" autocomplete=off />
				    </b-input-group>

				</b-row>


			</b-container>

			<b-container fluid v-if="sorting">
				
				<b-row>

					<b-col sm=3  v-for="m in sortingM">
						<b-card no-body :img-src="m.img" img-alt="Image" :class="gallery-card" img-top>
							<div class="card-header">
								{{ m.title }}
								<br>
								<b-button variant="danger" @click="deleteItem($event,m.id)"><i class="fas  fa-times"></i></b-button>
							</div>
					    </b-card>
					</b-col>

				</b-row>


			</b-container>

			<b-container fluid v-else>

				<b-tabs class="nav-justified" content-class="mt-3" v-if="galleries.length">
					<b-tab title="Kitchen" active>
						
						<b-container fluid>
							<b-row>
								<b-col sm=3  v-for="m in kitchen"  >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card"  img-top>
										<div class="card-header">
											{{ m.title }}
											<br>
											<b-button variant="danger" @click="deleteItem($event,m.id)"><i class="fas fa-1x fa-times"></i></b-button>
										</div>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Bathroom">
						
						<b-container fluid>	
							<b-row>
							
								<b-col sm=3  v-for="m in bathroom" >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
										<div class="card-header">
											{{ m.title }}
											<br>
											<b-button variant="danger" @click="deleteItem($event,m.id)"><i class="fas fa-1x fa-times"></i></b-button>
										</div>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Comercial space">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3  v-for="m in comercialSpace" >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
										<div class="card-header">
											{{ m.title }}
											<br>
											<b-button variant="danger" @click="deleteItem($event,m.id)"><i class="fas fa-1x fa-times"></i></b-button>
										</div>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Living space">
						
						<b-container fluid>	
							<b-row>
							
								<b-col sm=3  v-for="m in livingSpace" >
									<b-card no-body :img-src="m.img" img-alt="Image" class="gallery-card" img-top>
										<div class="card-header">
												{{ m.title }}
												<br>
												<b-button variant="danger" @click="deleteItem($event,m.id)"><i class="fas  fa-times"></i></b-button>
											</div>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>

					</b-tab>
				</b-tabs>

				<b-row v-else>
					<b-col class="text-center">
						<p>There's no items in any gallery</p>
					</b-col>
				</b-row>
			</b-container>
		 </b-container>
	</div>
	  `,
	data() {
		return {
			galleries: [],

			kitchen: [],
			bathroom: [],
			comercialSpace: [],
			livingSpace: [],

			loading: true,
			sorting: "",
			sortingM: [],

			vd: new VueDiana()
		}
	},
	mounted(){
		console.log(this.galleries);
		console.log(this.sortingM);
	},
	created(){
		this.$root.$data.gallery = [];
		var main = this;
		main.loadGallery();

		console.log("Hi")

		window.addEventListener("reloadGallery",function(){
			main.loadGallery();
			main.$forceUpdate();
		});
	},
	updated(){
		console.log(this.galleries);
		console.log(this.sortingM);
	},
	methods: {
		deleteItem(e,id){
			e.preventDefault();

			if(confirm("Are you sure to delete gallery item?")){

		      let main = this;
		      NProgress.start();
		      
		      var updateForm = new FormData();

		      updateForm.append("id",id);
		      updateForm.append("request","delete");
		      updateForm.append("method","post");

		      console.log(this.galleryNew);

		      axios.post('gallery',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errorsColl = [];

		             switch(call){

		               case true:
		                 main.vd.toastSuccess();
		                 window.dispatchEvent(new Event("reloadGallery"));
		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NotEnoughForm":
		                     main.errorsColl.push({message:"Must fill all fields in form"});
		                   break;
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
			    	NProgress.done();
			    });
			}
		},
		searchMat(){
			var main = this;
			
			this.sortingM = this.galleries.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});
		},
		loadGallery(){
			//asset?r=designpattern&a=da
			 var main = this;

			 main.loading = true;

			 main.galleries = [];

			 main.kitchen = [];
			 main.bathroom = [];
			 main.comercialSpace = [];
			 main.livingSpace = [];

			  axios({
		        method: 'get',
		        url: 'gallery?g=a',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.galleries = response.data.info;

		                  	main.sortingM = main.galleries;

							for(var m in main.galleries){
						  
						      	main.galleries[m].category = parseInt(main.galleries[m].category);
						      	
						      	switch(main.galleries[m].category){
							  		case 1:
							  			main.kitchen.push(main.galleries[m]);
							  		break;

							  		case 2:
							  			main.bathroom.push(main.galleries[m]);
							  		break;

							  		case 3:
							  			main.comercialSpace.push(main.galleries[m]);
							  		break;

							  		case 4:
							  			main.livingSpace.push(main.galleries[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.galleries = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
	}
});

