const AGalleryComponent = Vue.component("agallery",{
	template: 
	`<div class="cs-container">
		<b-container fluid>
				<b-row>
					<b-col sm=12 >
						<h4 class="component-title padding">{{ $root.$data.componentTitle }}</h4>
					</b-col>
				</b-row>
			
		    	<b-row v-if="errorsColl.length">
                  <b-col sm=12 v-for="error in errorsColl">
                    <b-alert variant="danger" show>{{ error.message }}</b-alert>
                  </b-col>
                </b-row>
				
				<b-row>
				  <b-col sm=12 md=3 class="padding">
				  	<b-button block v-b-toggle.collapse1>Add image</b-button>
				  </b-col>
				  	
				</b-row>

		    	<b-row>
					  <b-col sm=12>
						  <b-collapse id="collapse1">
							<b-card>

							      <b-form-group>
 								    <b-form-input v-model="galleryNew.title" type="text" placeholder="NAME" autocomplete="off" />
								  </b-form-group>

						    	  <b-form-group description="Image and materials that will be displayed in the landing page">
	                                  <b-form-file
	                                    v-model="galleryNew.file"
	                                    placeholder="Chose pattern display image..."
	                                    drop-placeholder="Drop file here..."
	                                    accept="image/*"
	                                  />

                                  </b-form-group>
								  
								  <b-form-group label="Bind to collection">
								  	
                                    <b-form-select v-model="galleryNew.collection" :options="collections" />
                                  </b-form-group>

                                  <b-form-group>
                                    <b-form-select v-model="galleryNew.category" :options="gCat" />
                                  </b-form-group>

								  <b-col class="text-right padding">
                                     <button @click="uploadGallery" class="btn-blue">Upload</button>
                                  </b-col>
							    </b-card>
					    
						  </b-collapse>
					  </b-col>
				</b-row>

				<b-container fluid v-if="!display">
					<b-container fluid v-if="loading">
						<b-col class="text-center padding">
							<l-spinner color="luna-text-gold"></l-spinner>
						</b-col>
					</b-container>
					
					<b-container v-else>
						<displayGallery :side=2 v-if="galleries.length"></displayGallery>

						<b-row v-else>
							<b-col class="text-center">
								<p>There's no patterns in your library</p>
							</b-col>
						</b-row>
					</b-container>
					
				</b-container>
			    
		</b-container>
	 </div> `,
	data() {
		return {
			sizes: ["Hi","Ah","Lalas"],
			tag: '',
      		tags: [],

      		galleryNew: {
      			category: null,
      			collection: null
      		},

      		loading: true,

      		display: false,

			errorsColl: [],

			galleries: [],

			gCat: [
				{text: 'Category', value: null },
				{text: "Kitchen",value: 1},
				{text: "Bathroom",value: 2},
				{text: "Comercial space",value: 3},
				{text: "Living space",value: 4}
			],

			collections: [],

			vd: new VueDiana(),

		}
	},
	mounted(){
		this.loadGallery();
		this.loadCollection();
		this.$root.$data.componentTitle = "Admin gallery";
	},
	updated(){
	},	
	methods: {
		uploadGallery(e){
			e.preventDefault();
			
		      let main = this;
		      NProgress.start();

		      var updateForm = new FormData();

		      for(var input in this.galleryNew){
		        updateForm.append(input,this.galleryNew[input]);
		      }

		      updateForm.append("request","add");
		      updateForm.append("method","post");

		      console.log(this.galleryNew);

		      axios.post('gallery',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errorsColl = [];

		             switch(call){

		               case true:
		                 main.vd.toastSuccess();
		                 window.dispatchEvent(new Event("reloadGallery"));
		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NotEnoughForm":
		                     main.errorsColl.push({message:"Must fill all fields in form"});
		                   break;

		                   case "NoFile":
		                     main.errorsColl.push({message:"An image is needed to upload the gallery item"});
		                   break;

		                   case "NotFileMoved":
		                   	 main.errorsColl.push({message:"Couldn't move file to server"});
		                   break;

		                   case "AlreadyTitle":
		                   	 main.errorsColl.push({message:"Another item has that title"});
		                   break;

		                   default:
		                     main.vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
			    	NProgress.done();
			    });

		  
		},
		loadCollection(e){

			var main = this;
			 main.collections = [];
			 axios({
		        method: 'get',
		        url: 'asset?r=collection&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		               	  main.collections.push({text: 'Select collection', value: null });

		               	  for(var key in response.data.info){
		               	  	 main.collections.push(response.data.info[key]);
		               	  }
		                 
		               }else{
		                  main.collections = [];
		               }
		              
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		      	vm.$forceUpdate();
		      });
		},
		loadGallery(){
			var main = this;
			main.galleries = [];

			main.loading = true;
			
			axios({
		        method: 'get',
		        url: 'gallery?g=a',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		               	  main.galleries = response.data.info;
		               	  window.dispatchEvent(new Event("reloadGallery"));
		               }else{
		                  main.galleries = [];
		               }
		             break;
		             
		             default:
		               main.vd.toastError();
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    }).then(function () {
		      	vm.$forceUpdate();
		      	main.loading = false;
		    });
		},

	}
});