const ProfileComponent = Vue.component("profile",{
  template: `
  <div class="cs-container contianer-profile">

    <b-container fluid class="material-input-container">
      <b-row>
        <b-col sm=12 >
          <h4 class="component-title padding">{{ $root.$data.componentTitle }}</h4>
        </b-col>
      </b-row>
      <b-row>
          <b-col sm=12 class="text-center">
            <b-img fluid circle rounded="circle" style="max-width: 164px;" :src="setImg(profile.img)" alt="Profile" />

            <br>

            <label for="file-input" class='profile-label'>
              Change
            </label>
            <input id="file-input" v-on:change="setImage" class="hidden" type="file" accept="image/*" />
          </b-col>
      </b-row>

      <b-row>
        <b-col>
          <b-form-group
            label="Name"
          >
            <b-form-input v-model="update.name" placeholder="NAME" trim autocomplete="off"></b-form-input>
          </b-form-group>
        </b-col>


        <b-col>
          <b-form-group
            label="Lastname"
          >
            <b-form-input v-model="update.lastname" placeholder="LASTNAME" trim autocomplete="off"></b-form-input>
          </b-form-group>
        </b-col>
    
      </b-row>

      <b-row v-if="profile.role > 1">
        <b-col>
          <b-form-group
            label="Company"
          >
            <b-form-input v-model="update.business" placeholder="COMPANY" trim autocomplete="off"></b-form-input>
          </b-form-group>
        </b-col>
      </b-row>

      <b-row>
        <b-col>
          <b-form-group
            label="Email"
          >
            <b-form-input v-model="update.email" placeholder="EMAIL" trim autocomplete="off"></b-form-input>
          </b-form-group>
        </b-col>
      </b-row>

      <b-row>
        <b-col>
          <b-form-group
            label="Phone"
          >
            <b-form-input v-model="update.phone" placeholder="PHONE" trim autocomplete="off"></b-form-input>
          </b-form-group>
        </b-col>
      </b-row>
      
      <div v-if="profile.role > 1">

      
        <b-row>
          <b-col>
            <b-form-group
              label="ADDRESS"
            >
              <b-form-input v-model="update.address" placeholder="ADDRESS" trim autocomplete="off"></b-form-input>
            </b-form-group>
          </b-col>
        </b-row>

        <b-row>
          <b-col>
            <b-form-group
              label="CITY"
            >
              <b-form-input v-model="update.city" placeholder="CITY" trim autocomplete="off"></b-form-input>
            </b-form-group>
          </b-col>

          <b-col>
            <b-form-group
              label="ZIP CODE"
            >
              <b-form-input v-model="update.zp" placeholder="CITY" trim autocomplete="off"></b-form-input>
            </b-form-group>
          </b-col>
        </b-row>

        <b-row>
          <b-col>
            <b-form-group
              label="STATE"
            >
              <b-form-select v-model="update.state" :options="states" ></b-form-select>
            </b-form-group>
          </b-col>
        </b-row>

      </div>
    </b-container>

    <b-container fluid>
        <b-row>
          <b-col>
            <b-form-group
              label="NEW PASSWORD"
            >
              <b-form-input v-model="update.pass1" placeholder="NEW PASSWORD" trim autocomplete="off"></b-form-input>
            </b-form-group>
          </b-col>
        </b-row>

        <b-row>
          <b-col>
            <b-form-group
              label="CONFIRM PASSWORD"
            >
              <b-form-input v-model="update.pass2" placeholder="CONFIRM PASSWORD" trim autocomplete="off"></b-form-input>
            </b-form-group>
          </b-col>
        </b-row>
        
        <b-row v-if="errors.length">
          <b-col sm=12 v-for="error in errors">
            <b-alert variant="danger" show>{{ error.message }}</b-alert>
          </b-col>
        </b-row>

        <b-row>
          <b-col class="text-right">
            <b-button class='btn-gold' @click="updateProfile">SAVE</b-button>
          </b-col>
        </b-row>
    </b-container>
  </div>`,
  data:function(){
    return {
        profile: [],

        update: {

        },

        states: [],

        errors: [],

        vd: new VueDiana()
    }
  },
  created(){
     let main = this;
     this.$root.$data.componentTitle = "Profile";
     this.loadProfile();
     this.loadStates();
  },
  methods: {
    setImage(e){
      try{
        this.update.file = e.target.files[0];

        this.update.img = URL.createObjectURL(this.update.file);
      }catch(ex){}
      
    },
    setImg(img){
      return img;
    },
    loadStates(){
      var main = this;
      axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8303e1d831e89cb8af24a11f2fa77353c317e408/states_titlecase.json').then(function(response){
        let lalas = response.data;
        for(var key in response.data){
          var temp = {
            text: response.data[key].name,
            value: response.data[key].abbreviation,
          }

          main.states.push(temp);
        }
        console.log(main.states);
      }).catch(function(error){
        console.log(error);
      });
    },
    updateProfile(e){
      e.preventDefault();

      let main = this;
      NProgress.start();

      console.log(this.update);

      var updateForm = new FormData();

      for(var input in this.update){
        updateForm.append(input,this.update[input]);
      }

      updateForm.append("file",this.update.file);
      updateForm.append("img",this.update.img);

      updateForm.append("request","update");
      updateForm.append("method","post");

      axios.post('profile',updateForm,{
        headers: {
           'content-type': 'multipart/form-data'
        }
      }).then(function (response) {
            //handle success
            console.log(response);
            console.log(response.data);

            let call = response.data.call;

            main.errors = [];

             switch(call){

               case true:
                 iziToast.show({
                  timeout: 5000,
                  title: "",
                  message: "Some changes will be taken in the next login",
                  class: "success-white",
                  backgroundColor: "#B99168",
                  color: "white",
                  timeout: 3000,
                  progressBar: false
                });
               break;

               case false:
                 switch(response.data.errors){

                   case "EmailExist":
                     main.errors.push({message:"That email is already in use"});
                   break; 

                   case "NotMatchPass":
                     main.errors.push({message:"New passwords doesn't match"});
                   break;

                   case "NotEnoughForm":
                     main.errors.push({message:"Must fill all fields in form"});
                   break;

                   case "ShortPass":
                     main.errors.push({message:"New password is too short, must be at least 6 characters"});
                   break; 

                   default:
                     main.vd.toastError();
                   break;
                 }
               break;
               
               default:
                 main.vd.toastError();
               break;
             }
        }).catch(function (response) {
            //handle error
            console.log(response);
        }).then(function () {
          NProgress.done();
        });
    },
    loadProfile(){
      NProgress.start();

      let main = this;

      axios({
        method: 'get',
        url: 'profile?g=me',
      }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

           switch(call){

             case true:
               main.profile = response.data.info[0];

               main.update = response.data.info[0];

             break;

             case false:
               switch(response.data.errors){

                 default:
                   vd.toastError();
                 break;
               }
             break;
             
             default:
               vd.toastError();
             break;
           }
      }).catch(function (response) {
          //handle error
          console.log(response);
      }).then(function () {
         NProgress.done();
      }); 
    }
  }
      //notifications?su=em

});