const MaterialComponent = Vue.component("material",{
	//Side default is null, 1 equals to landing page and 2 is admin dashboard, 3 is client dashboard.
	props:{
		side: null
	},
	template: 
	`<div>
		
		<b-container fluid v-if=loading>
			<b-row v-if="materials.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
				
			<b-container class="padding">
				
				<b-row>

					<b-input-group class="mt-3">
				       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH MATERIAL" autocomplete=off />
				    </b-input-group>

				</b-row>


			</b-container>

			<b-container v-if="sorting">
				
				<b-row>

					<b-col sm=4 md=2 v-for="m in sortingM">
						<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" @click="deleteMaterial(m)" img-top>
							<span class='material-title'>{{ m.title }}</span>
					    </b-card>
					</b-col>

				</b-row>


			</b-container>

			<b-container fluid v-else>

				<b-tabs class="nav-justified" content-class="mt-3" v-if="materials.length">
					<b-tab title="Stone" active>
						
						<b-container>
							<b-row>
								<b-col sm=4 md=2 v-for="m in stone">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" @click="deleteMaterial(m)" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Mirror">
						
						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in mirror" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" @click="deleteMaterial(m)" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Metal">
						
						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in metal" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" @click="deleteMaterial(m)" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Glass">
						
						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in glass" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" @click="deleteMaterial(m)" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Gemstones">

						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in gemstones" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" @click="deleteMaterial(m)" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>
						
					</b-tab>
				</b-tabs>

				<b-row v-else>
					<b-col class="text-center">
						<p>There's no materials in your library</p>
					</b-col>
				</b-row>
			</b-container>
		 </b-container> 
	</div>
	  `,
	data() {
		return {
			materials: [],
			stone: [],
			mirror: [],
			metal: [],
			glass: [],
			gemstones: [],
			loading: false,
			sorting: "",
			sortingM: []
		}
	},
	mounted(){
		console.log(this.materials);
		console.log(this.sortingM);
		

	},
	created(){
		var main = this;
		main.loadMaterial();

		window.addEventListener("reloadMaterials",function(){
			main.loadMaterial();
			main.$forceUpdate();
		});
	},
	updated(){
		console.log(this.materials);
		console.log(this.sortingM);
	},
	methods: {
		searchMat(){
			var main = this;
			
			this.sortingM = this.materials.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});

			console.warn(this.sortingM)
		},
		deleteMaterial(m){
			   var main = this;
		       var updateForm =  new FormData();
		       var vd = new VueDiana();

		       if(confirm("Are you sure that you want delete this material?")){
		       	 NProgress.start();
		         updateForm.append("id",m.id);
		         updateForm.append("request","material");
		         updateForm.append("action","delete");
		         updateForm.append("method","post");

		        axios.post('asset',updateForm,{
		          headers: {
		             'content-type': 'multipart/form-data'
		          }
		        }).then(function (response) {
		              //handle success
		              console.log(response);
		              console.log(response.data);

		              let call = response.data.call;

		              main.errors = [];

		               switch(call){

		                 case true:
		                   vd.toastSuccess();
		                   main.loadMaterial();
						   main.$forceUpdate();
		                 break;

		                 case false:
		                   switch(response.data.errors){

		                     default:
		                       vd.toastError();
		                     break;
		                   }
		                 break;
		                 
		                 default:
		                   vd.toastError();
		                 break;
		               }
		          }).catch(function (response) {
		              //handle error
		              console.log(response);
		          }).then(function () {
		         	NProgress.done();
		        });
		       }
		},
		loadMaterial(){
			//asset?r=material&a=da
			 var main = this;

			 main.loading = true;

			 main.materials = [];

			 main.stone = [];
			 main.mirror = [];
			 main.metal = [];
			 main.glass = [];
			 main.gemstones = [];

			  axios({
		        method: 'get',
		        url: 'asset?r=material&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.materials = response.data.info;

		                  	main.sortingM = main.materials;

							for(var m in main.materials){

						      	main.materials[m].category = parseInt(main.materials[m].category);
						      	
						      	switch(main.materials[m].category){
							  		case 1:
							  			main.stone.push(main.materials[m]);
							  		break;

							  		case 2:
							  			main.mirror.push(main.materials[m]);
							  		break;

							  		case 3:
							  			main.metal.push(main.materials[m]);
							  		break;

							  		case 4:
							  			main.glass.push(main.materials[m]);
							  		break;

							  		case 5:
							  			main.gemstones.push(main.materials[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.materials = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
	}
});
