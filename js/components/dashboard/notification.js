const NotificationComponent = Vue.component("l-notification",{
  template: `<b-container fluid class="cs-container">
                <b-row>

                  <b-col sm=12 v-for="noti in notifications" v-if=notifications.length>
                      <div class="card" >
                            <p class='font-boldy padding-left'>{{noti.title}}</p>
                            <div class='card-body' v-html="noti.content"></div>
                        </div>
                    </b-col>

                  <b-col sm=12 v-else>
                    <p>Theres no notifications</p>
                  </b-col>
                </b-row>
              </b-container>`,
  data:function(){
    return {
      notifications:[]
    }
  },
  created(){
     let main = this;
     axios.get('notifications?su=em').then(function(response){
        console.log(response)
        main.notifications = response.data.info.query;
      }).catch(function(error){
        console.log(error);
      });
  },
  readed(){

  },
  methods: {
    setNoti(c){
      return c;
    }
  }
      //notifications?su=em

});