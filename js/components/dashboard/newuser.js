const newUserComponent = Vue.component("new-user",{
	props: {
		contact:{
			default: true
		}
	},
	template: 
	`	
	<div>
		<b-row>
			<b-col>
				<b-form-checkbox v-on:input ="toggleType">
			      Registering new <b>{{ (form.role ? "admin" : "client") }}</b>
			    </b-form-checkbox>
			</b-col>
		</b-row>
		<b-form id="formId" @submit="preventSubmit">

			<b-form-group>

				<b-row v-if="errors.length">
					<b-col sm=12 v-for="error in errors">
						<b-alert variant="danger" show>{{ error.message }}</b-alert>
					</b-col>
				</b-row>

				<b-row>
					<b-col sm=12>
						<b-input class="mb-2 mr-sm-2 mb-sm-0" name="signupName" v-model="form.fname" placeholder="FIRST NAME" autocomplete="off" />
					</b-col>

					<b-col sm=12>
						<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
							<b-input v-model="form.sname" placeholder="LAST NAME" autocomplete="off" />
						</b-input-group>
					</b-col>

					<b-col sm=12>
						<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
							<b-input v-model="form.email" placeholder="EMAIL" autocomplete=email type=email />
						</b-input-group>
					</b-col>
					
					<div class="full-width" v-if="!form.role">
						<b-col sm=12>
							<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
								<b-input v-model="form.phone" placeholder="PHONE NUMBER" autocomplete="off" />
							</b-input-group>
						</b-col>

						<b-col sm=12>
							<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
								<b-input v-model="form.address" placeholder="ADDRESS" autocomplete="off" />
							</b-input-group>
						</b-col>

						<b-col sm=12>
							<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
								<b-input v-model="form.city" placeholder="CITY" autocomplete=off />
							</b-input-group>
						</b-col>

						<b-col sm=12>
							<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
								<b-form-select v-model="form.stateS">
							      <option v-for='state in states' :value='state.abbreviation'>
									{{ state.name }}
							      </option>
							    </b-form-select>
							</b-input-group>
						</b-col>

						<b-col sm=12>
							<b-input-group class="mb-2 mr-sm-2 mb-sm-0">
								<b-input v-model="form.zp" placeholder="ZIPCODE" type=number autocomplete="off" />
							</b-input-group>
						</b-col>
					</div>
					
					<b-row class="full-width" style="padding-left:15px;">
						<b-col sm=10 md=11>
							<b-col sm=12>
								<b-input-group>
									<b-input :type="pass.type" autocomplete=new-password v-model="form.fpass" placeholder="PASSWORD" />
								</b-input-group>
							</b-col>

							<b-col sm=12>
								<b-input-group>
									<b-input :type="pass.type" autocomplete=new-password v-model="form.spass" placeholder="COMFIRM PASSWORD" />
								</b-input-group>
							</b-col>
						</b-col>

						<b-col  sm=2 md=1>
							<b-button type="button" class="full-height" block @click="togglePass"><i class="far fa-eye"></i></b-button>
						</b-col>
					</b-row>
									
				</b-row>
				
				<b-row>
					<b-col sm=4>
						<b-button type="submit" block class="golden-border" variant="none">Save</b-button>
					</b-col>
				</b-row>

				<b-row class="text-center padding" v-if=loading>
					<b-col>
						<l-spinner color="luna-text-gold"></l-spinner>
					</b-col>
				</b-row>
			</b-form-group>
		</b-form>
	</div>
	  `,
	data() {
		return {
			formId: null,
			states: [{name: "STATE", abbreviation: null}],
			errors: [],
			loading: false,
			flag: null,
			pass: {
				type: "password",
				seen: false
			},
			form: {
				fname: null,
				sname: null,
				email: null,
				phone: null,
				address: null,
				city: null,
				stateS: null,
				zp: null,
				fpass: null,
				spass: null,
				role: true,
				fmessageC: "Form request"
			}
		}
	},
	created(){

		console.log(this.Toasted);

		let main = this;
		axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8303e1d831e89cb8af24a11f2fa77353c317e408/states_titlecase.json').then(function(response){
	      let lalas = response.data;
	      console.log(lalas)
	      console.log(main.states);
	      for(var key in response.data){
	      	main.states.push(response.data[key]);
	      }
	      console.log(main.states);
	    }).catch(function(error){
	      console.log(error);
	    });

	   console.log(this.$root._data);

	   
	},
	update(){

	},
	methods: {
		toggleType(e){
			this.form.role = !this.form.role;
			this.flag = this.form.role;
		},
		togglePass(e){
			this.pass.seen = !this.pass.seen

			this.pass.type = (this.pass.seen ? "text" : "password");
		},
		preventSubmit(e){
			e.preventDefault();

			let main = this;
			main.loading = true;
			console.log("clic")

			var form = document.getElementById(this.formId);
			var signupForm = new FormData(form);

			var vd = new VueDiana();

			console.log(form)
       		console.log(signupForm.get("signupLastname"));

			signupForm.append("request","newUser");
			signupForm.append("method","post");


			console.log(this.form);

			this.form.role = (this.form.role ? 1 : 2);

			for(var input in this.form){
				signupForm.append(input,this.form[input]);
			}

       		axios({
			    method: 'post',
			    url: 'user',
			    data: signupForm,
			    config: { headers: {'Content-Type': 'multipart/form-data' }}
		    }).then(function (response) {
		        //handle success
		        console.log(response);
		        console.log(response.data);

		        let call = response.data.call;

		        main.errors = [];

		       	switch(call){

		       		case true:
		       			vd.toastSuccess();
		       		break;

		       		case false:
			       		switch(response.data.errors){

			       			case "NotPassMatch":
			       				vd.toastError("Error","Your passwords doesn't match");
			       				main.errors.push({message:"Passwords doesn't match"});
			       			break; 

			       			case "EmailExist":
			       				main.errors.push({message:"That email is already in use"});
			       			break; 

			       			case "NotEnoughForm":
			       				main.errors.push({message:"Must fill all fields in form"});
			       			break;

			       			default:
			       				vd.toastError();
			       			break;
			       		}
		       		break;
		       		
		       		default:
		       			vd.toastError();
		       		break;
		       	}
		    }).catch(function (response) {
		        //handle error
		        console.log(response);
		    }).then(function () {
				main.loading = false;
			}); 

			main.form.role = main.flag; 
		}


	}
});
