const QuotePatternComponent = Vue.component("quote-pattern",{
	template: 
		`<div style="padding-top: 20px;">
			<b-container fluid>
				<b-row>
					<b-col cols=12 md=8>
						<h5 class="component-title">{{ $root.$data.componentTitle }}</h5>
					</b-col>
				</b-row>
			</b-container>

			<div v-if="!role">
				<b-container fluid>
					<!-- Guest form -->
					<span>Step 1</span>
					<b-row>
						<b-col><h5 class="gold-flat-text">Submit your pattern design</h5></b-col>
					</b-row>
					<image-component @file-ref="setFileRef"></image-component>
					<b-container fluid>
						<b-row>
							<b-col>
								<p class="no-margin-bottom padding-top">Project ZIP code: </p>
								<b-form-group
									description="Zip code where the project should be send"
								>
									<b-form-input
										placeholder="Ex. 22204"
										v-model="item.zp"
										:state="zpState"
										type="number"
									></b-form-input>
								</b-form-group>
							</b-col>
						</b-row>
					</b-container>

					<br/>
					<span>Step 2</span>
					<quote-form @quote-form="setForm" ref="quote-form-component"></quote-form>
				</b-container>

				<b-container fluid>
					<b-row>
						<b-col>
							<b-button @click="setRequest" class="btn btn-bordered">SUBMIT</b-button>
						</b-col>
					</b-row>
				</b-container>
			</div>

			<div v-else>
				<!-- Company form -->
				<b-row class="breadcumb-row">
					<div class="vertical-gold"></div>
					<b-col v-for="(step,index) in steps" class="text-center breadcumb-steps" :class="'breadcumb-step-'+(index+1)">
						<div class="text-step" :class="{'active': (step.active ? true : false)}">{{ (index+1) }}</div>
					</b-col>
				</b-row>
				<div v-if="step == 1">
					<b-container fluid>
						<b-row>
							<b-col><h5 class="gold-flat-text">Submit your purchase order</h5></b-col>
						</b-row>

						<b-row>
							<b-col cols=8>
								<b-input-group>
									<b-form-file
										v-model="poFile"
										placeholder="Choose the purchase order file or drop it here..."
										drop-placeholder="Drop file here..."
									></b-form-file>
								</b-input-group>
							</b-col>
						</b-row>
					</b-container>
				</div>

				<div v-if="step == 2">
					<quote-form  @quote-form="setForm" ref="quote-form-component"></quote-form>
				</div>
				
				<div v-if="step == 3">
					<b-row>
						<b-col><h5 class="gold-flat-text">Upolad image for reference</h5></b-col>
					</b-row>
					<b-row><b-col cols=6><image-component @file-ref="setFileRef"></image-component></b-col></b-row>
				</div>
				
				<b-container fluid style="padding-top: 5px;">
					<b-row>
						<b-col cols=2 v-if="step != 1">
							<b-button class="btn btn-bordered"  @click="prev">PREV</b-button>
						</b-col>
						<b-col cols=2>
							<b-button class="btn btn-bordered" v-if="step != 3" @click="next">NEXT</b-button>
							<b-button class="btn btn-bordered" v-if="step == 3" @click="setOrder">SUBMIT</b-button>
						</b-col>
					</b-row>
				</b-container>
			</div>

			<div>
				<b-container fluid>
				</b-container>
			</div>
		</div>`,
	data() {
		return {
			role: false, //False is guest and true is company
			step: 1,


			poFile: null,

			quoteForm: {},

			item: {
				pattern: {
					preview: null,
				}
			},
			zpState: true,

			steps: [
				{
					title: "Step 1",
					active: true,
				},
				{
					title: "Step 2",
					active: false,
				},
				{
					title: "Step 3",
					active: false,
				}
			],

			vd: new VueDiana(),
			
		}
	},
	created(){
		this.$root.$data.componentTitle = "Quote your own pattern";
		this.role = (parseInt(this.$root.$data.session.role) == 2 ? true : false);
		console.log("Quote pattern",this.$root.$data.session.role)
	},
	mounted(){
		console.log();	
	},
	update(){

	},
	methods: {
		activateSteps(a){
			for(var key in this.steps){
				this.steps[key].active = false;
			}

			this.steps[(a-1)].active = true;
			
		},
		prev(){
			if(this.step > 1){
				this.step--;
				this.activateSteps(this.step);
			}
		},
		next(){
			if(this.step < 3){
				this.step++;
				this.activateSteps(this.step);
			}
		},
		setOrder(){
			NProgress.start();	

			var main = this;

			var submitForm = new FormData();

			console.log(this.item);

			this.item.client = this.$root.$data.session.id;

			this.$refs["quote-form-component"].setQuoteForm();

			this.item.cutType = this.cutType;
			this.entryPoint = false;

			if(this.entryPoint){

				var ImageURL = this.item.pattern.preview;
				var block = ImageURL.split(";");

				var contentType = block[0].split(":")[1];

				var realData = block[1].split(",")[1];

				var blob = main.vd.b64toBlob(realData, contentType);
				
				submitForm.append("original", this.item.yourimage.original);
				submitForm.append("canvas", this.item.yourimage.canvas);
				submitForm.append("preview", blob);
				//Im
				submitForm.append("imgType",true);
			}else{
				submitForm.append("imgType",false);
				submitForm.append("preview", this.item.pattern.preview);
			}
			
			if(this.item.zp == null || this.item.zp == "" || this.item.zp == "undefined"){
				this.zpState = false;
				this.vd.toastNotEnough();
				NProgress.done();
				return false;
			}
			
			submitForm.append("custom",true);
			submitForm.append("comment",this.comment);
		    submitForm.append("request","addCustom");
		    submitForm.append("method","post");

		    //Here apennds stringified all content
		    submitForm.append("form",JSON.stringify(this.item));

			axios.post('orders',submitForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errors = [];

		             switch(call){

		               case true:
						 main.vd.toastConfirming(title="",body="Your order information has been submitted, you will receive an e-mail briefly with your nearest dealership information",okText="UNDERSTOOD");
						 main.$root.updateRoute("/dashboard");
		               break;

		               case false:
		                 switch(response.data.errors){
						   case "NotEnoughMaterials":
								main.vd.toastError("","You need at least one material to ser the request");
						   break;

						   case "NotPreview":
								main.vd.toastError("","We need a reference image of your pattern");
						   break;

		                   case "EmailExist":
								main.vd.toastError();
		                   break; 
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		    }).catch(function (response) {
				//handle error
				console.log(response);
		    }).then(function () {
				NProgress.done();
			});
		},
		setRequest(){
			NProgress.start();	

			var main = this;

			var submitForm = new FormData();

			console.log(this.item);

			this.item.client = this.$root.$data.session.id;

			this.$refs["quote-form-component"].setQuoteForm();

			this.item.cutType = this.cutType;
			this.entryPoint = false;

			if(this.entryPoint){

				var ImageURL = this.item.pattern.preview;
				var block = ImageURL.split(";");

				var contentType = block[0].split(":")[1];

				var realData = block[1].split(",")[1];

				var blob = main.vd.b64toBlob(realData, contentType);
				
				submitForm.append("original", this.item.yourimage.original);
				submitForm.append("canvas", this.item.yourimage.canvas);
				submitForm.append("preview", blob);
				//Im
				submitForm.append("imgType",true);
			}else{
				submitForm.append("imgType",false);
				submitForm.append("preview", this.item.pattern.preview);
			}
			
			if(this.item.zp == null || this.item.zp == "" || this.item.zp == "undefined"){
				this.zpState = false;
				this.vd.toastNotEnough();
				NProgress.done();
				return false;
			}
			
			submitForm.append("zp",this.item.zp);
			submitForm.append("custom",true);
			submitForm.append("comment",this.comment);
		    submitForm.append("request","add");
		    submitForm.append("method","post");

		    //Here apennds stringified all content
		    submitForm.append("form",JSON.stringify(this.item));

			axios.post('my-request',submitForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errors = [];

		             switch(call){

		               case true:
						 main.vd.toastConfirming(title="",body="Your order information has been submitted, you will receive an e-mail briefly with your nearest dealership information",okText="UNDERSTOOD");
						 main.$root.updateRoute("/dashboard");
		               break;

		               case false:
		                 switch(response.data.errors){
						   case "NotEnoughMaterials":
								main.vd.toastError("","You need at least one material to ser the request");
						   break;

						   case "NotPreview":
								main.vd.toastError("","We need a reference image of your pattern");
						   break;

		                   case "EmailExist":
								main.vd.toastError();
		                   break; 
		                 }
		               break;
		               
		               default:
		                 main.vd.toastError();
		               break;
		             }
		    }).catch(function (response) {
				//handle error
				console.log(response);
		    }).then(function () {
				NProgress.done();
			});
		},
		setForm(formObj){
			this.item.details = formObj;
			console.log(formObj);
		},
		setFileRef(fileObj){
			this.item.pattern.preview = fileObj;
			console.log(fileObj);
		},
		materialHandler(event){
			console.log(event)
		}
	}
});