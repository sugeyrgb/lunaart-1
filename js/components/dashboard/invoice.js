const InvoiceComponent = { 
  name: "invoice",
  template: 
	`<div class="cs-container">
		<b-container fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-form-input v-model="search" @input="sortInvoice(search)" list='orders' placeholder="ORDER NUMBER" autocomplete="off" />
				</b-col>
			</b-row>
			
			<b-row v-if="invErrors.length" class='padding'>
              <b-col sm=12 v-for="error in invErrors">
                <b-alert variant="danger" show>{{ error.message }}</b-alert>
              </b-col>
            </b-row>
			
			<div v-if="!sortInvoices.length">

				<b-row v-if="!loading">
					<b-col class="table-responsive padding">
			            <table class="table table-hover text-center with-grid" style="width: 100%!important">
			              <thead>
			                <tr>
			                  <th v-for="f in infields" scope="col">{{ f }}</th>
			                </tr>
			              </thead>
			              <tbody>
			                <tr v-for="inv in invoices">
			                  <th scope="row">#{{ inv.number }}</th>
			               	  <td>{{ inv.fdated }}</td>
			               	  <td class="text-center"><b-button block v-on:click.stop="viewInvoice(inv.pdf)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
			               	  <td>{{ (inv.description != "" && inv.description != null ? inv.description : 'Not description') }}</td>
			                </tr>
			              </tbody>
			            </table>
			        </b-col>
				</b-row>

				<b-row v-else>

				</b-row>
			</div>

			<div v-else>
				<b-row>
					<b-col class="table-responsive padding">
			            <table class="table table-hover text-center with-grid" style="width: 100%!important">
			              <thead>
			                <tr>
			                  <th v-for="f in infields" scope="col">{{ f }}</th>
			                </tr>
			              </thead>
			              <tbody>
			                <tr v-for="inv in sortInvoices">
			                  <th scope="row">#{{ inv.number }}</th>
			               	  <td>{{ inv.fdated }}</td>
			               	  <td class="text-center"><b-button block v-on:click.stop="viewInvoice(inv.pdf)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>
			               	  <td>{{ (inv.description != "" && inv.description != null ? inv.description : 'Not description') }}</td>
			                </tr>
			              </tbody>
			            </table>
			        </b-col>
				</b-row>
			</div>
		</b-container>	
	</div>`,

  data(){
  	return {
  		loading: false,
  		search: "",

  		infields: ["NUMBER", "DATE", "PDF","DESCRIPTION"],

  		invoices: [],

  		invsU: {
		},
		infoU: [],

		orders: [],
		invErrors: [],

		sortInvoices: [],

  		vd: new VueDiana(),
  	}
  },
  created(){
  	this.$root.$data.componentTitle = "Invoices";

  	this.searchInvoices();

  	let main = this;
	var timeQuery = setInterval(function(){
		main.getAllOrders();
	},60000);
  },
  methods: {
  	sortInvoice(e){
		let main = this;
		this.sortInvoices = this.invoices.filter(function(index) {
			
			if(index.number.toLowerCase().match(main.search.toLowerCase()) || index.fdated.toLowerCase().match(main.search.toLowerCase())){
				return index;
			}

			console.log(index.number.toLowerCase())
			console.log(main.search.toLowerCase())
			
		});
	},
  	viewInvoice(url){
  		window.open(url);
  	},
  	searchOrders(number){
  		var main = this;
  		axios({
	        method: 'get',
	        url: 'orders?g=as&number='+number,
	    }).then(function (response) {
	          //handle success
	          console.log(response);
	          console.log(response.data);

	          let call = response.data.call;

	           switch(call){

	             case true:
	             	main.orders = response.data.info;

	             break;

	             case false:
	               switch(response.data.errors){

	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	             break;
	             
	             default:
	               main.vd.toastError();
	             break;
	           }
	    }).catch(function (response) {
	          //handle error
	          console.log(response);
	    }).then(function () {
	        main.loading = false;
	    }); 
  	},
  	searchInvoices(par='m',args=""){
  		var main = this;
  			main.loading = true;

  		axios({
	        method: 'get',
	        url: 'invoice?r=a' ,
	    }).then(function (response) {
	          //handle success
	          console.log(response);
	          console.log(response.data);

	          let call = response.data.call;

	           switch(call){

	             case true:
	             	main.invoices = response.data.info;

	             break;

	             case false:
	               switch(response.data.errors){

	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	             break;
	             
	             default:
	               main.vd.toastError();
	             break;
	           }
	    }).catch(function (response) {
	          //handle error
	          console.log(response);
	    }).then(function () {
	        main.loading = false;
	    });  
  	},
  	deleteInv(id){
       main = this;
       var updateForm =  new FormData();
       var vd = new VueDiana();

       if(confirm("Are you sure that you want delete invoice?")){
         updateForm.append("id",id);
         updateForm.append("request","delete");
         updateForm.append("method","post");

        axios.post('invoice',updateForm,{
          headers: {
             'content-type': 'multipart/form-data'
          }
        }).then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data);

              let call = response.data.call;

              main.invErrors = [];

               switch(call){

                 case true:
                   main.vd.toastSuccess();
                   main.searchInvoices();
                 break;

                 case false:
                   switch(response.data.errors){

                     default:
                       main.vd.toastError();
                     break;
                   }
                 break;
                 
                 default:
                   main.vd.toastError();
                 break;
               }
          }).catch(function (response) {
              //handle error
              console.log(response);
          }).then(function () {
          main.loading = false;
        });
       }
    },
    uploadInv(e){
      e.preventDefault();

      let main = this;
      main.loading = true;
      console.log("clic")

      var form = document.getElementById(this.formId);
      var updateForm = new FormData(form);

      var vd = new VueDiana();

      console.log(form);

      for(var input in this.invsU){
        updateForm.append(input,this.invsU[input]);
      }

      updateForm.append("id",this.infoU["id"]);
      updateForm.append("request","add");
      updateForm.append("method","post");

      console.log(this.invsU);

      axios.post('invoice',updateForm,{
        headers: {
           'content-type': 'multipart/form-data'
        }
      }).then(function (response) {
            //handle success
            console.log(response);
            console.log(response.data);

            let call = response.data.call;

            main.invErrors = [];

             switch(call){

               case true:
                 main.vd.toastSuccess();
                 main.loadInv();
               break;

               case false:
                 switch(response.data.errors){

                   case "NotRealClient":
                     main.invErrors.push({message:"That number order doesn't exist"});
                   break;

                   case "NotEnoughForm":
                     main.invErrors.push({message:"Is needed a file and a number"});
                   break;

                   case "NumberAlready":
                     main.invErrors.push({message:"That number already belongs to another invoice"});
                   break;

                   default:
                     main.vd.toastError();
                   break;
                 }
               break;
               
               default:
                 main.vd.toastError();
               break;
             }
        }).catch(function (response) {
            //handle error
            console.log(response);
        }).then(function () {
        main.loading = false;
      });
    },
  }
};