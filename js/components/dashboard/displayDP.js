const DisplayDPComponent = Vue.component("displaydp",{
	//Side default is null, 1 equals to landing page and 2 is admin dashboard, 3 is client dashboard.
	props:{
		side: null
	},
	template: 
	`<div>
		
		<b-container fluid v-if=loading>
			<b-row v-if="designpatterns.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
				
			<b-container class="padding">
				
				<b-row>

					<b-input-group class="mt-3">
				       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH PATTERN" autocomplete=off />
				    </b-input-group>

				</b-row>


			</b-container>

			<b-container v-if="sorting">
				
				<b-row>

					<b-col sm=3  v-for="m in sortingM">
						<b-card :title="m.title" :img-src="m.img" img-alt="Image" class="designpattern-card"  img-top>
						</b-card>
					</b-col>

				</b-row>


			</b-container>

			<b-container fluid v-else>

				<b-tabs class="nav-justified" content-class="mt-3" v-if="designpatterns.length">
					<b-tab title="Coastline" active>
						
						<b-container>
							<b-row>
								<b-col sm=3  v-for="m in coastline" @click="selectThis(m)" >
									<b-card :title="m.title" :img-src="m.img" img-alt="Image" class="designpattern-card"  img-top>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Progressive">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3  v-for="m in progresive" @click="selectThis(m)" >
									<b-card :title="m.title" :img-src="m.img" img-alt="Image" class="designpattern-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Timeless">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3  v-for="m in timeless" @click="selectThis(m)" >
									<b-card :title="m.title" :img-src="m.img" img-alt="Image" class="designpattern-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Traditional">
						
						<b-container>	
							<b-row>
							
								<b-col sm=3  v-for="m in traditional" @click="selectThis(m)" >
									<b-card :title="m.title" :img-src="m.img" img-alt="Image" class="designpattern-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Geometric">

						<b-container>	
							<b-row>
							
								<b-col sm=3  v-for="m in geometric" @click="selectThis(m)" >
									<b-card :title="m.title" :img-src="m.img" img-alt="Image" class="designpattern-card" img-top>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>
						
					</b-tab>
				</b-tabs>

				<b-row v-else>
					<b-col class="text-center">
						<p>There's no designpatterns in your library</p>
					</b-col>
				</b-row>
			</b-container>
		 </b-container>
	</div>
	  `,
	data() {
		return {
			designpatterns: [],
			coastline: [],
			progresive: [],
			timeless: [],
			traditional: [],
			geometric: [],
			loading: false,
			sorting: "",
			sortingM: []
		}
	},
	mounted(){
		console.log(this.designpatterns);
		console.log(this.sortingM);
		

	},
	created(){
		this.$root.$data.mainPattern = [];
		var main = this;
		main.loaddesignpattern();

		console.log("Hi")

		window.addEventListener("reloaddesignpatterns",function(){
			main.loaddesignpattern();
			main.$forceUpdate();
		});
	},
	updated(){
		console.log(this.designpatterns);
		console.log(this.sortingM);
	},
	methods: {
		selectThis(pt){
			console.log(pt);
			this.$root.$data.mainPattern = pt;
			window.dispatchEvent(new Event('closeDisplayPattern'));
		},
		searchMat(){
			var main = this;
			
			this.sortingM = this.designpatterns.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});

			console.warn(this.sortingM)
		},
		loaddesignpattern(){
			//asset?r=designpattern&a=da
			 var main = this;

			 main.loading = true;

			 main.designpatterns = [];

			 main.coastline = [];
			 main.progresive = [];
			 main.timeless = [];
			 main.traditional = [];
			 main.geometric = [];

			  axios({
		        method: 'get',
		        url: 'asset?r=dp&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.designpatterns = response.data.info;

		                  	main.sortingM = main.designpatterns;

							for(var m in main.designpatterns){
						      	console.log(m);
						      	console.log(main.designpatterns[m].category)

						      	console.log(main.designpatterns[m]);

						      	main.designpatterns[m].category = parseInt(main.designpatterns[m].category);
						      	
						      	switch(main.designpatterns[m].category){
							  		case 1:
							  			main.coastline.push(main.designpatterns[m]);
							  		break;

							  		case 2:
							  			main.progresive.push(main.designpatterns[m]);
							  		break;

							  		case 3:
							  			main.timeless.push(main.designpatterns[m]);
							  		break;

							  		case 4:
							  			main.traditional.push(main.designpatterns[m]);
							  		break;

							  		case 5:
							  			main.geometric.push(main.designpatterns[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.designpatterns = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
	}
});

