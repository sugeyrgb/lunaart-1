const AinvoiceComponent = {
  name: "a-invoice",
  template: 
	`<div class="cs-container">
		<b-container fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>

				<b-col sm=12 md=6>
					<b-form-input v-model="search" @input="searchInvoices(search)" list='orders' placeholder="ORDER NUMBER" autocomplete="off" />
				</b-col>
			</b-row>
			
			<b-row v-if="invErrors.length" class='padding'>
              <b-col sm=12 v-for="error in invErrors">
                <b-alert variant="danger" show>{{ error.message }}</b-alert>
              </b-col>
            </b-row>

			<b-row>
				
		      <b-col sm=12 class="padding">
		        <b-collapse id="collapse2">
		          <b-card>
		            <b-form-group>
		               <div class="mt-3">Selected file: {{ invsU.file ? invsU.file.name : '' }}</div>
		              <b-form-file
		                v-model="invsU.file"
		                placeholder="Chose invoice file"
		                drop-placeholder="Drop file here..."
		                accept="application/pdf"
		              />
		            </b-form-group>

		            <b-form-group>
		              <b-form-input v-model="invsU.number" placeholder="ORDER" list='orders' @input="searchOrders(invsU.number)" />

			            <datalist id="orders">
							<option v-for='o in orders' :value="o.number">{{ o.name+' '+o.lastname+' '+(o.business != ''? '('+o.business+')' : '')}}</option>
						</datalist>
		            </b-form-group>

		            <b-form-group>
		              <b-form-textarea
		                v-model="invsU.desc"
		                placeholder="Invoice description..."
		                rows="3"
		                max-rows="6"

		              />
		            </b-form-group>
					
				

		            <b-col class="text-right">
		              <button @click="uploadInv" class="btn-blue">Upload</button>
		            </b-col>
		          </b-card>
		        </b-collapse>
		        
		      </b-col>
		    </b-row>

			<b-row v-if="!loading">
				<b-col class="table-responsive padding">
		            <table class="table table-hover text-center with-grid" style="width: 100%!important">
		              <thead>
		                <tr>
		                  <th v-for="f in infields" scope="col">{{ f }}</th>
		                  <th><b-button v-b-toggle.collapse2 class="m-1">Add Invoice</b-button></th>
		                </tr>
		              </thead>
		              <tbody>
		                <tr v-for="inv in invoices">
		                  <th scope="row">#{{ inv.number }}</th>
		               	  <td>{{ inv.fdated }}</td>
		               	  <td class="text-center"><b-button block v-on:click.stop="viewInvoice(inv.pdf)"><i class="fas fa-file-pdf fa-1x"></i></b-button> </td>

		               	  <td>{{ inv.name+' '+inv.lastname+' '+(inv.business != '' ? '('+inv.business+')' : '' ) }}</td>

		               	  <td>{{ (inv.description != "" && inv.description != null ? inv.description : 'Not description') }}</td>

		               	  <td class="text-center"><button type="button" @click="deleteInv(inv.id)" class="btn-flat"><i class="fas fa-times"></i></button></td>
		                </tr>
		              </tbody>
		            </table>
		        </b-col>
			</b-row>

			<b-row v-else>

			</b-row>
		</b-container>	
	</div>`,

  data(){
  	return {
  		loading: false,
  		search: "",

  		infields: ["NUMBER", "DATE", "PDF", "CLIENT", "DESCRIPTION"],

  		invoices: [],

  		invsU: {
		},
		infoU: [],

		orders: [],
		invErrors: [],

  		vd: new VueDiana(),
  	}
  },
  created(){
  	this.$root.$data.componentTitle = "Admin invoices";

  	this.searchInvoices();
  },
  methods: {
  	viewInvoice(url){
  		window.open(url);
  	},
  	searchOrders(number){
  		var main = this;
  		axios({
	        method: 'get',
	        url: 'orders?g=as&number='+number,
	    }).then(function (response) {
	          //handle success
	          console.log(response);
	          console.log(response.data);

	          let call = response.data.call;

	           switch(call){

	             case true:
	             	main.orders = response.data.info;

	             break;

	             case false:
	               switch(response.data.errors){

	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	             break;
	             
	             default:
	               main.vd.toastError();
	             break;
	           }
	    }).catch(function (response) {
	          //handle error
	          console.log(response);
	    }).then(function () {
	        main.loading = false;
	    }); 
  	},
  	searchInvoices(par='m',args=""){
  		var main = this;
  			main.loading = true;

  		axios({
	        method: 'get',
	        url: 'invoice?r='+par+'&args='+args,
	    }).then(function (response) {
	          //handle success
	          console.log(response);
	          console.log(response.data);

	          let call = response.data.call;

	           switch(call){

	             case true:
	             	main.invoices = response.data.info;

	             break;

	             case false:
	               switch(response.data.errors){

	                 default:
	                   main.vd.toastError();
	                 break;
	               }
	             break;
	             
	             default:
	               main.vd.toastError();
	             break;
	           }
	    }).catch(function (response) {
	          //handle error
	          console.log(response);
	    }).then(function () {
	        main.loading = false;
	    });  
  	},
  	deleteInv(id){
       main = this;
       var updateForm =  new FormData();
       var vd = new VueDiana();

       if(confirm("Are you sure that you want delete invoice?")){
         updateForm.append("id",id);
         updateForm.append("request","delete");
         updateForm.append("method","post");

        axios.post('invoice',updateForm,{
          headers: {
             'content-type': 'multipart/form-data'
          }
        }).then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data);

              let call = response.data.call;

              main.invErrors = [];

               switch(call){

                 case true:
                   main.vd.toastSuccess();
                   main.searchInvoices();
                 break;

                 case false:
                   switch(response.data.errors){

                     default:
                       main.vd.toastError();
                     break;
                   }
                 break;
                 
                 default:
                   main.vd.toastError();
                 break;
               }
          }).catch(function (response) {
              //handle error
              console.log(response);
          }).then(function () {
          main.loading = false;
        });
       }
    },
    uploadInv(e){
      e.preventDefault();

      let main = this;
      main.loading = true;
      console.log("clic")

      var form = document.getElementById(this.formId);
      var updateForm = new FormData(form);

      var vd = new VueDiana();

      console.log(form);

      for(var input in this.invsU){
        updateForm.append(input,this.invsU[input]);
      }

      updateForm.append("id",this.infoU["id"]);
      updateForm.append("request","add");
      updateForm.append("method","post");

      console.log(this.invsU);

      axios.post('invoice',updateForm,{
        headers: {
           'content-type': 'multipart/form-data'
        }
      }).then(function (response) {
            //handle success
            console.log(response);
            console.log(response.data);

            let call = response.data.call;

            main.invErrors = [];

             switch(call){

               case true:
                 main.vd.toastSuccess();
                 main.loadInv();
               break;

               case false:
                 switch(response.data.errors){

                   case "NotRealClient":
                     main.invErrors.push({message:"That number order doesn't exist"});
                   break;

                   case "NotEnoughForm":
                     main.invErrors.push({message:"Is needed a file and a number"});
                   break;

                   case "NumberAlready":
                     main.invErrors.push({message:"That number already belongs to another invoice"});
                   break;

                   default:
                     main.vd.toastError();
                   break;
                 }
               break;
               
               default:
                 main.vd.toastError();
               break;
             }
        }).catch(function (response) {
            //handle error
            console.log(response);
        }).then(function () {
        main.loading = false;
      });
    },
  }
};