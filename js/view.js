const myServer = "";
var reload = new CustomEvent("ReloadEvent",{});
var vd = new VueDiana();

function IsValidJSONString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

var viewComponent = Vue.component('route-view',{
  template: `
    <div>
      <b-container v-if="order.length == 0">
        <b-row>
          <b-col class="text-center">
            <b-img src="media/img/logo_d.png" fluid style="max-width: 182px;"></b-img>
          </b-col>
        </b-row>

        <b-row>
          <b-col>{{ title }}</b-col>
        </b-row>
      </b-container>

      <b-container v-else>
        <b-row>
          <b-col class="text-center">
            <b-img src="media/img/logo_d.png" fluid style="max-width: 182px;"></b-img>
          </b-col>
        </b-row>
        
        <b-row>
          <b-col>
            <p v-if="order.clientInfo"><span class="order-prepend">Company / Client: </span> <span class="order-underline">{{ order.clientInfo.name+' '+order.clientInfo.lastname+' '+(order.clientInfo.business ? '('+order.clientInfo.business+')' : '') }}</span></p> 
          </b-col>
          <b-col>
            <p v-if="order.admin"><span class="order-prepend">Received by: </span> <span class="order-underline">{{ order.admin.name+' '+order.admin.lastname }}</span></p>
          </b-col>
        </b-row>
        
        <b-row v-if="order.content">
          <b-col>
              <p><span class="order-prepend">P.O: </span> <span class="order-underline">{{ order.content.details.po }}</span></p>
          </b-col>

          <b-col>
              <p><span class="order-prepend">Date: </span> <span class="order-underline">{{ order.fdated }}</span></p>
          </b-col>
        </b-row>
        
        <b-row v-if="order.content">
          <b-col>
             <p><span class="order-prepend">Pattern name: </span> <span class="order-underline">{{ order.content.pattern.title }}</span></p>
          </b-col>
        </b-row>

        <b-row v-if="order.content">
          <b-col>
             <p><span class="order-prepend">Pattern square feet: </span> <span class="order-underline">{{ order.content.details.sf }}</span></p>
          </b-col>

          <b-col>
            <p><span class="order-prepend">Square feet to cover: </span> <span class="order-underline">{{ order.content.details.sfCovered }}</span></p>
          </b-col>

          <b-col>
            <p><span class="order-prepend">Quantity: </span> <span class="order-underline">{{ order.content.details.quantity }}</span></p>
          </b-col>
        </b-row>

        <b-row>
          <b-col>
            <p><span class="order-prepend">Cut type: </span> <span class="order-underline">{{ (order.cutType == null ? cutTypeOptions[0] : cutTypeOptions[order.cutType])  }}</span></p>
          </b-col>
        </b-row>

        <b-row>
          <b-col>
            <p><span class="order-prepend">Extra details: </span></p>
            <p class="order-area">{{ order.clientComment }}</p>
          </b-col>
        </b-row>

        <b-row v-if="order.content">
          <b-col sm=4 v-for="(m,i) in order.content.build" :key='i'>
            <h4>{{ (i+1) }}</h4>
            <p>Material para orden</p>
            <b-img :src="m.img" fluid class='golden-border'></b-img>
            <p>FINISH</p>
            <p v-if="m.finish != null ">{{ (m.finish.value == null ?  finishOpt[0] : finishOpt[m.finish.value])  }}</p>
            <p v-else>{{  finishOpt[0] }}</p>
          </b-col>
        </b-row>
    
      </b-container>
      
    </div>
  `,
  data(){
    return {
       title: "",
       order: [],

       cutTypeOptions: [
        "Not cut type selected",
        "Water Jet",
        "Cut by hand",
        "Mix cut",
        "Nominal dimension",
        "Real dimension",
      ],

      finishOpt: [
        "Not specified",
        "Honed",
        "Polished",
        "Tumbled",
      ],
    }
  },
  beforeCreate(){
    NProgress.start();

    var main = this;

    axios.get('orders?g=o&n='+number).then(function(response){
      console.log(response)
      if(response.data.hasOwnProperty("info")){
        main.order = response.data.info[0];
        if(main.order.client == user.id){
          main.order.clientInfo['name'] = user.name;
          main.order.clientInfo['lastname'] = user.lastname;
          main.order.clientInfo['business'] = user.business;

        }
      }else{
        main.title = "Order not found";
      }
      
    }).catch(function(error){
      console.log(error);
    });
  },
  mounted(){
    NProgress.done();
  },

});

new Vue({
  el: '#app',
  methods: {
    printPage(){
      window.print();
    },
    closeWindow(){
      window.close();
    }
  },
});