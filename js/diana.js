/*!
 * Dianajs (https://luballsoftware.com)
 * Copyright 2014-2018 Materialize
 * MIT License
 */

class Diana{

	jsonHandler(json,onSuccess){
		var aux = true;
		console.log(json);
		try{
			json = $.trim(json);
			json = JSON.parse(json);

			onSuccess(json);

		}catch(e){
			console.log(e);
			console.log(json);
		}

		return aux;
	}

	b64toBlob(b64Data, contentType, sliceSize) {
	        contentType = contentType || '';
	        sliceSize = sliceSize || 512;

	        var byteCharacters = atob(b64Data);
	        var byteArrays = [];

	        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
	            var slice = byteCharacters.slice(offset, offset + sliceSize);

	            var byteNumbers = new Array(slice.length);
	            for (var i = 0; i < slice.length; i++) {
	                byteNumbers[i] = slice.charCodeAt(i);
	            }

	            var byteArray = new Uint8Array(byteNumbers);

	            byteArrays.push(byteArray);
	        }

	      var blob = new Blob(byteArrays, {type: contentType});
	      return blob;
	}

	renderText(el,obj,temp = false){
			
		if(temp){
			el.html(temp);
		}

		var section = el.html();
		Mustache.parse(section);
		var rendered = Mustache.render(section,obj);
		el.html(rendered);
	}

	test(){
		console.log("Loaded");
	}

	allStorage(){
		var archive = {}, 
			keys = Object.keys(localStorage),
			i = keys.length;
		while (i--) {
			archive[keys[i]] = localStorage.getItem(keys[i]);
			archive[""];
		}
		return archive;
	}

}