var server = "http://localhost/";

class webAppUI{

	scrollHandler(lastY = 0){
		let Y = window.scrollY;
		if(lastY > 0 && lastY > Y){
			return true;
		}else{
			return false;
		}
	}

	ValidURL(str) {
	  var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/; // fragment locater
	  if(!pattern.test(str)) {
	    return false;
	  } else {
	    return true;
	  }
	}

	navHandler(caseName,el){
		var el = document.querySelector(el);
		switch(caseName){
			case "top":
				el.classList.remove("coloredNav");
			break;

			case "down":
				el.classList.add("coloredNav");
			break;

			default:
				console.log("")
			break;
		}
	}

	setBanner(el,nav,fix=false){
		if(!fix){
			el = document.querySelector(el);
			nav = document.querySelector(nav);
			let wh = window.innerHeight;
			let navH = -nav.offsetHeight;
				
			if(wh > 1200){
				wh = 1200;
			}
			
			wh++;
			
			el.style.height = (wh+48)+"px";
			el.style.marginTop = navH+"px";
		}else{
			el = document.querySelector(el);
			nav = document.querySelector(nav);
			let wh = window.innerHeight;
			let navH = nav.offsetHeight;
				
			if(wh > 1200){
				wh = 1200;
			}
			
			wh++;
			el.style.marginTop = navH+"px";
		}
		
		
	}

	compareJSON(obj1, obj2) {
		var diff = {};
		
		if(obj1.length <= obj2.length){
			for(var i in obj2) {
				if(!obj1.hasOwnProperty(i) || obj2[i] !== obj1[i]) {
					diff[i] = obj2[i];
				}
			}
		}else{
			for(var i in obj1) {
				if(!obj2.hasOwnProperty(i) || obj1[i] !== obj2[i]) {
					diff[i] = obj1[i];
				}
			}
		}

		if(Object.keys(diff).length === 0){
			return false
		}else{
			return diff;
		}
		
	}

	myCallback(jsonInString){
		try{
			var json = JSON.parse(jsonInString);
				json = json.callback;

			return json;
			
		}catch(e){

			console.log("An error happen");
			console.log(e);
			console.log(jsonInString)
			return false;
		}
	}

	doubleDigit(number) {
		return (number < 10 ? '0' : '') + number;
	}

	textWrapper(str,length){
		if(str.length > length){
			str = str.substring(0,length-3);
			str += "...";
			console.log("La cadena: "+str);
		}
		
		return str;
	}

	invertArrayByNum(trans){
		var tmp_ar = {};
		var c = 0;

		for(var i = trans.length - 1; i >= 0; i--){
			tmp_ar[i] = trans[c];
			c++;
		}

		return tmp_ar;
	}

	copyToClipboard(elementId) {

		// Create a "hidden" input
		var aux = document.createElement("input");

		// Assign it the value of the specified element
		aux.setAttribute("value", document.getElementById(elementId).innerHTML);

		// Append it to the body
		document.body.appendChild(aux);

		// Highlight its content
		aux.select();

		// Copy the highlighted text
		document.execCommand("copy");

		// Remove it from the body
		document.body.removeChild(aux);

	}

	openSection(name){
		/* Remember to add the link hidden*/
		$("#slide-out").append("");
		$("a.mipos-element[href$="+name+"]").trigger("click");
	}

	openAdminSection(name){
		/* Remember to add the link hidden*/
		$("#slide-out").append("");
		$("a.admin-element[href$="+name+"]").trigger("click");
	}

	cleanImage(imageUrl){
		imageUrl = imageUrl.split(" ").join("%20");
		return imageUrl;
	}

	generateString(length = 1){
		var text = "";
		var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for(var i = 0; i < length; i++){
			text += charset.charAt(Math.floor(Math.random()*charset.length));
		}

		return text;
	}

	trimFullName(name,lastname){
		return $.trim(name)+" "+$.trim(lastname);
	}

	activityUserColor(uc,us,ua){

		/*console.log("\n");
		console.log(uc);
		console.log(us);
		console.log(ua);*/

		if((uc != null && uc != 0) || (us != null && us != 0) || (ua != null && ua != 0)){
			return true;
		}else{
			return false;
		}
	}

	moneyFormater(money,currency = "MXN", convert=false){
		currency = $.trim(currency);

		if(money == null){
			money = 0;
		}

		if(convert){
			money = money*usd;
		}

		if(currency == "MXN"){
			return "$"+parseFloat(Math.round(money * 100) / 100).toFixed(2)+" MXN";
		}else{
			return "$"+parseFloat(Math.round(money * 100) / 100).toFixed(2)+" USD";
		}
	}

	setClock(element){
		
		setInterval(function(){
			let clock = new Date();
			clock = clock.toLocaleString('en-US', { hour: 'numeric',minute:'numeric', hour12: true });
			$(element).html(clock);
		},1000);
	}

	getCallback(callback){
		var callbackCodes = [
			/**/
		];
	}

	/* Funcion para cargar un elemento de un archivo externo*/ /*yolfry: Y entiendo */
	getElementFromPage(selector,fileHTML){
		$.get(fileHTML,function(data){
			$(data).prependTo(selector);
			return true;				
		});	
	}

	getElementFromPageConstant(selector,fileHTML,timer=1000){
		setInterval(function(){ 
			$(selector).load(fileHTML); 
		}, timer);
	}

	/* Funcion para mandar el footer al fondo de la pagina siempre. (Sticky footer)*/
	setFooterBot(){
		if($("footer").length && $("main").length){
			$("main,footer").ready(function(){
				var footerHeight = $("footer").height();
			
				$("footer").css({
					bottom: 0,
					position: "absolute"
				});

				$("main").css("margin-bottom",footerHeight);
				return true;
			});
			
		}else{
			console.log("The element footer or main wasn't defined");
			return false;
		}
	}

	pagePreloader(loader){
		$(loader).fadeOut(1500);
		$("html").css("overflow","auto");
	}

	dropdownFade(bind){
		$(document).click(function(event) {/*
			console.log($(event.target));
			if($(event.target) !== $(bind)){
				console.log("No es");
			}else{
				console.log("Si es");
			}*/
		});
	}

	dropdownEditDelete(bind){
		$(window).on("click", function(e){
			if(!$(e.target).is(bind)){
				$(".dropdown_delete_edit").css({display: "none"});
			}else{
				var offset = $(bind).offset();
				var relativeX = e.pageX+5, 
					relativeY = e.pageY+5;

				$(".dropdown_delete_edit").css({
					top: relativeY,
					left: relativeX,
					display: "block"
				});
			}

		});
	}

	dropdownInventory(bind){
		$(window).on("click", function(e){
			if(!$(e.target).is(bind)){
				$(".dropdown_inventory").css({display: "none"});
			}else{
				var offset = $(bind).offset();
				var relativeX = e.pageX+5, 
					relativeY = e.pageY+5;

				$(".dropdown_inventory").css({
					top: relativeY,
					left: relativeX,
					display: "block"
				});

				alert($(bind).parent().data("product"));
			}

		});
	}

	bindCollapsible(link,collapsible){
		var flag = false;

		$(link).click(function(event) {
			if(flag){
				$(collapsible).collapsible("close",0);
				flag = false;
			}else{
				$(collapsible).collapsible("open",0);
				flag = true;
			}
			
		});
	}
}
