var lang = {	
	"index": {
		"form": {
			"message": {
				"registerFail": "No se a podido registrar, intente más tarde",
				"already": "Sí ya tienes cuenta",
				"forgotPass": "¿Olvidaste tu contraseña?",
				"loginFalseUser": "Es necesario un usuario",
				"loginFalsePass": "El campo de la contraseña esta vacío"
			},
			"title": {
				"login": "Iniciar sesión",
				"signUp": "Registrarse"
			},
			"input": {
				"name": "Nombre",
				"lastname": "Apellido",
				"email": "Correo electronico",
				"user": "Usuario",
				"password": "Contraseña"
			},
			"button": {
				"facebook": "Entrar con Facebook",
				"signUp": "Registrarse",
				"login": "Inicar sesión",
				"enter": "Entrar"
			},
		}	
	},
	"dashboard":{
		"sideNav": {
			"notes": "Notas",
			"lists": "Listas",
			"reminders": "Recordatorios",
			"trash": "Basura",
			"themes": "Temas",
			"settings": "Configuración",
			"logout": "Salir"
		},
		"note": {
			"title": "Sin titulo",
			"content": "Escribe aquí...",
			"cancel": "Cancela",
			"save": "Guardad"
		}
	},
	"gMessage": {
		"incompleteForm": "Falta información para enviar el formulario",
		"error": "Algo salió mal, intente de nuevo más tarde",
		"redirecting": "Redireccionando",
		"done": "Completado"
	}
}