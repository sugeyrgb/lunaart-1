<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="media/img/logo_2.png">
<!-- CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<link rel="stylesheet" href="css/luballstyle.css">
<link rel="stylesheet" href="css/materialize-color.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<link rel="stylesheet" href="css/iziToast.min.css">
<link rel="stylesheet" href="css/style.css">

<link rel="stylesheet" href="https://unpkg.com/vue2-animate/dist/vue2-animate.min.css"/>


<!-- Script -->
<script src="js/vue.js" ></script>
<script src="js/vue-router.js"></script>

<script src="js/dater.js" ></script>
<script src="js/main.js" ></script>
<script src="js/VueDiana.js" ></script>

<link type="text/css" rel="stylesheet" href="css/bootstrap.css" />

<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@2.0.0-rc.11/dist/bootstrap-vue.css"/>

<link href="https://fonts.googleapis.com/css?family=El+Messiri:100,200,300,400,500,700,bold,lighter" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,700,bold,lighter" rel="stylesheet">

<!-- Add this after vue.js -->
<script src="//unpkg.com/babel-polyfill@6.26.0/dist/polyfill.min.js"></script>
<script src="//unpkg.com/bootstrap-vue@2.0.0-rc.11/dist/bootstrap-vue.js"></script>

<link href="css/nprogress.css" rel="stylesheet" />
<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>


<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vue-select@2.6.3/dist/vue-select.js"></script>
<script src="js/vuelidate.min.js"></script>
<script src="js/iziToast.min.js" type="text/javascript"></script>

<script src="https://unpkg.com/vuejs-datepicker@1.5.4/dist/vuejs-datepicker.min.js"></script>
<script src="https://unpkg.com/vue-gallery-slideshow@1.3.1/dist/js/vue-gallery-slideshow.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue2-dropzone@3.5.8/dist/vue2Dropzone.min.js"></script>

<!-- Global components -->

<script src="js/components/404.js"></script>
<script src="js/components/spinner.js"></script>

<script src="https://www.paypal.com/sdk/js?client-id=AXGqm4c0-4UBKYdQohq-XRjPbjLbbf7A5u9SW7UD0n02QLo7p_APUVdGmp-mWV1SgiwO7iC8QpnCaU5x"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://unpkg.com/vuelidate@0.7.4/dist/vuelidate.min.js"></script>
<script src="https://unpkg.com/vuelidate@0.7.4/dist/validators.min.js"></script>
<script type="module" src="https://cdn.jsdelivr.net/npm/push-js@1.1.1/dist/Push.js"></script>

<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<link href="//unpkg.com/viewerjs/dist/viewer.css" rel="stylesheet">
<script src="//unpkg.com/viewerjs/dist/viewer.js"></script>
<script src="//unpkg.com/v-viewer/dist/v-viewer.js"></script>

<link rel="stylesheet" href="css/vue-virtual-scroller.css"/>
<script src="https://unpkg.com/vue-mugen-scroll@0.2.5/dist/vue-mugen-scroll.min.js"></script>

<meta name="theme-color" content="#12172D">

<title><?= $title; ?></title>