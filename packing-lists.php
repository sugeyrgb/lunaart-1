<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	/*
	var_dump($_SERVER['REQUEST_METHOD']);
	echo "<br> GET dump: <br>";
	var_dump($_GET);
	echo "<br> POST dump: <br>";
	var_dump($_POST);
	echo "<br>";
	echo "Array POST: ".var_dump(empty($_POST));
	echo "<br>";
	echo "Array Get: ".var_dump(empty($_GET));
	echo "<br>";*/

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();
		
		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
				
				switch ($_GET["g"]) {
					case 'a':
						$lb->power_session_start();
						if($lb->isAdmin()){
							$sql = "SELECT *, LPAD(packing.`number`, 6, '0') as `number`,  DATE_FORMAT(packing.dated, '%m - %d  - %y') as fdated, user.id as uid, packing.id as id FROM packing,user WHERE packing.client = user.id LIMIT 10";

							//$client = $db->queryOneValue("orders",'client','number',$_POST['number']);

							/*

							if(!$client){
								$lb->toClient(false,$callback,"NotRealClient");
								return false;
							}	*/

							$list = $db->query($sql,true);

							if($list["status"]){
								$lb->toClient(true,$list['query']);
							}else{
								$lb->defaultQuery();
							}
						}
					break;
					
					case "o":
						$lb->power_session_start();
						$sql = "SELECT *, LPAD(packing.`number`, 6, '0') as `number`,  DATE_FORMAT(packing.dated, '%m - %d  - %y') as fdated, user.id as uid, packing.id as id FROM packing,user WHERE packing.id = ".$_GET['args']." LIMIT 1";


						$list = $db->query($sql,true);

						if($list["status"]){
							$lb->toClient(true,$list['query']);
						}else{
							$lb->defaultQuery();
						}
					break;

					case "ou":
						$lb->power_session_start();
						$sql = "SELECT *, LPAD(packing.`number`, 6, '0') as `number`,  DATE_FORMAT(packing.dated, '%m - %d  - %y') as fdated FROM packing WHERE packing.client = ".$_GET['id'];

						$list = $db->query($sql,true,true);

						if($list["status"]){
							$lb->toClient(true,$list['query']);
						}else{
							$lb->defaultQuery();
						}
					break;

					case "m":
						$lb->power_session_start();
						$sql = "SELECT *, LPAD(packing.`number`, 6, '0') as `number`,  DATE_FORMAT(packing.dated, '%m - %d  - %y') as fdated FROM packing WHERE packing.client = ".$_SESSION['user']['id'];


						$list = $db->query($sql,true);

						if($list["status"]){
							$lb->toClient(true,$list['query']);
						}else{
							$lb->defaultQuery();
						}
					break;

					default:
						$sql = "SELECT *, LPAD(packing.`number`, 6, '0') as `number`,  DATE_FORMAT(packing.dated, '%m - %d  - %y') as fdated, user.id as uid, packing.id as id FROM packing,user WHERE packing.client = user.id AND ((packing.`number` LIKE '%".$_GET['g']."%') OR (CONCAT(user.name,' ',user.lastname,' ',user.business) LIKE '%".$_GET['g']."%' )) LIMIT 10";


						$list = $db->query($sql,true);

						if($list["status"]){
							$lb->toClient(true,$list['query']);
						}else{
							$lb->defaultQuery();
						}
					break;
				}

			break;

			case 'POST':
				//Post request handler

				switch ($_POST["request"]) {
					case 'add':

						if($lb->isSEA($_POST)){
							$client = $db->queryOneValue("orders",'client','number',$_POST['number']);

							if(!$client){
								$lb->toClient(false,$callback,"NotRealClient");
								return false;
							}	

							if($db->isValue("packing","number",$_POST["number"])){
								$lb->toClient(false,$callback,"NumberAlready");
								return false;
							}

							extract($_POST);

							$sql = "INSERT INTO `packing` (`track`, 
														   `number`, 
														   `client`, 
														   `boxes`, 
														   `weight`, 
														   `width`, 
														   `height`, 
														   `status`, 
														   `carrier`, 
														   `origin`, 
														   `destiny`, 
														   `dated`) 
													VALUES ('$track', 
															'$number', 
															'$client', 
															'$boxes', 
															'$weight', 
															'$w', 
															'$h', 
															'$status', 
															'$carrier', 
															'$origin', 
															'$destiny', 
															CURRENT_TIMESTAMP)";

					
							$info = $db->query($sql,false);

							if($info["status"]){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->toClient(false,$callback,"NotEnough");
						}
					break;
						
					case "update":
						$lb->power_session_start();

						if($lb->isAdmin()){
							extract($_POST);
							$sql = "UPDATE `packing` 
									SET track = '$track',
										carrier = '$carrier',
										weight = '$weight',
										status = '$status',
										boxes = '$boxes',
										origin = '$origin',
										destiny = '$destiny',
										width = '$width',
										height = '$height'
									WHERE `packing`.`id` = $id";
							
							$info = $db->query($sql,false,true);

							if($info["status"]){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}
					break;

					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT

			break;

			case 'DELETE':
				//Post handled like Delete

			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

		//$db = new FriendofMySQL($execute);
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 
	

?>